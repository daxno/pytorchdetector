from modeling import get_mobilenetv3_large_fpn, get_atss_model
from torch.backends import cudnn
from torch2trt import TRTModule
from config import cfg
from tqdm import tqdm
import torch
import time


if __name__ == '__main__':
    cudnn.benchmark = True
    num_test_iterations = 200
    batch_size = 8

    model_trt = TRTModule()
    model_trt.load_state_dict(torch.load('/home/nlab/Experiments/Detector/trt_engines/ATSS_MobileNetV3_FaceDetector.pth'))

    # model_pt = get_mobilenetv3_large_fpn(cfg).eval().cuda()
    # model_pt = get_atss_model(cfg, get_mobilenetv3_large_fpn).eval().cuda()

    print('WarmUp')
    for _ in range(100):
        in_tensor = torch.randn((batch_size, 3, 768, 1024)).cuda()
        out = model_trt(in_tensor)

    start_time = time.time()
    for _ in tqdm(range(num_test_iterations)):
        in_tensor = torch.randn((batch_size, 3, 768, 1024)).cuda()
        out = model_trt(in_tensor)
    duration = time.time() - start_time

    print(f'Time for {num_test_iterations} examples: {duration:.4f} s; Mean FPS: {(num_test_iterations*batch_size)/duration:.4f}')
