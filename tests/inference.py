from modeling import get_shufflenetv2_fpn, get_atss_model, get_mobilenetv3_small_fpn
from modeling.ATSS import get_atss_box_coder, get_atss_loss_evaluator
from config import cfg
import torch


if __name__ == '__main__':
    model = get_atss_model(cfg, get_mobilenetv3_small_fpn, trt_mode=False).cuda()

    in_tensor = torch.zeros((8, 3, 768, 1024)).cuda()
    output = model(in_tensor)
    for out in output:
        print(f'TRT Mode outputs: {[o.size() for o in out]}')

