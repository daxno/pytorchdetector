from PIL import Image
from tqdm import tqdm
import numpy as np
import time
import json
import cv2
import os
import io


if __name__ == '__main__':
    # format: /home/nlab/Datasets/WIDER/WIDER_val/images_all/0_Parade_marchingband_1_593.jpg
    annot = json.load(open('/home/nlab/Experiments/Detector/resources/wider_val_annot.json'))

    for image_path in tqdm(annot.keys()):
        # image = cv2.imread(image_path).astype(np.uint8)
        # np.save(image_path.replace('images_all', 'images_np').replace('.jpg', '.npy'), image)
        # np_file = np.memmap(image_path.replace('images_all', 'images_np').replace('.jpg', '.npy'), dtype=np.uint8,
        #                     mode='')
        image = cv2.imread(image_path)
        out_path = image_path.replace('images_all', 'images_np').replace('.jpg', '.npy')
        # fp = np.memmap(out_path, dtype='uint8', mode='w+', shape=image.shape)
        # fp[:] = image[:]
        np.savez_compressed(out_path, image=image)

    # images_path = '/home/nlab/Datasets/WIDER/WIDER_val/images_all'
    # start_time = time.time()
    # images_shapes = json.load(open('/home/nlab/Experiments/Detector/resources/images_shapes.json', 'r'))
    # for image_title in tqdm(os.listdir(images_path)):
    #     image = cv2.imread(f'{images_path}/{image_title}')
    #     # images_shapes.update({f'{images_path}/{image_title}': image.shape})
    #     # image = np.load(f'{images_path}/{image_title}', mmap_mode='r')
    #     # with open(f'{images_path}/{image_title}', 'rb') as f:
    #         # image = cv2.imdecode(np.frombuffer(f.read(), dtype=np.uint8), -1)
    #         # image = np.array(Image.open(io.BytesIO(np.frombuffer(f.read(), dtype=np.uint8))))
    #     # image = np.reshape(image, images_shapes[f'{images_path}/{image_title}'])
    #     assert image is not None
    # duration = time.time() - start_time
    # print(f'Loading time: {duration:.2f} s; mean time per image: {duration/len(images_path):.2f} s')
    # # json.dump(images_shapes, open('/home/nlab/Experiments/Detector/resources/images_shapes.json', 'w'))
