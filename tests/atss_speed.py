from modeling import (get_ssd_face_detector_mxnet,
                      get_atss_model,
                      get_mobilenetv1_fpn,
                      get_shufflenetv2_fpn,
                      get_mobilenetv2_fpn,
                      get_mobilenetv2,
                      get_mobilenetv1_fpnlight,
                      get_mobilenetv3_small_fpn,
                      get_mobilenetv3_large_fpn,
                      get_hbonet_fpn,
                      get_resnet50_fpn,
                      get_resnet34_fpn,
                      get_resnet18_fpn)
from modeling.ATSS import get_atss_box_coder, get_atss_postprocessor
from layers import get_anchor_generator_atss
from structures import to_image_list
from config import cfg
from tqdm import tqdm
import onnxruntime
import mxnet as mx
import torch.onnx
import torch
import onnx
import time


class Postprocessor:
    def __init__(self, cfg):
        # self.device = torch.device('cpu')
        self.anchor_generator = get_anchor_generator_atss(cfg).to(device)
        self.postprocessor = get_atss_postprocessor(cfg, get_atss_box_coder(cfg)).to(device)

    def __call__(self, model_outputs, images=None, anchors=None):
        if anchors is None:
            assert images is not None
        box_cls, box_regression, centerness, bb_features = model_outputs
        # box_cls = [feature.to(self.device) for feature in box_cls]
        # box_regression = [feature.to(self.device) for feature in box_regression]
        # centerness = [feature.to(self.device) for feature in centerness]
        # bb_features = [feature.to(self.device) for feature in bb_features]
        if anchors is None:
            anchors = self.anchor_generator(images, bb_features)
        return self.postprocessor(box_cls, box_regression, centerness, anchors)


def to_numpy(tensor):
    return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()


if __name__ == '__main__':
    # onnx_out_path = '/home/nlab/Experiments/Detector/resources/ATSS_MobileNetV1_FPNLight.onnx'
    device = torch.device('cuda:1')

    # backbone = get_mobilenetv3_large_fpn(cfg).to(device)

    atss_model = get_atss_model(cfg, get_mobilenetv3_small_fpn)
    postprocessor = Postprocessor(cfg)
    atss_model.to(device).eval()
    num_test_iterations = 1000
    im_size = (768, 1024)

    print('Warmup')
    in_tensor = torch.randn((1, 3, *im_size), device=device)
    for _ in tqdm(range(10)):
        in_tensor = torch.randn((1, 3, *im_size), device=device)
        with torch.no_grad():
            output = atss_model(in_tensor)

    print('Testing speed')
    start_time = time.time()
    for _ in tqdm(range(num_test_iterations)):
        in_tensor = torch.randn((1, 3, *im_size), device=device)
        with torch.no_grad():
            output = atss_model(in_tensor)

    duration = time.time() - start_time

    print(f'Time for {num_test_iterations} examples: {duration:.4f} s; Mean FPS: {num_test_iterations/duration:.4f}')

