import sys
import torch
sys.path.append('.')
from config import cfg
from data import get_dataloader
from modeling import get_mobilenetv1_fpn
from modeling import get_atss_model


if __name__ == '__main__':
    loader = get_dataloader(cfg, is_train=True)
    print('Loader created')
    device = torch.device('cuda:0')

    backbone = get_mobilenetv1_fpn(cfg)
    model = get_atss_model(cfg, backbone).to(device)
    print('Model created')
    for idx, (images, targets, _) in enumerate(loader):
        images = images.to(device)
        targets = [target.to(device) for target in targets]
        model_out = model(images, targets)
        print(len(model_out))
