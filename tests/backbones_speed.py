import time
import torch
from tqdm import tqdm
from config import cfg
from modeling import (
    get_mobilenetv1,
    get_shufflenetv2,
    get_resnet18c4,
    get_hardnet39ds
)


def get_backbone_speed(model, model_name, device):
    input_tensor = torch.randn((1, 3, 768, 1024)).to(device)

    # Warmup
    for _ in tqdm(range(10)):
        output = model(input_tensor)

    # Inference
    start_time = time.time()
    for _ in tqdm(range(100)):
        output = model(input_tensor)
    dur = time.time() - start_time
    print(f'{model_name} FPS: {100 / dur}')


if __name__ == '__main__':
    device = torch.device('cpu')
    models = [
        get_mobilenetv1,
        get_resnet18c4,
        get_shufflenetv2,
        get_hardnet39ds
    ]

    model_names = [
        'MobileNetV1',
        'ResNet18',
        'ShuffleNetV2',
        'HardNet39ds'
    ]

    for model, model_name in zip(models, model_names):
        mod = model(cfg).to(device)
        get_backbone_speed(mod, model_name, device)
