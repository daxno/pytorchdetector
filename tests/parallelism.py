from modeling import get_mobilenetv1_fpn
from modeling.ATSS import get_atss_loss_evaluator, get_atss_postprocessor, get_atss_box_coder
from layers import get_anchor_generator_atss
from modeling.ATSS.ATSS import ATSS
from structures import to_image_list
from data import get_dataloader
from config import cfg
from tqdm import tqdm
import torch


if __name__ == '__main__':
    root_device = torch.device('cuda:0')
    loss_evaluator_device = torch.device('cpu')

    bb = get_mobilenetv1_fpn(cfg)
    model = ATSS(cfg, bb)

    box_coder = get_atss_box_coder(cfg)
    # Train stage
    loss_evaluator = get_atss_loss_evaluator(cfg, box_coder)
    # Val stage
    postprocessor_test = get_atss_postprocessor(cfg, box_coder)
    anchor_generator = get_anchor_generator_atss(cfg)

    loader = get_dataloader(cfg, True)

    model.to(root_device)
    # model.eval()
    # model.bind((768, 1024), anchor_generator, root_device)
    model_parallel = torch.nn.DataParallel(model)
    model_parallel.train()

    for idx, (images, targets) in enumerate(loader):
        model_input = images.tensors.to(root_device)

        box_cls, box_regression, centerness, bb_features = model_parallel(model_input)
        # TRAIN STEP
        # 220 FPS - anchor generator
        # for _ in tqdm(range(1000000)):
        anchors = anchor_generator(images, bb_features)
        box_cls = [o.to(loss_evaluator_device) for o in box_cls]
        box_regression = [o.to(loss_evaluator_device) for o in box_regression]
        centerness = [o.to(loss_evaluator_device) for o in centerness]

        for _ in tqdm(range(1000000)):
            loss_dict = loss_evaluator(box_cls, box_regression, centerness, targets, anchors)
        print(loss_dict)
        # break
        # VAL STEP
        # boxes = model_parallel(images.tensors.to(root_device))
