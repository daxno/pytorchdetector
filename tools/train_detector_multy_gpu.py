import os
import time
import torch
from os import mkdir
import datetime
from tqdm import tqdm
from torch.backends import cudnn

from modeling.ATSS import get_atss_loss_evaluator_old as get_atss_loss_evaluator, get_atss_box_coder, get_atss_postprocessor
from modeling import get_atss_model, get_mobilenetv3_small_fpn, get_hbonet_fpn
from nvidia.dali.plugin.pytorch import DALIGenericIterator
from solver import make_lr_scheduler, make_sgd_optimizer
from layers import get_anchor_generator_atss
from utils.logger import setup_logger
from structures import to_image_list
from utils import TBWriter, Meter
from data import get_dataloader
from solver import Ranger
from config import cfg

# from apex import amp, optimizers
# from apex.parallel import DistributedDataParallel
# TODO fix Warning: Original ImportError was: ModuleNotFoundError("No module named 'amp_C'")  in apex


class LossEvaluator:
    def __init__(self, device, anchors=None):
        self.device = device
        print(f'Device in LossEvaluator: {self.device}')
        self.anchors = None
        self.anchor_generator = None
        if anchors is not None:
            self.anchors = [a.to(self.device) for a in anchors]
        else:
            self.anchor_generator = get_anchor_generator_atss(cfg)
        self.loss_evaluator = get_atss_loss_evaluator(cfg, get_atss_box_coder(cfg), self.device)

    def __call__(self, images, model_outputs, targets: list):
        box_cls, box_regression, centerness, bb_features = model_outputs
        if box_cls[0].device != self.device:
            box_cls = [feature.to(self.device) for feature in box_cls]
            box_regression = [feature.to(self.device) for feature in box_regression]
            centerness = [feature.to(self.device) for feature in centerness]
            bb_features = [feature.to(self.device) for feature in bb_features]
        if self.anchors is None:
            anchors = self.anchor_generator(images, bb_features)
            anchors = [[a.to(self.device) for a in anch] for anch in anchors]
            loss_dct = self.loss_evaluator(box_cls=box_cls, box_regression=box_regression, centerness=centerness,
                                           targets=targets, anchors=anchors)
        else:
            labels, reg_targets, centerness_target = targets
            anchors = self.anchors
            loss_dct = self.loss_evaluator(box_cls=box_cls, box_regression=box_regression, centerness=centerness,
                                           anchors=anchors, labels=labels, reg_targets=reg_targets,
                                           centerness_target=centerness_target)
        return loss_dct


class Postprocessor:
    def __init__(self):
        self.anchor_generator = get_anchor_generator_atss(cfg)
        self.postprocessor = get_atss_postprocessor(cfg, get_atss_box_coder(cfg))

    def __call__(self, images, model_outputs):
        box_cls, box_regression, centerness, bb_features = model_outputs
        anchors = self.anchor_generator(images, bb_features)
        return self.postprocessor(box_cls, box_regression, centerness, anchors)


def get_anchors_for_fixed_size(cfg, model):
    anchor_generator = get_anchor_generator_atss(cfg)
    input_tensor = torch.zeros((1, 3, *cfg.DATA.TRAIN_SIZE)).cuda()
    images = to_image_list(input_tensor)
    _, _, _, bb_features = model(input_tensor)
    anchors = anchor_generator(images, bb_features)[0]
    # Может тут нужно взять нулевой
    del anchor_generator, images, input_tensor
    return anchors


def save_model(model, iteration, optimizer_dict, scheduler_dict, regr_loss, cls_loss, cn_loss, amp_dict=None):
    # Saving model in the end of epoch
    state_dict = {
        'iteration': iteration,
        'optimizer': optimizer_dict,
        'scheduler': scheduler_dict,
        'amp': amp_dict,
        'state_dict': model.state_dict()
    }
    torch.save(state_dict, f"{cfg.PATHS.OUTPUT_DIR}/{cfg.PATHS.MODEL_NAME}_{iteration}_regr_{regr_loss:.5f}_cls_{cls_loss:.5f}_cn_{cn_loss:.5f}.pth")


def train_step(cfg, model, loss_evaluator, loader, optimizer, scheduler, tb_writer, meter, step,
               loss_scaler=None, logger=None):
    for idx, (images, targets) in tqdm(enumerate(loader), total=len(loader)):
        model_input = images.tensors.to('cuda:0')
        model_output = model(model_input)
        # targets = target_generator(boxes, labels, num_dets_per_image)
        # targets = [targets['cls_target'], targets['reg_target'], targets['centerness_target']]
        loss_dict = loss_evaluator(images, model_output, targets)
        # loss_dict = loss_evaluator(box_cls, box_regr, centerness, anchors, cls_targets, regr_targets, cn_target)

        # Update TBWriter and Meter
        tb_writer.add_scalars(loss_dict, step)
        tb_writer.add_scalar('learning_rate', optimizer.param_groups[0]['lr'], step)
        meter.update(loss_dict)

        # Backward step
        optimizer.zero_grad()
        print('losses sizes:', [l.size() for l in loss_dict.values()])
        losses = sum(loss for loss in loss_dict.values())

        start_time = time.time()
        # Scale loss if using FLOAT16 reduction
        if loss_scaler is not None:
            with loss_scaler.scale_loss(losses, optimizer) as scaled_loss:
                scaled_loss.backward()
        else:
            losses.backward()

        optimizer.step()
        scheduler.step()
        print('Backward step time:', time.time() - start_time)
        step += 1

        # Saving model
        if idx and not idx % cfg.LOGGER.CHECKPOINT_PERIOD:
            save_model(model=model, iteration=step, optimizer_dict=optimizer.state_dict(),
                       scheduler_dict=scheduler.state_dict(), amp_dict=None,
                       regr_loss=meter.get_by_name('loss_reg'), cls_loss=meter.get_by_name('loss_cls'),
                       cn_loss=meter.get_by_name('loss_centerness'))

        # Log temp metrics
        if idx and logger is not None and not idx % cfg.LOGGER.LOG_PERIOD:
            msg = f'Iteration {idx}: '
            for name, val in meter.get().items():
                msg += f'{name}: {val:.5f} '
            logger.info(msg)
            meter.reset()


def val_step(cfg, model, postprocessor, loader, device, tb_writer, meter, step, logger=None):
    raise NotImplementedError('')


def train(cfg, logger):
    device = torch.device('cuda:0')
    # Creating model
    backbone = get_hbonet_fpn(cfg)
    model = get_atss_model(cfg, backbone)
    model.to(device)
    # Parallelize model across all GPUs
    model = torch.nn.DataParallel(model)
    # optimizer = make_sgd_optimizer(cfg, model)
    optimizer = Ranger(model.parameters(), lr=1e-3)
    scheduler = make_lr_scheduler(cfg, optimizer)
    if cfg.DATA.USE_APEX:
        raise NotImplementedError('Multiprecision training is not implemented yet')

    # Если обучаем для фиксированного размера, то сгенерим якоря сразу и отдадим в Dataloader
    anchors = get_anchors_for_fixed_size(cfg, model)
    # Creating loss evaluator and anchor generator
    loss_evaluator = LossEvaluator(torch.device('cpu'), anchors=anchors)
    # postprocessor = Postprocessor()

    # Creating dataloaders
    train_loader = get_dataloader(cfg, is_train=True, anchors=anchors)
    # val_loader = get_dataloader(cfg, is_train=False)
    # dali_data_pipeline = get_dali_dataloader(cfg, is_train=True)
    # torch_data_iterator = DALIGenericIterator([dali_data_pipeline], output_map=['images', 'boxes', 'labels', 'num_dets'],
    #                                           size=cfg.DATALOADER.ITER_PER_EPOCH, auto_reset=True)
    # target_generator = get_atss_target_generator(cfg, anchors, cfg.DATA.TRAIN_SIZE, cfg.DATALOADER.PREPROCESSING_DEVICE)

    # (optional) Loading model
    start_epoch = 0
    start_iter = 0
    if cfg.PATHS.LOAD_FROM:
        state_dict = torch.load(cfg.PATHS.LOAD_FROM)
        model.load_state_dict(state_dict['state_dict'])
        optimizer.load_state_dict(state_dict['optimizer'])
        if 'scheduler' in state_dict.keys():
            scheduler.load_state_dict(state_dict['scheduler'])
        # if 'amp' in state_dict.keys():
        #    amp.load_state_dict(state_dict['amp'])

        start_iter = state_dict['iteration']
        start_epoch = (start_iter // len(cfg.DATALOADER.ITER_PER_EPOCH)) + 1
        # train_loader = get_dataloader(cfg, is_train=True, start_iter=0)

    # Creating Meter and TBWriter
    ts = time.time()
    stamp = datetime.datetime.fromtimestamp(ts).strftime('%Y_%m_%d__%H_%M_%S')
    tb_writer = TBWriter(os.path.join(cfg.PATHS.TB_WRITER_PATH, stamp))
    meter = Meter()
    logger.info('Train starting')
    logger.info(f'Start epoch: {start_epoch}')
    # Perform train step
    for epoch in range(start_epoch, cfg.DATA.MAX_EPOCH):
        train_step(cfg=cfg, model=model, loss_evaluator=loss_evaluator, loader=train_loader,
                   optimizer=optimizer, scheduler=scheduler, tb_writer=tb_writer,
                   meter=meter, step=start_iter, loss_scaler=None, logger=logger)
        start_iter = len(train_loader) * (epoch+1)
        # Perform validation step
        # TODO implement


def main():
    torch.random.manual_seed(155)
    num_gpus = torch.cuda.device_count()
    cfg.freeze()
    print('Config loaded')

    output_dir = cfg.PATHS.OUTPUT_DIR
    if output_dir and not os.path.exists(output_dir): mkdir(output_dir)
    print('Output directory created')

    logger = setup_logger("ATSS person detector", output_dir, 0)
    logger.info("Using {} GPUS".format(num_gpus))

    logger.info("Running with config:\n{}".format(cfg))
    train(cfg, logger)


if __name__ == '__main__':
    # cudnn.benchmark = True
    main()
    # from structures import to_image_list
    # anchor_generator = get_anchor_generator_atss(cfg)
    # model = get_atss_model(cfg, get_shufflenetv2_fpn(cfg)).cuda()
    # input_tensor = torch.zeros((1, 3, 768, 1024)).cuda()
    # _, _, _, bb_features = model(input_tensor)
    # images_list = to_image_list(input_tensor)
    # anchors = anchor_generator(images_list, bb_features)
    # print(anchors)
