import torch
import tensorrt as trt
from config import cfg
from torch2trt import torch2trt
from torch.backends import cudnn
from layers import get_anchor_generator_atss, convert_iterpolate
from modeling import get_hbonet_fpn, get_mobilenetv3_large_fpn, get_atss_model, get_mobilenetv3_small_fpn


# Ошибка: не найден файл ....7
# Лечится: sudo cp TensorRT-7.0.0.11/targets/x86_64-linux-gnu/lib/lib* /usr/lib

if __name__ == '__main__':
    cudnn.benchmark = True
    device = torch.device('cuda:1')
    ckpt_dir = '/home/nlab/ExperimentsOutputs/face_detector'
    state_dict_path = f'{ckpt_dir}/ATSS_MobileNetV3Large_FaceDetector_110001_regr_0.20057_cls_0.07469_cn_0.59436.pth'
    state_dict = torch.load(state_dict_path)['state_dict']
    state_dict = {k.replace('module.', ''): v for k, v in state_dict.items()}
    # create some regular pytorch model...
    # anchor_generator = get_anchor_generator_atss(cfg)
    # anchors = anchor_generator(to_image_list(images), features)
    model = get_atss_model(cfg, get_mobilenetv3_small_fpn, trt_mode=True).eval().to(device)
    model.load_state_dict(state_dict)
    print('PyTorch model loaded')

    # create example data
    x = torch.ones((cfg.DATALOADER.VAL_BS, 3, 768, 1024)).to(device)
    print(x.size())
    model(x)

    # convert to TensorRT feeding sample data as input
    # TODO тут можно добваить калибровку в INT8
    inputs = ['images']
    outputs = [f'feature_map_{idx}' for idx in range(5)]
    model_trt = torch2trt(model,
                          [x],
                          input_names=inputs,
                          output_names=outputs,
                          max_batch_size=cfg.DATALOADER.VAL_BS,
                          log_level=trt.Logger.VERBOSE,
                          strict_type_constraints=False,
                          keep_network=True)
    torch.save(model_trt.state_dict(), '/home/nlab/Experiments/Detector/trt_engines/ATSS_MobileNetV3_FaceDetector_bs_8.pth')
    print('Engine saved successfully!')

