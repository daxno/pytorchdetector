import os
import cv2
import time
import torch
from os import mkdir
import datetime
import numpy as np
from tqdm import tqdm
from torch.backends import cudnn

from modeling.ATSS import get_atss_loss_evaluator, get_atss_box_coder, get_atss_postprocessor
from modeling import get_atss_model, get_mobilenetv3_large_fpn, get_hbonet_fpn, get_mobilenetv1_fpn, get_mobilenetv3_small_fpn
from data import get_dali_dataloader, get_atss_target_generator
from nvidia.dali.plugin.pytorch import DALIGenericIterator
from solver import make_lr_scheduler, make_sgd_optimizer
from layers import get_anchor_generator_atss
from utils.logger import setup_logger
from structures import to_image_list
from utils import TBWriter, Meter
from solver import Ranger
from config import cfg


class LossEvaluator:
    def __init__(self, device, anchors=None):
        self.device = device
        print(f'Device in LossEvaluator: {self.device}')
        self.anchors = None
        self.anchor_generator = None
        if anchors is not None:
            self.anchors = [a.to(self.device) for a in anchors]
        else:
            self.anchor_generator = get_anchor_generator_atss(cfg)
        self.loss_evaluator = get_atss_loss_evaluator(cfg, get_atss_box_coder(cfg), self.device)

    def __call__(self, images, model_outputs, targets: list):
        box_cls, box_regression, centerness, bb_features = model_outputs
        if box_cls[0].device != self.device:
            box_cls = [feature.to(self.device) for feature in box_cls]
            box_regression = [feature.to(self.device) for feature in box_regression]
            centerness = [feature.to(self.device) for feature in centerness]
            bb_features = [feature.to(self.device) for feature in bb_features]
        if self.anchors is None:
            anchors = self.anchor_generator(images, bb_features)
            anchors = [[a.to(self.device) for a in anch] for anch in anchors]
            loss_dct = self.loss_evaluator(box_cls=box_cls, box_regression=box_regression, centerness=centerness,
                                           targets=targets, anchors=anchors)
        else:
            labels, reg_targets, centerness_target = targets
            anchors = self.anchors
            loss_dct = self.loss_evaluator(box_cls=box_cls, box_regression=box_regression, centerness=centerness,
                                           anchors=anchors, labels=labels, reg_targets=reg_targets,
                                           centerness_target=centerness_target)
        return loss_dct


class Postprocessor:
    def __init__(self, cfg):
        # self.device = torch.device('cpu')
        self.anchor_generator = get_anchor_generator_atss(cfg)
        self.postprocessor = get_atss_postprocessor(cfg, get_atss_box_coder(cfg))

    def __call__(self, model_outputs, images=None, anchors=None):
        if anchors is None: assert images is not None
        box_cls, box_regression, centerness, bb_features = model_outputs
        # box_cls = [feature.to(self.device) for feature in box_cls]
        # box_regression = [feature.to(self.device) for feature in box_regression]
        # centerness = [feature.to(self.device) for feature in centerness]
        # bb_features = [feature.to(self.device) for feature in bb_features]
        if anchors is None:
            anchors = self.anchor_generator(images, bb_features)
        return self.postprocessor(box_cls, box_regression, centerness, anchors)


def get_anchors_for_fixed_size(cfg, model):
    anchor_generator = get_anchor_generator_atss(cfg)
    input_tensor = torch.zeros((cfg.DATALOADER.TRAIN_BS, 3, 768, 1024)).cuda()
    images = to_image_list(input_tensor)
    _, _, _, bb_features = model(input_tensor)
    anchors = anchor_generator(images, bb_features)[0]
    del anchor_generator, images, input_tensor
    return anchors


def save_model(model, iteration, optimizer_dict, scheduler_dict, regr_loss, cls_loss, cn_loss, amp_dict=None):
    # Saving model in the end of epoch
    state_dict = {
        'iteration': iteration,
        'optimizer': optimizer_dict,
        'scheduler': scheduler_dict,
        'amp': amp_dict,
        'state_dict': model.state_dict()
    }
    torch.save(state_dict, f"{cfg.PATHS.OUTPUT_DIR}/{cfg.PATHS.MODEL_NAME}_{iteration}_regr_{regr_loss:.5f}_cls_{cls_loss:.5f}_cn_{cn_loss:.5f}.pth")


def train_step(cfg, model, loss_evaluator, target_generator, loader, optimizer, scheduler, tb_writer, meter, step,
               postprocessor, anchors=None, logger=None, num_iterations=None, iterator_val=None):
    if num_iterations is None:
        num_iterations = loader._size

    for idx, data in tqdm(enumerate(loader), total=num_iterations):
        images = torch.cat([d['images'] for d in data])
        # print(images.min(), images.max(), images.size())
        boxes = torch.cat([d['boxes'] for d in data])
        labels = torch.cat([d['labels'] for d in data])
        num_dets_per_image = torch.cat([d['num_dets'].squeeze(-1) for d in data])
        # for image, im_boxes in zip(images, boxes):
        #     image = image.data.cpu().numpy()
        #     if image.ndim == 4: image = image[0]
        #     image = (image.transpose((1, 2, 0))*np.array([58.3950, 57.1200, 57.3750])+np.array([123.6750, 116.2800, 103.5300])).astype(np.uint8).copy()
        #     print(image.min(), image.max())
        #     for box in im_boxes:
        #         box = (box.data.cpu().numpy() * np.array([1024, 768, 1024, 768]))
        #         x1, y1, x2, y2 = list(map(int, box))
        #         image = cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
        #     cv2.imshow('image', image)
        #     cv2.waitKey()
        # exit(1)

        original_shapes = torch.cat([d['orig_shapes'] for d in data])
        boxes = loader._pipes[0].preprocess_boxes(images=images, bboxes=boxes, orig_shapes=original_shapes)

        model_output = model(images)
        targets = target_generator(boxes, labels, num_dets_per_image, image_size=images.size()[-2:])

        targets = [targets['cls_target'], targets['reg_target'], targets['centerness_target']]
        loss_dict = loss_evaluator(images, model_output, targets)

        # Update TBWriter and Meter
        tb_writer.add_scalars(loss_dict, step)
        # print(f'Temp lr:', optimizer.param_groups[0]['lr'])
        tb_writer.add_scalar('learning_rate', optimizer.param_groups[0]['lr'], step)
        meter.update(loss_dict)

        # Backward step
        optimizer.zero_grad()
        # print('Losses:', loss_dict.values())
        losses = sum(loss for loss in loss_dict.values())

        # Scale loss if using FLOAT16 reduction
        losses.backward()

        optimizer.step()
        scheduler.step()
        step += 1

        # Saving temp images with predictions
        if anchors is not None and idx and not idx % cfg.LOGGER.SAVE_IMAGE_EVERY:
            if iterator_val is not None:
                data = next(iterator_val)
                images = torch.cat([d['images'] for d in data])
                model_output = model(images)

            tb_anchors = [[anch.to('cuda:0') for anch in anchors] for _ in range(cfg.DATALOADER.TRAIN_BS)]
            predictions = postprocessor(model_output, anchors=tb_anchors)
            pred_boxes = [pred.bbox.to(torch.device('cpu')) for pred in predictions]
            tb_writer.add_images(images, pred_boxes, global_step=step)

        # Saving model
        if idx and not idx % cfg.LOGGER.CHECKPOINT_PERIOD:
            save_model(model=model, iteration=step, optimizer_dict=optimizer.state_dict(),
                       scheduler_dict=scheduler.state_dict(), amp_dict=None,
                       regr_loss=meter.get_by_name('loss_reg'), cls_loss=meter.get_by_name('loss_cls'),
                       cn_loss=meter.get_by_name('loss_centerness'))

        # Log temp metrics
        if idx and logger is not None and not idx % cfg.LOGGER.LOG_PERIOD:
            msg = f'Iteration {idx}: '
            for name, val in meter.get().items():
                msg += f'{name}: {val:.5f} '
            logger.info(msg)
            meter.reset()


def val_step(cfg, model, postprocessor, loader, device, tb_writer, meter, step, logger=None):
    raise NotImplementedError('')


def train(cfg, logger):
    device = torch.device('cuda:0')
    # Creating model
    model = get_atss_model(cfg, get_mobilenetv3_small_fpn)
    model.to(device)
    # Parallelize model across all GPUs
    model = torch.nn.DataParallel(model)
    optimizer = make_sgd_optimizer(cfg, model)
    # optimizer = Ranger(model.parameters(), lr=1e-3)
    scheduler = make_lr_scheduler(cfg, optimizer)
    if cfg.DATA.USE_APEX:
        raise NotImplementedError('Multiprecision training is not implemented yet')

    # Если обучаем для фиксированного размера, то сгенерим якоря сразу и отдадим в Dataloader
    anchors = get_anchors_for_fixed_size(cfg, model)
    # Creating loss evaluator and anchor generator
    loss_evaluator = LossEvaluator(torch.device('cuda:1'), anchors=anchors)
    postprocessor = Postprocessor(cfg)

    # Creating dataloaders
    # train_loader = get_dataloader(cfg, is_train=True, anchors=anchors)
    # val_loader = get_dataloader(cfg, is_train=False)
    pipelines = [
        # get_dali_dataloader(cfg, is_train=True, dev_id=0),
        get_dali_dataloader(cfg, is_train=True, dev_id=1)
    ]
    torch_data_iterator = DALIGenericIterator(pipelines, output_map=['images', 'boxes', 'labels', 'num_dets',
                                                                     'orig_shapes'],
                                              size=cfg.DATALOADER.ITER_PER_EPOCH*cfg.DATALOADER.TRAIN_BS*len(pipelines),
                                              auto_reset=True, dynamic_shape=True)
    target_generator = get_atss_target_generator(cfg, anchors, cfg.DATA.TRAIN_SIZE if len(cfg.DATA.TRAIN_SIZE) else None,
                                                 'cuda:1')

    # (optional) Loading model
    start_epoch = 0
    start_iter = 0
    if cfg.PATHS.LOAD_FROM:
        state_dict = torch.load(cfg.PATHS.LOAD_FROM)
        model.load_state_dict(state_dict['state_dict'])
        optimizer.load_state_dict(state_dict['optimizer'])
        if 'scheduler' in state_dict.keys():
            scheduler.load_state_dict(state_dict['scheduler'])

        # start_iter = state_dict['iteration']
        start_iter = 0
        try: start_epoch = cfg.DATALOADER.START_EPOCH
        except: start_epoch = (start_iter // cfg.DATALOADER.ITER_PER_EPOCH) + 1

    # Creating Meter and TBWriter
    ts = time.time()
    stamp = datetime.datetime.fromtimestamp(ts).strftime('%Y_%m_%d__%H_%M_%S')
    tb_writer = TBWriter(cfg, os.path.join(cfg.PATHS.TB_WRITER_PATH, stamp))
    meter = Meter()
    logger.info('Train starting')
    logger.info(f'Start epoch: {start_epoch}')
    # Perform train step
    for epoch in range(start_epoch, cfg.DATA.MAX_EPOCH):
        train_step(cfg=cfg, model=model, loss_evaluator=loss_evaluator, loader=torch_data_iterator,
                   optimizer=optimizer, scheduler=scheduler, tb_writer=tb_writer, target_generator=target_generator,
                   meter=meter, step=start_iter, logger=logger, postprocessor=postprocessor, anchors=anchors,
                   num_iterations=cfg.DATALOADER.ITER_PER_EPOCH, iterator_val=None)
        start_iter = cfg.DATALOADER.ITER_PER_EPOCH * (epoch+1)


def main():
    torch.random.manual_seed(155)
    num_gpus = torch.cuda.device_count()
    cfg.freeze()
    print('Config loaded')

    output_dir = cfg.PATHS.OUTPUT_DIR
    if output_dir and not os.path.exists(output_dir): mkdir(output_dir)
    print('Output directory created')

    logger = setup_logger("ATSS person detector", output_dir, 0)
    logger.info("Using {} GPUS".format(num_gpus))

    logger.info("Running with config:\n{}".format(cfg))
    train(cfg, logger)


if __name__ == '__main__':
    cudnn.benchmark = True
    main()
    # from structures import to_image_list
    # anchor_generator = get_anchor_generator_atss(cfg)
    # model = get_atss_model(cfg, get_shufflenetv2_fpn(cfg)).cuda()
    # input_tensor = torch.zeros((1, 3, 768, 1024)).cuda()
    # _, _, _, bb_features = model(input_tensor)
    # images_list = to_image_list(input_tensor)
    # anchors = anchor_generator(images_list, bb_features)
    # print(anchors)
