from data.transforms.transforms import ResizeKeepAspectRatio
from tqdm import tqdm
import json
import cv2
import os


if __name__ == '__main__':
    out_path = '/media/nlab/DATA/FaceDetectorData/pics_processed'
    resize_fn = ResizeKeepAspectRatio((768, 1024))

    annot_new = {}
    counter = 0
    annot = json.load(open('/home/nlab/Experiments/Detector/resources/face_annotations_all.json', 'r'))
    for idx, (image_path, dets) in tqdm(enumerate(annot.items()), total=len(annot.keys())):
        subset, image_name = image_path.split('/')[-2:]
        new_path = f'{out_path}/{subset}/{image_name}'
        if not os.path.exists(f'{out_path}/{subset}'): os.makedirs(f'{out_path}/{subset}')

        image = cv2.imread(image_path)

        bboxes = [[float(box['x']), float(box['y']), float(box['w']), float(box['h'])] for box in dets]
        out = resize_fn(True, image, bboxes)
        image = out['image']
        bboxes = out['bboxes']

        # for b in bboxes:
        #     x, y, w, h = list(map(int, b))
        #     image = cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

        # cv2.imshow('image', image)
        # if cv2.waitKey() == 27:
        #     cv2.destroyAllWindows()
        #     break

        cv2.imwrite(new_path, image)
        annot_new.update({new_path: [{'class': 'face', 'x': b[0], 'y': b[1], 'w': b[2], 'h': b[3]} for b in bboxes]})
        if idx and not idx % 1000:
            json.dump(annot_new, open(f'/home/nlab/Experiments/Detector/resources/faces_annot_processed/faces_{counter}.json', 'w'))
            counter += 1
            annot_new = {}
    json.dump(annot_new, open(f'/home/nlab/Experiments/Detector/resources/faces_annot_processed/faces_{counter}.json', 'w'))
