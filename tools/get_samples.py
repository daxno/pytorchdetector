from tqdm import tqdm
import numpy as np
import json
import cv2


if __name__ == '__main__':
    annot = json.load(open('/home/nlab/Experiments/Detector/resources/face_annotations_all.json', 'r'))
    out_images_path = '/home/nlab/Datasets/FacesSamples/images'

    metadata = {}

    paths = np.random.choice(list(annot.keys()), size=105000, replace=False)
    for path in tqdm(paths):
        image_name = path.split('/')[-1]
        dets = annot[path]
        det_idx = np.random.choice(list(range(len(dets))))
        det = list(dets)[det_idx]

        image = cv2.imread(path)
        x1, y1, w, h, x2, y2 = det['x'], det['y'], det['w'], det['h'], det['x']+det['w'], det['y']+det['h']
        x1 = x1 - w*0.2
        y1 = y1 - h*0.2
        x2 = x2 + w*0.2
        y2 = y2 + h*0.2

        pad_x1, pad_x2, pad_y1, pad_y2 = 0, 0, 0, 0
        if x1 < 0: pad_x1 = abs(x1)
        if y1 < 0: pad_y1 = abs(y1)
        if x2 > image.shape[1]: pad_x2 = x2 - image.shape[1]
        if y2 > image.shape[0]: pad_y2 = y2 - image.shape[0]

        # w, h = x2 - x1, y2 - y1
        # max_side = max(w, h)
        # pad_w, pad_h = (max_side - w) / 2, (max_side - h) / 2

        # sample = np.zeros((int(max_side), int(max_side), 3), dtype=np.uint8)
        x1, y1, x2, y2, pad_x1, pad_y1, pad_x2, pad_y2 = list(map(int, [x1, y1, x2, y2, pad_x1, pad_y1, pad_x2, pad_y2]))
        # sample[pad_h:-pad_h, pad_w:-pad_w] = image[y1:y2, x1:x2]
        # sample = np.zeros(((y2+pad_y2) - (y1-pad_y1), (x2+pad_x2) - (x1-pad_x1), 3), dtype=np.uint8)
        # sample[pad_y1: sample.shape[0]-pad_y2, pad_x1: sample.shape[1]-pad_x2, :] = image[y1: y2, x1: x2, :]
        sample = image[y1: y2, x1:x2]

        if sum([pad_x1, pad_x2, pad_y1, pad_y2]): continue
        # sample = np.pad(sample, ((pad_y1, pad_y2), (pad_x1, pad_x2), (0, 0)))
        cv2.imwrite(f'{out_images_path}/{image_name}', sample)

        metadata.update({path: det})
    json.dump(metadata, open('/home/nlab/Datasets/FacesSamples/metadata.json', 'w'))


