from .lr_scheduler import WarmupMultiStepLR
from .LookAhead import Lookahead
from .Ranger import Ranger
from .RAdam import RAdam
import logging
import torch


def make_sgd_optimizer(cfg, model):
    logger = logging.getLogger(cfg.LOGGER.NAME)
    params = []
    lr = cfg.SOLVER.SGD.BASE_LR
    for key, value in model.named_parameters():
        if not value.requires_grad:
            continue
        weight_decay = cfg.SOLVER.SGD.WEIGHT_DECAY
        if "bias" in key:
            lr = cfg.SOLVER.SGD.BASE_LR * cfg.SOLVER.SGD.BIAS_LR_FACTOR
            weight_decay = cfg.SOLVER.SGD.WEIGHT_DECAY_BIAS
        if key.endswith(".offset.weight") or key.endswith(".offset.bias"):
            logger.info("set lr factor of {} as {}".format(
                key, cfg.SOLVER.SGD.DCONV_OFFSETS_LR_FACTOR
            ))
            lr *= cfg.SOLVER.SGD.DCONV_OFFSETS_LR_FACTOR
        params += [{"params": [value], "lr": lr, "weight_decay": weight_decay}]

    optimizer = torch.optim.SGD(params, lr, momentum=cfg.SOLVER.SGD.MOMENTUM)
    return optimizer


def make_lr_scheduler(cfg, optimizer):
    return WarmupMultiStepLR(
        optimizer,
        cfg.DATALOADER.STEPS,
        cfg.SOLVER.WARMUP.GAMMA,
        warmup_factor=cfg.SOLVER.WARMUP.FACTOR,
        warmup_iters=cfg.SOLVER.WARMUP.ITERS,
        warmup_method=cfg.SOLVER.WARMUP.METHOD,
    )
