import time
import h5py
import cv2
import numpy as np

if __name__ == '__main__':
    path = '/home/nlab/Datasets/COCO/val2017.hdf5'

    with h5py.File(path, 'r') as f:
        images = list(f['train_images'])
        boxes = list(f['boxes'])
        labels = list(f['labels'])
        orig_shapes = list(f['original_shape'])
        paths = list(f['paths'])

    start_time = time.time()
    for image, shape, path in zip(images, orig_shapes, paths):
        image = image.reshape(tuple(shape))
        # image_orig = cv2.imread(path)
        # print(image_orig.shape)
        # diff = np.sum(image_orig - image)
        # print(diff)
        # cv2.imshow('image', image)
        #
        # if cv2.waitKey() == 27:
        #     break
    duration = time.time() - start_time
    print('Per image mean time:', duration/len(images))

