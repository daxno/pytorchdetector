from tqdm import tqdm
import numpy as np
import h5py
import json
import cv2
import os
import io
os.environ['H5T_CSET_ASCII'] = '1'

if __name__ == '__main__':
    annotations = json.load(open('/home/nlab/Experiments/Detector/resources/wider_val_annot.json', 'r'))

    # images_path = '/home/nlab/Datasets/COCO/val2017'
    # images_num = 10  # len(annotations)
    # All images already resized
    # test_image = cv2.imread(list(annotations.keys())[10])
    # image_size = test_image.shape[:2]
    hdf5_path = '/home/nlab/Datasets/WIDER/WIDER_val/data.hdf5'

    max_gt_boxes = 50
    image_shape = (768, 1024, 3)
    hdf5_file = h5py.File(hdf5_path, mode='w')
    # hdf5_file.setdefault('H5T_CSET_ASCII', True)
    images_num = len(annotations)
    # hdf5_file.create_dataset('train_images', (images_num, *image_shape), h5py.h5t.STD_U8BE,
    #                          compression="gzip", compression_opts=4)
    # hdf5_file.create_dataset('original_shape', (images_num, np.int16))

    # for idx, (image_path, dets) in tqdm(enumerate(annotations.items()), total=len(annotations)):
    images = np.empty((images_num, ), dtype=h5py.string_dtype())
    shapes = np.empty((images_num, 3), dtype=np.uint16)
    boxes = np.empty((images_num, ), dtype=h5py.vlen_dtype(np.dtype('float16')))
    labels = np.empty((images_num, ), dtype=h5py.vlen_dtype(np.dtype('uint16')))
    paths = np.empty((images_num, ), dtype=h5py.string_dtype())
    # for idx, image_title in tqdm(enumerate(os.listdir(images_path)), total=len(os.listdir(images_path))):
    for idx, (image_path, dets) in tqdm(enumerate(annotations.items()), total=len(annotations)):
        image = cv2.imread(image_path)
        image_shape_orig = image.shape
        # hdf5_file['train_images'][idx, ...] = cv2.resize(image, (image_shape[1], image_shape[0]))[np.newaxis, ...]
        # hdf5_file['original_shape'][idx, ...] = [image_shape_orig]
        images[idx] = open(image_path, 'rb').read()
        shapes[idx] = image_shape_orig
        boxes[idx] = np.array([[d['x'], d['y'], d['w'], d['h']] for d in dets]).flatten()
        labels[idx] = np.array([0 for _ in range(len(dets))])
        paths[idx] = image_path

    # hdf5_file.create_dataset('images', (images_num, ), h5py.string_dtype(encoding='ascii'), data=images)
    hdf5_file.create_dataset('images', (images_num, ), dtype=h5py.string_dtype(encoding='ascii'), data=images)
    hdf5_file.create_dataset('original_shape', (images_num, 3), dtype=np.uint16, compression="gzip", compression_opts=0,
                             data=shapes)
    hdf5_file.create_dataset('boxes', (images_num, ), h5py.vlen_dtype(np.dtype('float16')), compression='gzip',
                             compression_opts=0, data=boxes)
    hdf5_file.create_dataset('labels', (images_num, ), h5py.vlen_dtype(np.dtype('uint16')), compression='gzip',
                             compression_opts=0, data=labels)
    hdf5_file.create_dataset('paths', (images_num,), h5py.string_dtype(), compression='gzip',
                             compression_opts=0, data=paths)
    hdf5_file.close()

