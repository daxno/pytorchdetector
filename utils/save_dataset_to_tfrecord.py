import os
import cv2
import json
import random
import numpy as np
import tensorflow as tf
from tqdm import tqdm


def _load_image(path):
    image = cv2.imread(path)
    if image is not None:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        return image.astype(np.float32)
    return None




def _build_examples_list(input_folder, seed):
    examples = []
    for classname in os.listdir(input_folder):
        class_dir = os.path.join(input_folder, classname)
        if (os.path.isdir(class_dir)):
            for filename in os.listdir(class_dir):
                filepath = os.path.join(class_dir, filename)
                example = {
                    'classname': classname,
                    'path': filepath
                }
                examples.append(example)

    random.seed(seed)
    random.shuffle(examples)
    return examples


def decode_detections(detections, image_path):
    # boxes, labels = np.zeros((max_dets_num, 4)), np.zeros((max_dets_num, 1)) - 1
    # for idx, det in enumerate(detections):
    image_h, image_w = cv2.imread(image_path).shape[:2]
    num_detections = len(detections)
    boxes = np.array([[box['x']/image_w, box['y']/image_h, box['w']/image_w, box['h']/image_h]
                      for box in detections]).flatten()
    # labels = [str.encode(box['class']) for box in detections]
    return boxes, num_detections


def _int64_feature(feature):
    if isinstance(feature, np.ndarray):
        feature = feature.tolist()
    if not isinstance(feature, list):
        feature = [feature]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=feature))


def _bytes_feature(feature):
    if isinstance(feature, np.ndarray):
        feature = feature.tolist()
    if not isinstance(feature, list):
        feature = [feature]
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=feature))


def _float_feature(feature):
    if isinstance(feature, np.ndarray):
        feature = feature.tolist()
    if not isinstance(feature, list):
        feature = [feature]
    return tf.train.Feature(float_list=tf.train.FloatList(value=feature))


def _feature_list(features):
    assert isinstance(features, list) and isinstance(features[0], tf.train.Feature), \
        f'Expected list of Features, got: {type(features)} of {type(features[0])}'
    return tf.train.FeatureList(feature=features)


def serialize_example(image_raw, image_path: str, boxes: list, num_detections: int):
    image_h, image_w = cv2.imread(image_path).shape[:2]
    # context = {
    #     'raw_image': _bytes_feature(image_raw),
    #     'image_height': _int64_feature(image_h),
    #     'image_width': _int64_feature(image_w),
    #     'image_path': _bytes_feature(str.encode(image_path))
    # }

    feature = {
        'raw_image': _bytes_feature(image_raw),
        'image_height': _int64_feature(image_h),
        'image_width': _int64_feature(image_w),
        'image_path': _bytes_feature(str.encode(image_path)),
        'boxes': _float_feature(boxes),
        'num_detections': _int64_feature(num_detections)

    }

    example_proto = tf.train.Example(
        features=tf.train.Features(feature=feature),
        # feature_lists=tf.train.FeatureLists(feature_list=feature)
    )
    return example_proto.SerializeToString()


def _parse_sequence_example(example_proto):
    context_features = {
        'raw_image': tf.io.VarLenFeature(dtype=tf.string),
        'image_height': tf.io.FixedLenFeature([], dtype=tf.int64),
        'image_width': tf.io.FixedLenFeature([], dtype=tf.int64),
        'image_path': tf.io.VarLenFeature(dtype=tf.string)
    }
    seq_features = {
        'boxes': tf.io.VarLenFeature(dtype=tf.float32),
        'labels': tf.io.VarLenFeature(dtype=tf.string)
    }

    return tf.io.parse_single_sequence_example(example_proto, context_features, seq_features)


def _parse_example(example_proto):
    features = {
        'raw_image': tf.io.VarLenFeature(dtype=tf.string),
        'image_height': tf.io.FixedLenFeature([], dtype=tf.int64),
        'image_width': tf.io.FixedLenFeature([], dtype=tf.int64),
        'image_path': tf.io.VarLenFeature(dtype=tf.string),
        'boxes': tf.io.VarLenFeature(dtype=tf.float32),
        'num_detections': tf.io.FixedLenFeature([], dtype=tf.int64)
    }

    return tf.io.parse_single_example(example_proto, features)


if __name__ == '__main__':
    annotations = json.load(open('/home/nlab/Experiments/Detector/resources/coco_FAKE_DETS.json'))
    # max_dets_num = max([len(dets) for dets in annotations.values()])
    # print('Max dets num:', max_dets_num)

    output_filename = f'/home/nlab/Experiments/Detector/resources/images.tfrec'

    # Writing
    # with tf.io.TFRecordWriter(output_filename) as writer:
    #     for image_path, detections in tqdm(annotations.items()):
    #         boxes, num_detections = decode_detections(detections, image_path)
    #         serialized_example = serialize_example(open(image_path, 'rb').read(), image_path, boxes, num_detections)
    #         # serialized_example = serialize_example(image_path, boxes, labels)
    #         writer.write(serialized_example)

    # Reading
    dataset = tf.data.TFRecordDataset(output_filename)

    parsed_dataset = dataset.map(_parse_example)
    for idx, features in tqdm(enumerate(parsed_dataset)):
        # print('Context features:', features)
        # print('Sequence features:', seq_features.keys())
        image_raw = features['raw_image']

        image_h, image_w = features['image_height'].numpy(), features['image_width'].numpy()
        # print(image_h, image_w)
        num_detections = features['num_detections'].numpy()
        # print(num_detections)
        boxes = features['boxes'].values.numpy().reshape((num_detections, 4))
        # print(boxes)
        # image_original = cv2.imread(image_path)
        image_flat = np.frombuffer(image_raw.values[0].numpy(), np.uint8)
        image = cv2.imdecode(image_flat, cv2.IMREAD_COLOR)
        for box in boxes:
            x, y, w, h = list(map(int, [box[0]*image_w, box[1]*image_h, box[2]*image_w, box[3]*image_h]))
            image = cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

        counter = idx
        # cv2.imshow('image', image)
        # # cv2.imshow('image_orig', image_original)
        #
        # if cv2.waitKey() == 27:
        #     break
    print(counter)
