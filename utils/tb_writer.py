from torch.utils.tensorboard import SummaryWriter
import torch


class TBWriter:
    def __init__(self, cfg, root):
        self.writer = SummaryWriter(log_dir=root)
        self.mean, self.std = torch.tensor(cfg.DATA.MEAN), torch.tensor(cfg.DATA.STD)
        if self.mean.max() >= 1: self.mean = self.mean / 255.
        if self.std.max() >= 1: self.std = self.std / 255.

    def add_scalar(self, name, value, global_step):
        self.writer.add_scalar(name, scalar_value=value, global_step=global_step)

    def add_scalars(self, items, global_step):
        for name, value in items.items():
            self.add_scalar(name, value, global_step)

    def add_images(self, images, boxes, global_step):
        """
        :param images: np.array or torch.Tensor
        :param boxes: np.array or torch.Tensor (in xyxy format, unnormalized)
        :return:
        """
        images = ((images.to(torch.device('cpu')).permute(0, 2, 3, 1) * self.std) + self.mean)
        for image, image_boxes in zip(images, boxes):
            self.writer.add_image_with_boxes('predictions', image.permute((2, 0, 1)), image_boxes,
                                             global_step=global_step, dataformats='CHW')
