import numpy as np
import torch


class Meter:
    def __init__(self):
        self.metrics = {}

    def get_names(self):
        return list(self.metrics.keys())

    def get_by_name(self, name, reduction='mean'):
        if reduction == 'mean':
            return np.mean(self.metrics[name])
        elif reduction == 'sum':
            return np.sum(self.metrics[name])
        else: raise RuntimeError(f'Unrecognized reduction method: {reduction}')

    def get(self, reduction='mean'):
        if reduction == 'mean':
            return {name: np.mean(val) for name, val in self.metrics.items()}
        elif reduction == 'sum':
            return {name: np.sum(val) for name, val in self.metrics.items()}
        else: raise RuntimeError(f'Unrecognized reduction method: {reduction}')

    def reset(self):
        for name in self.metrics.keys():
            self.metrics[name] = []

    def update(self, items):
        for name, val in items.items():
            if isinstance(val, torch.Tensor): val = val.data.cpu().numpy()
            if name not in self.metrics.keys():
                self.metrics.update({name: [val]})
            else: self.metrics[name].append(val)
