import json
from tqdm import tqdm

# REQUIRED FIELDS
# {
#     "images": [{
#         "id" : int,
#         "file_name" : str
#     }],
#     "annotations": [{
#         "id" : int,
#         "image_id" : int,
#         "category_id" : int,
#         "bbox" : [x, y, w, h]
#     }],
#     "categories": [{
#         "id" : int
#     ]}
# }

if __name__ == '__main__':
    obj_365_annot = json.load(open('/home/nlab/Experiments/Detector/resources/obj_365_train_person.json'))

    obj_365_coco_format = {
        "categories": [{
            "id": 0,
            "name": "person"
        }],
        "images": [],
        "annotations": []
    }

    for idx, image_path in tqdm(enumerate(obj_365_annot.keys()), total=len(obj_365_annot.keys())):
        file_name = image_path.split('/')[-1]
        obj_365_coco_format["images"].append({"id": idx, "file_name": file_name})
        for det_idx, det in enumerate(obj_365_annot[image_path]):
            x, y, w, h = max(0, det['x']), max(0, det['y']), max(0, det['w']), max(0, det['h'])
            obj_365_coco_format["annotations"].append({
                "id": det_idx,
                "image_id": idx,
                "category_id": 0,
                "bbox": [x, y, w, h]
            })
    json.dump(obj_365_coco_format, open('/home/nlab/Experiments/Detector/resources/obj_365_train_coco_format.json', 'w'))

