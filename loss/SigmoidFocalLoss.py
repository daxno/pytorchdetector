import torch
from torch import nn
from torch.autograd import Function
from torch.autograd.function import once_differentiable

from c_sources import _C


class _SigmoidFocalLossCPU:
    def __init__(self, gamma, alpha):
        self.gamma, self.alpha = gamma, alpha

    def apply(self, logits, targets):
        num_classes = logits.shape[1]
        gamma = self.gamma  # [0]
        alpha = self.alpha  # [0]
        dtype = targets.dtype
        device = targets.device
        class_range = torch.arange(1, num_classes + 1, dtype=dtype, device=device).unsqueeze(0)
        t = targets  # .unsqueeze(1)
        p = torch.sigmoid(logits)
        term1 = ((1 - p) ** gamma * torch.log(p)).to(device)
        term2 = (p ** gamma * torch.log(1 - p)).to(device)
        ret_loss = -(t == class_range).type(dtype) * term1.to(device) * alpha - \
                   ((t != class_range) * (t >= 0)).type(dtype) * term2.to(device) * (1 - alpha)
        return ret_loss


class _SigmoidFocalLossCUDA(Function):
    @staticmethod
    def forward(ctx, logits, targets, gamma, alpha):
        ctx.save_for_backward(logits, targets)
        num_classes = logits.shape[1]
        ctx.num_classes = num_classes
        ctx.gamma = gamma
        ctx.alpha = alpha

        losses = _C.sigmoid_focalloss_forward(
            logits, targets, num_classes, gamma, alpha
        )
        return losses

    @staticmethod
    @once_differentiable
    def backward(ctx, d_loss):
        logits, targets = ctx.saved_tensors
        num_classes = ctx.num_classes
        gamma = ctx.gamma
        alpha = ctx.alpha
        d_loss = d_loss.contiguous()
        d_logits = _C.sigmoid_focalloss_backward(
            logits, targets, d_loss, num_classes, gamma, alpha
        )
        return d_logits, None, None, None, None


class SigmoidFocalLoss(nn.Module):
    def __init__(self, gamma, alpha):
        super(SigmoidFocalLoss, self).__init__()
        self.gamma = gamma
        self.alpha = alpha
        self.focal_loss_cpu = _SigmoidFocalLossCPU(self.gamma, self.alpha)
        self.loss_fn_cpu = self.focal_loss_cpu.apply
        self.sigmoid_focal_loss_cuda = _SigmoidFocalLossCUDA()
        self.loss_fn_cuda = self.sigmoid_focal_loss_cuda.apply

    def forward(self, logits, targets):
        # TODO fix (Error Float16 is not implemented for SigmoidFocalLossCUDA)
        # if logits.is_cuda:
        #     return self.loss_fn_cuda(logits, targets, self.gamma, self.alpha).sum()
        #     # raise NotImplementedError('CUDA SigmoidFocalLoss computing is not implemented')
        # else:
        #     return self.loss_fn_cpu(logits, targets).sum()
        # if logits.is_cuda():
        #     logits = logits.detach()
        #     targets = targets.detach()
        cls_loss = self.loss_fn_cpu(logits, targets).sum()
        return cls_loss.sum()
        # return self.loss_fn_cuda(logits, targets, self.gamma, self.alpha).sum()
