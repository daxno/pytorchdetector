from .tensorrt_layers import convert_iterpolate
from .anchor_generator import AnchorGenerator
from .initialization import weight_init
from .scale import Scale


def get_anchor_generator_atss(cfg):
    anchor_sizes = cfg.MODEL.ATSS.ANCHOR_SIZES
    aspect_ratios = cfg.MODEL.ATSS.ASPECT_RATIOS
    anchor_strides = cfg.MODEL.ATSS.ANCHOR_STRIDES
    straddle_thresh = cfg.MODEL.ATSS.STRADDLE_THRESH
    octave = cfg.MODEL.ATSS.OCTAVE
    scales_per_octave = cfg.MODEL.ATSS.SCALES_PER_OCTAVE

    assert len(anchor_strides) == len(anchor_sizes), "Only support FPN now"
    new_anchor_sizes = []
    for size in anchor_sizes:
        per_layer_anchor_sizes = []
        for scale_per_octave in range(scales_per_octave):
            octave_scale = octave ** (scale_per_octave / float(scales_per_octave))
            per_layer_anchor_sizes.append(octave_scale * size)
        new_anchor_sizes.append(tuple(per_layer_anchor_sizes))

    anchor_generator = AnchorGenerator(
        tuple(new_anchor_sizes), aspect_ratios, anchor_strides, straddle_thresh
    )
    return anchor_generator


def get_anchor_generator_retinanet(cfg):
    anchor_sizes = cfg.MODEL.RETINANET.ANCHOR_SIZES
    aspect_ratios = cfg.MODEL.RETINANET.ASPECT_RATIOS
    anchor_strides = cfg.MODEL.RETINANET.ANCHOR_STRIDES
    straddle_thresh = cfg.MODEL.RETINANET.STRADDLE_THRESH
    octave = cfg.MODEL.RETINANET.OCTAVE
    scales_per_octave = cfg.MODEL.RETINANET.SCALES_PER_OCTAVE

    assert len(anchor_strides) == len(anchor_sizes), "Only support FPN now"
    new_anchor_sizes = []
    for size in anchor_sizes:
        per_layer_anchor_sizes = []
        for scale_per_octave in range(scales_per_octave):
            octave_scale = octave ** (scale_per_octave / float(scales_per_octave))
            per_layer_anchor_sizes.append(octave_scale * size)
        new_anchor_sizes.append(tuple(per_layer_anchor_sizes))

    anchor_generator = AnchorGenerator(
        tuple(new_anchor_sizes), aspect_ratios, anchor_strides, straddle_thresh
    )
    return anchor_generator
