from torch2trt.torch2trt import *

# Override concatenation layer
# @tensorrt_converter('torch.cat')
# def convert_cat(ctx):
#     inputs = ctx.method_args[0]
#
#     if 'dim' in ctx.method_kwargs: dim = ctx.method_kwargs['dim']
#     else: dim = ctx.method_args[1]
#
#     output = ctx.method_return
#     trt_inputs = [trt_(ctx.network, i) for i in inputs]
#
#     layer = ctx.network.add_concatenation(inputs=trt_inputs)
#     layer.axis = dim if len(trt_inputs[0].shape) == 4 else dim - 1
#     output._trt = layer.get_output(0)


# @tensorrt_converter('torch.nn.functional.avg_pool2d')
# def convert_avg_pool2d(ctx):
#     # parse args
#     input = get_arg(ctx, 'input', pos=0, default=None)
#     kernel_size = get_arg(ctx, 'kernel_size', pos=1, default=None)
#     stride = get_arg(ctx, 'stride', pos=2, default=None)
#     padding = get_arg(ctx, 'padding', pos=3, default=0)
#     ceil_mode = get_arg(ctx, 'ceil_mode', pos=4, default=False)
#     count_include_pad = get_arg(ctx, 'count_include_pad', pos=5, default=True)
#
#     # get input trt tensor (or create constant if it doesn't exist)
#     input_trt = trt_(ctx.network, input)
#     print('Input shape in AvgPooling2d:', input_trt.shape)
#     output = ctx.method_return
#
#     # get kernel size
#     if not isinstance(kernel_size, tuple):
#         kernel_size = (kernel_size,) * 2
#
#     # get stride
#     if not isinstance(stride, tuple):
#         stride = (stride,) * 2
#
#     # get padding
#     if not isinstance(padding, tuple):
#         padding = (padding,) * 2
#
#     layer = ctx.network.add_pooling(
#         input=input_trt, type=trt.PoolingType.AVERAGE, window_size=kernel_size)
#
#     layer.stride = stride
#     layer.padding = padding
#     layer.average_count_excludes_padding = not count_include_pad
#
#     if ceil_mode:
#         layer.padding_mode = trt.PaddingMode.EXPLICIT_ROUND_UP
#
#     output._trt = layer.get_output(0)


@tensorrt_converter('torch.nn.functional.interpolate')
def convert_iterpolate(ctx):
    input, _, scale_factor, mode, _ = ctx.method_args
    output = ctx.method_return

    # print(inputs.shape)
    # print(f'Scale factor: {scale_factor}')
    # print(f'Mode: {mode}')
    resize_mode = trt.ResizeMode.LINEAR if mode in ['linear', 'bilinear'] else trt.ResizeMode.NEAREST
    # trt_inputs = [trt_(ctx.network, i) for i in inputs]
    layer = ctx.network.add_resize(input=trt_(ctx.network, input))
    layer.scales = [1.0, scale_factor, scale_factor]
    layer.resize_mode = resize_mode
    output._trt = layer.get_output(0)
