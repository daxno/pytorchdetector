#! /bin/bash

# Install COCOAPI
if [[ ! -d "$PWD/cocoapi" ]]
then
    git clone https://github.com/cocodataset/cocoapi.git
fi
cd cocoapi/PythonAPI
python setup.py build_ext install
cd ../..

# Install APEX
git clone https://github.com/NVIDIA/apex
cd apex
pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./
cd ..

# Install detector
python setup.py build develop --no-deps