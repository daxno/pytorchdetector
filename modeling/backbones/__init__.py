from torchvision.models import resnet as vrn
from .ShuffleNetV2 import shufflenet_v2_x1
from modeling.HBONet.HBONet import HBONet
from .SqueezeNet import squeezenet1_0
from .MobileNetV3 import MobileNetV3
from .MobileNetV2 import MobileNetV2
from .MobileNetV1 import MobileNetV1
from .HarDNet import hardnet39ds
from .FPN import FPN, FPNLight
from .ResNet import ResNet
import torch


def get_resnet18c4(cfg):
    return ResNet(layers=[2, 2, 2, 2], bottleneck=vrn.BasicBlock, outputs=[4], url=vrn.model_urls['resnet18'])


def get_resnet34c4(cfg):
    return ResNet(layers=[3, 4, 6, 3], bottleneck=vrn.BasicBlock, outputs=[4], url=vrn.model_urls['resnet34'])


def get_resnet18_fpn(cfg):
    backbone = ResNet(layers=[2, 2, 2, 2],
                      bottleneck=vrn.BasicBlock,
                      outputs=[3, 4, 5],
                      url=vrn.model_urls['resnet18'])
    return FPN(backbone=backbone, in_channels=[128, 256, 512], out_channels=[256]*5)


def get_resnet34_fpn(cfg):
    backbone = ResNet(layers=[3, 4, 6, 3],
                      bottleneck=vrn.BasicBlock,
                      outputs=[3, 4, 5],
                      url=vrn.model_urls['resnet34'])
    return FPN(backbone=backbone, in_channels=[128, 256, 512], out_channels=[256]*5)


def get_resnet50_fpn(cfg):
    backbone = ResNet(layers=[3, 4, 6, 3],
                      bottleneck=vrn.Bottleneck,
                      outputs=[3, 4, 5],
                      url=vrn.model_urls['resnet50'])
    return FPN(backbone, in_channels=[512, 1024, 2048], out_channels=[256]*5)


def get_resnet101_fpn(cfg):
    backbone = ResNet(layers=[3, 4, 23, 3],
                      bottleneck=vrn.Bottleneck,
                      outputs=[3, 4, 5],
                      url=vrn.model_urls['resnet101'])
    return FPN(backbone, in_channels=[512, 1024, 2048], out_channels=[256]*5)


def get_resnet152_fpn(cfg):
    backbone = ResNet(layers=[3, 8, 36, 3],
                      bottleneck=vrn.Bottleneck,
                      outputs=[3, 4, 5],
                      url=vrn.model_urls['resnet152'])
    return FPN(backbone, in_channels=[512, 1024, 2048], out_channels=[256]*5)


def get_resnet_custom(cfg):
    return ResNet(layers=cfg.MODEL.BACKBONE.RESNET.LAYERS,
                  bottleneck=vrn.Bottleneck if cfg.MODEL.BACKBONE.RESNET.BOTTLENECK == 'Bottleneck' else vrn.BasicBlock,
                  outputs=cfg.MODEL.BACKBONE.RESNET.OUTPUTS,
                  url=vrn.model_urls.get(f'resnet{cfg.RESNET.NUMBER}', None))


def get_resnetfpn_custom(cfg):
    backbone = get_resnet_custom(cfg.RESNET)
    return FPN(backbone, cfg.MODEL.BACKBONE.FPN.IN_CHANNELS, cfg.MODEL.BACKBONE.FPN.OUT_CHANNELS)


def get_mobilenetv1(cfg):
    return MobileNetV1('MobileNetV1', out_layers=(5, 11, 13), width_mult=cfg.MODEL.BACKBONE.MOBILENETV1.WIDTH_MULT,
                       norm_layer=cfg.MODEL.BACKBONE.MOBILENETV1.NORM_LAYER)


def get_mobilenetv1_fpn(cfg):
    backbone = get_mobilenetv1(cfg)
    wm = cfg.MODEL.BACKBONE.MOBILENETV1.WIDTH_MULT
    return FPN(backbone=backbone, in_channels=(int(256*wm), int(512*wm), int(1024*wm)),
               out_channels=cfg.MODEL.BACKBONE.FPN.OUT_CHANNELS)


def get_mobilenetv1_fpnlight(cfg):
    backbone = get_mobilenetv1(cfg)
    wm = cfg.MODEL.BACKBONE.MOBILENETV1.WIDTH_MULT
    return FPNLight(backbone=backbone, in_channels=(int(256*wm), int(512*wm), int(1024*wm)),
                    out_channels=cfg.MODEL.BACKBONE.FPN.OUT_CHANNELS[:3])


def get_mobilenetv2(cfg):
    return MobileNetV2(name='MobileNetV2', out_layers=(6, 13, 18), width_mult=cfg.MODEL.BACKBONE.MOBILENETV2.WIDTH_MULT)


def get_mobilenetv2_fpn(cfg):
    backbone = get_mobilenetv2(cfg)
    return FPN(backbone=backbone, in_channels=(32, 96, 1280), out_channels=cfg.MODEL.BACKBONE.FPN.OUT_CHANNELS)


def get_mobilenetv3_small(cfg):
    cfgs = [
        # k, t, c, SE, NL, s
        [3, 16, 16, 1, 0, 2],
        [3, 72, 24, 0, 0, 2],
        [3, 88, 24, 0, 0, 1],
        [5, 96, 40, 1, 1, 2],
        [5, 240, 40, 1, 1, 1],
        [5, 240, 40, 1, 1, 1],
        [5, 120, 48, 1, 1, 1],
        [5, 144, 48, 1, 1, 1],
        [5, 288, 96, 1, 1, 2],
        [5, 576, 96, 1, 1, 1],
        [5, 576, 96, 1, 1, 1],
    ]
    model = MobileNetV3('MobileNetV3Small', (3, 8, 13), cfgs, mode='small',
                        width_mult=cfg.MODEL.BACKBONE.MOBILENETV3.WIDTH_MULT)
    wm = cfg.MODEL.BACKBONE.MOBILENETV3.WIDTH_MULT
    if cfg.MODEL.BACKBONE.PRETRAINED:
        model.load_state_dict(torch.load(cfg.PATHS.BACKBONES_WEIGHTS_PATH+f'mobilenetv3_small_{wm}.pth'), strict=False)
        print('MobileNetV3 weights loaded')
    return model


def get_mobilenetv3_small_fpn(cfg):
    backbone = get_mobilenetv3_small(cfg)
    wm = cfg.MODEL.BACKBONE.MOBILENETV3.WIDTH_MULT
    in_channels_for_wm = {
        0.5: (16, 24, 288),
        1: (24, 48, 576),
        2: (48, 96, 1152),
        3: (72, 144, 1728)
    }
    return FPN(backbone=backbone, in_channels=in_channels_for_wm[wm], out_channels=cfg.MODEL.BACKBONE.FPN.OUT_CHANNELS)


def get_mobilenetv3_large(cfg):
    cfgs = [
        # k, t, c, SE, NL, s
        [3,  16,  16, 0, 0, 1],
        [3,  64,  24, 0, 0, 2],
        [3,  72,  24, 0, 0, 1],
        [5,  72,  40, 1, 0, 2],
        [5, 120,  40, 1, 0, 1],
        [5, 120,  40, 1, 0, 1],
        [3, 240,  80, 0, 1, 2],
        [3, 200,  80, 0, 1, 1],
        [3, 184,  80, 0, 1, 1],
        [3, 184,  80, 0, 1, 1],
        [3, 480, 112, 1, 1, 1],
        [3, 672, 112, 1, 1, 1],
        [5, 672, 160, 1, 1, 1],
        [5, 672, 160, 1, 1, 2],
        [5, 960, 160, 1, 1, 1]
    ]
    wm = cfg.MODEL.BACKBONE.MOBILENETV3.WIDTH_MULT
    # if wm != 1.0: raise NotImplementedError('MobileNetV3 with FPN not implemented for wight multiplier != 1')
    model = MobileNetV3('MobileNetV3Large', (6, 13, 17), cfgs, mode='large', width_mult=wm)
    if cfg.MODEL.BACKBONE.PRETRAINED:
        model.load_state_dict(torch.load(cfg.PATHS.BACKBONES_WEIGHTS_PATH+f'mobilenetv3_large_{wm}.pth'), strict=False)
        print('MobileNetV3 weights loaded')
    return model


def get_mobilenetv3_large_fpn(cfg):
    backbone = get_mobilenetv3_large(cfg)
    wm = cfg.MODEL.BACKBONE.MOBILENETV3.WIDTH_MULT
    in_channels_for_wm = {
        0.5: (24, 80, 480),
        1: (40, 160, 960),
        2: (80, 320, 1920),
    }
    return FPN(backbone=backbone, in_channels=in_channels_for_wm[wm], out_channels=cfg.MODEL.BACKBONE.FPN.OUT_CHANNELS)


def get_hardnet39ds(cfg):
    return hardnet39ds(cfg)


def get_shufflenetv2(cfg):
    return shufflenet_v2_x1(cfg)


def get_shufflenetv2_fpn(cfg):
    backbone = shufflenet_v2_x1(cfg)
    model = FPN(backbone, in_channels=(116, 232, 1024), out_channels=cfg.MODEL.BACKBONE.FPN.OUT_CHANNELS)
    return model


def get_hbonet(cfg):
    model = HBONet('HBONet', out_layers=[9, 15, 18], width_mult=cfg.MODEL.BACKBONE.HBONET.WM)
    if cfg.MODEL.BACKBONE.PRETRANED:
        model.load_state_dict(torch.load(f'{cfg.PATHS.BACKBONES_WEIGHTS_PATH}/hbonet_{cfg.MODEL.BACKBONE.HBONET.WM}.pth'),
                              strict=False)
    return model


def get_hbonet_fpn(cfg):
    backbone = get_hbonet(cfg)
    wm = cfg.MODEL.BACKBONE.HBONET.WM
    if wm == 1: in_channels = (96, 144, 400)
    elif wm == 0.8: in_channels = (80, 112, 320)
    elif wm == 0.5: in_channels = int(96*wm), int(144*wm), int(400*wm)
    else: raise RuntimeError(f'Unsupported WM value: {wm}. (Possible values: 1., 0.8, 0.5)')
    model = FPN(backbone, in_channels=in_channels, out_channels=cfg.MODEL.BACKBONE.FPN.OUT_CHANNELS)
    return model
