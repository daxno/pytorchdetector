from torch import nn
from layers import weight_init
import torch.nn.functional as F


class FPN(nn.Module):
    def __init__(self, backbone, in_channels, out_channels):
        super().__init__()
        # TODO работает только с 5 слоями, пофиксить
        # TODO сломается если в out_channels[-2], out_channels[-1] передать разное количество каналов

        self.stride = 128
        self.backbone = backbone
        self.lateral3 = nn.Conv2d(in_channels[0], 256, 1)
        self.lateral4 = nn.Conv2d(in_channels[1], 256, 1)
        self.lateral5 = nn.Conv2d(in_channels[2], 256, 1)
        self.pyramid6 = nn.Conv2d(in_channels[2], 256, 3, stride=2, padding=1)
        self.pyramid7 = nn.Conv2d(256, out_channels[4], 3, stride=2, padding=1)
        self.smooth3 = nn.Conv2d(256, out_channels[0], 3, padding=1)
        self.smooth4 = nn.Conv2d(256, out_channels[1], 3, padding=1)
        self.smooth5 = nn.Conv2d(256, out_channels[2], 3, padding=1)
        self.p4_up = nn.Upsample(scale_factor=2, mode='bilinear')
        self.p5_up = nn.Upsample(scale_factor=2, mode='bilinear')
        self.initialize()

    def initialize(self):
        layers_to_init = [self.lateral3, self.lateral4, self.lateral5, self.pyramid6, self.pyramid7,
                          self.smooth3, self.smooth4, self.smooth5]
        for module in layers_to_init:
            module.apply(weight_init)
        print('FPN initialized')

    def forward(self, x):
        c3, c4, c5 = self.backbone(x)
        p5 = self.lateral5(c5)

        p4 = self.lateral4(c4)
        p4 = self.p5_up(p5) + p4
        p3 = self.lateral3(c3)
        p3 = self.p4_up(p4) + p3

        p6 = self.pyramid6(c5)
        p7 = self.pyramid7(F.relu(p6))

        p3 = self.smooth3(p3)
        p4 = self.smooth4(p4)
        p5 = self.smooth5(p5)
        return [p3, p4, p5, p6, p7]


class FPNLight(nn.Module):
    def __init__(self, backbone, in_channels, out_channels):
        super(FPNLight, self).__init__()
        self.backbone = backbone

        self.convs = nn.ModuleList([
            nn.Conv2d(in_ch, out_ch, kernel_size=1) for in_ch, out_ch in zip(in_channels, out_channels)])

    def initialize(self):
        self.apply(weight_init)
        self.backbone.initialize()

    def forward(self, x):
        c3, c4, c5 = self.backbone(x)
        return [conv(feature) for conv, feature in zip(self.convs, [c3, c4, c5])]
