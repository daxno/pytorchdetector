import torch
from torch import nn
from layers.initialization import weight_init


class BackboneAbstract(nn.Module):
    def __init__(self, name, out_layers=None):
        super(BackboneAbstract, self).__init__()

        self.name = name
        self.out_layers = out_layers

    def initialize(self):
        raise NotImplementedError('initialize method should be overridden in child class')

    def extract_features(self, x):
        raise NotImplementedError('extract_features method should be overridden in child class')

    def forward(self, x):
        return self.extract_features(x)

    def load_weights(self, path):
        self.load_state_dict(torch.load(path), strict=False)

