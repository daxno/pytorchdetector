import torch
import torch.nn as nn
from modeling.backbones.BackboneAbstract import BackboneAbstract
try:
    from torch.hub import load_state_dict_from_url
except ImportError:
    from torch.utils.model_zoo import load_url as load_state_dict_from_url


__all__ = [
    'ShuffleNetV2', 'shufflenet_v2_x0_5', 'shufflenet_v2_x1',
    'shufflenet_v2_x1_5', 'shufflenet_v2_x2_0'
]

model_urls = {
    'shufflenetv2_x0.5': 'https://download.pytorch.org/models/shufflenetv2_x0.5-f707e7126e.pth',
    'shufflenetv2_x1.0': 'https://download.pytorch.org/models/shufflenetv2_x1-5666bf0f80.pth',
    'shufflenetv2_x1.5': None,
    'shufflenetv2_x2.0': None,
}


def channel_shuffle(x, groups):
    # type: (torch.Tensor, int) -> torch.Tensor
    batchsize, num_channels, height, width = x.data.size()
    channels_per_group = num_channels // groups

    # reshape
    x = x.view(batchsize, groups,
               channels_per_group, height, width)

    x = torch.transpose(x, 1, 2).contiguous()

    # flatten
    x = x.view(batchsize, -1, height, width)

    return x


class InvertedResidual(nn.Module):
    def __init__(self, inp, oup, stride, norm_layer='batch_norm'):
        super(InvertedResidual, self).__init__()

        if not (1 <= stride <= 3):
            raise ValueError('illegal stride value')
        self.stride = stride

        branch_features = oup // 2
        assert (self.stride != 1) or (inp == branch_features << 1)

        if self.stride > 1:
            self.branch1 = nn.Sequential(
                self.depthwise_conv(inp, inp, kernel_size=3, stride=self.stride, padding=1),
                nn.BatchNorm2d(inp) if norm_layer == 'batch_norm' else nn.GroupNorm(32, inp),
                nn.Conv2d(inp, branch_features, kernel_size=1, stride=1, padding=0, bias=False),
                nn.BatchNorm2d(branch_features) if norm_layer == 'batch_norm' else nn.GroupNorm(32, branch_features),
                nn.ReLU(inplace=True),
            )
        else:
            self.branch1 = nn.Sequential()

        self.branch2 = nn.Sequential(
            nn.Conv2d(inp if (self.stride > 1) else branch_features,
                      branch_features, kernel_size=1, stride=1, padding=0, bias=False),
            nn.BatchNorm2d(branch_features) if norm_layer == 'batch_norm' else nn.GroupNorm(32, branch_features),
            nn.ReLU(inplace=True),
            self.depthwise_conv(branch_features, branch_features, kernel_size=3, stride=self.stride, padding=1),
            nn.BatchNorm2d(branch_features) if norm_layer == 'batch_norm' else nn.GroupNorm(32, branch_features),
            nn.Conv2d(branch_features, branch_features, kernel_size=1, stride=1, padding=0, bias=False),
            nn.BatchNorm2d(branch_features) if norm_layer == 'batch_norm' else nn.GroupNorm(32, branch_features),
            nn.ReLU(inplace=True),
        )

    @staticmethod
    def depthwise_conv(i, o, kernel_size, stride=1, padding=0, bias=False):
        return nn.Conv2d(i, o, kernel_size, stride, padding, bias=bias, groups=i)

    def forward(self, x):
        if self.stride == 1:
            x1, x2 = x.chunk(2, dim=1)
            out = torch.cat((x1, self.branch2(x2)), dim=1)
        else:
            out = torch.cat((self.branch1(x), self.branch2(x)), dim=1)

        out = channel_shuffle(out, 2)

        return out


class ShuffleNetV2(BackboneAbstract):
    def __init__(self, name, out_layers, stages_repeats, stages_out_channels, inverted_residual=InvertedResidual,
                 norm_layer='batch_norm'):
        super(ShuffleNetV2, self).__init__(name=name, out_layers=out_layers)

        if len(stages_repeats) != 3:
            raise ValueError('expected stages_repeats as list of 3 positive ints')
        if len(stages_out_channels) != 5:
            raise ValueError('expected stages_out_channels as list of 5 positive ints')
        self._stage_out_channels = stages_out_channels

        input_channels = 3
        output_channels = self._stage_out_channels[0]
        self.conv1 = nn.Sequential(
            nn.Conv2d(input_channels, output_channels, 3, 2, 1, bias=False),
            nn.BatchNorm2d(output_channels) if norm_layer == 'batch_norm' else nn.GroupNorm(32, output_channels),
            nn.ReLU(inplace=True),
        )
        input_channels = output_channels

        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        stage_names = ['stage{}'.format(i) for i in [2, 3, 4]]
        for name, repeats, output_channels in zip(
                stage_names, stages_repeats, self._stage_out_channels[1:]):
            seq = [inverted_residual(input_channels, output_channels, 2, norm_layer)]
            for i in range(repeats - 1):
                seq.append(inverted_residual(output_channels, output_channels, 1, norm_layer))
            setattr(self, name, nn.Sequential(*seq))
            input_channels = output_channels

        output_channels = self._stage_out_channels[-1]
        self.conv5 = nn.Sequential(
            nn.Conv2d(input_channels, output_channels, 1, 1, 0, bias=False),
            nn.BatchNorm2d(output_channels) if norm_layer == 'batch_norm' else nn.GroupNorm(32, output_channels),
            nn.ReLU(inplace=True),
        )
        self.features = nn.Sequential(
            self.conv1,
            self.maxpool,
            self.stage2,
            self.stage3,
            self.stage4,
            self.conv5
        )

    def extract_features(self, x):
        features = []
        for idx, module in enumerate(self.features):
            x = module(x)
            if idx in self.out_layers:
                features.append(x)
        return features

    def forward(self, x):
        return self.extract_features(x)


def _shufflenetv2(arch, pretrained, progress, *args, **kwargs):
    model = ShuffleNetV2(*args, **kwargs)

    if pretrained:
        model_url = model_urls[arch]
        if model_url is None:
            raise NotImplementedError('pretrained {} is not supported as of now'.format(arch))
        else:
            state_dict = load_state_dict_from_url(model_url, progress=progress)
            model.load_state_dict(state_dict, strict=False)

    return model


def shufflenet_v2_x0_5(cfg):
    model = ShuffleNetV2(name='shufflenetv2_0.5', out_layers=(2, 3, 5), stages_repeats=(4, 8, 4),
                         stages_out_channels=(24, 48, 96, 192, 1024))
    if cfg.MODEL.BACKBONE.PRETRINED:
        model_path = f'{cfg.PATHS.BACKBONES_WEIGHTS_PATH}/shufflenetv2_x0.5.pth'
        state_dict = torch.load(model_path)
        model.load_state_dict(state_dict, strict=False)
    return model


def shufflenet_v2_x1(cfg):
    model = ShuffleNetV2(name='shufflenetv2_1.0', out_layers=(2, 3, 5), stages_repeats=(4, 8, 4),
                         stages_out_channels=(24, 116, 232, 464, 1024),
                         norm_layer=cfg.MODEL.BACKBONE.SHUFFLENET.NORM_LAYER)
    if cfg.MODEL.BACKBONE.PRETRINED:
        model_path = f'{cfg.PATHS.BACKBONES_WEIGHTS_PATH}/shufflenetv2_x1.pth'
        state_dict = torch.load(model_path)
        model.load_state_dict(state_dict, strict=False)
    return model


def shufflenet_v2_x1_5(pretrained=False, progress=True, **kwargs):
    raise NotImplementedError('')
    # return _shufflenetv2('shufflenetv2_x1.5', pretrained, progress,
    #                      [4, 8, 4], [24, 176, 352, 704, 1024], **kwargs)


def shufflenet_v2_x2_0(pretrained=False, progress=True, **kwargs):
    raise RuntimeError('')
    # return _shufflenetv2('shufflenetv2_x2.0', pretrained, progress,
    #                      [4, 8, 4], [24, 244, 488, 976, 2048], **kwargs)


if __name__ == '__main__':
    from config import cfg
    shuffnet = shufflenet_v2_x1(cfg).cuda()

    in_tensor = torch.zeros((1, 3, 768, 1024)).cuda()
    featu = shuffnet(in_tensor)
    for idx, f in enumerate(featu):
        print(idx, f.size())
