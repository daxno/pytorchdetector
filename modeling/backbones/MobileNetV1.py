import os
import torch
from torch import nn
from layers import weight_init
from modeling.backbones.BackboneAbstract import BackboneAbstract


class ConvBlock(torch.nn.Sequential):
    def __init__(self, in_channels, out_channels, kernel=1, stride=1, pad=0, num_group=1, active=True, relu6=False,
                 norm_layer='batch_norm'):
        layers = [
            torch.nn.Conv2d(in_channels, out_channels, kernel_size=kernel, stride=stride, padding=pad,
                            groups=num_group, bias=False)
        ]
        if norm_layer == 'batch_norm': layers.append(torch.nn.BatchNorm2d(out_channels))
        elif norm_layer == 'group_norm': layers.append(nn.GroupNorm(num_groups=32, num_channels=out_channels))
        else: raise RuntimeError(f'Unrecognized norm layer type: {norm_layer}')
        if active: layers.append(torch.nn.ReLU6(inplace=True) if relu6 else torch.nn.ReLU(inplace=True))
        super(ConvBlock, self).__init__(*layers)


class CondDepthWiseBlock(torch.nn.Sequential):
    def __init__(self, in_channels, out_channels, dw_channels, stride, relu6=False, norm_layer='batch_norm'):
        layers = [
            ConvBlock(in_channels=in_channels, out_channels=dw_channels, kernel=3, stride=stride,
                      pad=1, num_group=dw_channels, relu6=relu6, norm_layer=norm_layer),
            ConvBlock(in_channels=dw_channels, out_channels=out_channels, relu6=relu6, norm_layer=norm_layer)
        ]
        super(CondDepthWiseBlock, self).__init__(*layers)


class MobileNetV1(BackboneAbstract):
    def __init__(self, name, out_layers, width_mult=1., norm_layer='batch_norm'):
        super(MobileNetV1, self).__init__(name=name, out_layers=out_layers)

        self.stride = 32
        in_channels = 3
        layers = [
            ConvBlock(in_channels, out_channels=int(32 * width_mult), kernel=3, pad=1, stride=2, norm_layer=norm_layer)
        ]
        dw_channels = [int(x * width_mult) for x in [32, 64] + [128] * 2 + [256] * 2 + [512] * 6 + [1024]]
        out_channels = [int(x * width_mult) for x in [64] + [128] * 2 + [256] * 2 + [512] * 6 + [1024] * 2]
        strides = [1, 2] * 3 + [1] * 5 + [2, 1]
        in_channels = int(32*width_mult)
        for dwc, oc, s in zip(dw_channels, out_channels, strides):
            layers.append(CondDepthWiseBlock(in_channels, out_channels=oc, dw_channels=dwc,
                                             stride=s, norm_layer=norm_layer))
            in_channels = oc
        self.features = torch.nn.Sequential(*layers)

    def initialize(self):
        self.apply(weight_init)

    def extract_features(self, x):
        features = []
        for idx, module in enumerate(self.features):
            x = module(x)
            if idx in self.out_layers:
                features.append(x)
        return features

    def forward(self, x):
        return self.extract_features(x)


def mobilenetv1(cfg):
    model = MobileNetV1(width_mult=cfg.MODEL.BACKBONE.MOBILENETV1.WIDTH_MULT,
                        norm_layer=cfg.MODEL.BACKBONE.MOBILENETV1.NORM_LAYER,
                        name='mobilenetv1', out_layers=(5, 11, 13))
    if cfg.MODEL.BACKBONE.PRETRINED:
        weight_file = f'{cfg.PATHS.BACKBONES_WEIGHTS_PATH}/mobilenetv1.pth'
        if not os.path.isfile(weight_file):
            print(weight_file, 'is not found')
            exit(0)
        weights = torch.load(weight_file)
        model.load_state_dict(weights, strict=False)
    return model
