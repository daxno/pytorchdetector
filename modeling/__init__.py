from modeling.backbones import *
from .ATSS.ATSS import ATSS
from .MXFaceDetector.FaceDetector import FaceDetector


def get_retina_model():
    raise NotImplementedError()


def get_atss_model(cfg, backbone, **kwargs):
    return ATSS(cfg, backbone, **kwargs)


def get_ssd_face_detector_mxnet(cfg):
    return FaceDetector(cfg)
