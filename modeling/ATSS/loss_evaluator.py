import os
import torch
from torch import nn

from loss import SigmoidFocalLoss, GIoULoss
from structures import cat_boxlist, boxlist_iou

from .matcher import Matcher

INF = 1e9


class ATSSLossComputation:
    def __init__(self, cfg, box_coder, device):
        # super(ATSSLossComputation, self).__init__()
        self.device = device
        self.cfg = cfg
        self.box_coder = box_coder
        self.cls_loss = SigmoidFocalLoss(cfg.CRITERION.CLASSIFICATION.GAMMA, cfg.CRITERION.CLASSIFICATION.ALPHA)
        self.regr_loss = GIoULoss(box_coder)
        self.centerness_loss = nn.BCEWithLogitsLoss(reduction='sum')
        self.matcher = Matcher(cfg.MODEL.ATSS.FG_IOU_THRESHOLD, cfg.MODEL.ATSS.BG_IOU_THRESHOLD, True)

    def prepare_targets(self, targets, anchors):
        cls_labels = []
        reg_targets = []
        for im_i in range(len(targets)):
            targets_per_im = targets[im_i]
            assert targets_per_im.mode == "xyxy"
            bboxes_per_im = targets_per_im.bbox
            labels_per_im = targets_per_im.get_field("labels")
            anchors_per_im = cat_boxlist(anchors[im_i])
            num_gt = bboxes_per_im.shape[0]

            num_anchors_per_level = [len(anchors_per_level.bbox) for anchors_per_level in anchors[im_i]]
            ious = boxlist_iou(anchors_per_im, targets_per_im)

            gt_cx = (bboxes_per_im[:, 2] + bboxes_per_im[:, 0]) / 2.0
            gt_cy = (bboxes_per_im[:, 3] + bboxes_per_im[:, 1]) / 2.0
            gt_points = torch.stack((gt_cx, gt_cy), dim=1)

            anchors_cx_per_im = (anchors_per_im.bbox[:, 2] + anchors_per_im.bbox[:, 0]) / 2.0
            anchors_cy_per_im = (anchors_per_im.bbox[:, 3] + anchors_per_im.bbox[:, 1]) / 2.0
            anchor_points = torch.stack((anchors_cx_per_im, anchors_cy_per_im), dim=1)

            distances = (anchor_points[:, None, :] - gt_points[None, :, :]).pow(2).sum(-1).sqrt()

            # Selecting candidates based on the center distance between anchor box and object
            candidate_idxs = []
            star_idx = 0
            for level, anchors_per_level in enumerate(anchors[im_i]):
                end_idx = star_idx + num_anchors_per_level[level]
                distances_per_level = distances[star_idx:end_idx, :]
                _, topk_idxs_per_level = distances_per_level.topk(self.cfg.MODEL.ATSS.TOPK, dim=0, largest=False)
                candidate_idxs.append(topk_idxs_per_level + star_idx)
                star_idx = end_idx
            candidate_idxs = torch.cat(candidate_idxs, dim=0)

            # Using the sum of mean and standard deviation as the IoU threshold to select final positive samples
            candidate_ious = ious[candidate_idxs, torch.arange(num_gt)]
            iou_mean_per_gt = candidate_ious.mean(0)
            iou_std_per_gt = candidate_ious.std(0)
            iou_thresh_per_gt = iou_mean_per_gt + iou_std_per_gt
            is_pos = candidate_ious >= iou_thresh_per_gt[None, :]

            # Limiting the final positive samples’ center to object
            anchor_num = anchors_cx_per_im.shape[0]
            for ng in range(num_gt):
                candidate_idxs[:, ng] += ng * anchor_num
            e_anchors_cx = anchors_cx_per_im.view(1, -1).expand(num_gt, anchor_num).contiguous().view(-1)
            e_anchors_cy = anchors_cy_per_im.view(1, -1).expand(num_gt, anchor_num).contiguous().view(-1)
            candidate_idxs = candidate_idxs.view(-1)
            l = e_anchors_cx[candidate_idxs].view(-1, num_gt) - bboxes_per_im[:, 0]
            t = e_anchors_cy[candidate_idxs].view(-1, num_gt) - bboxes_per_im[:, 1]
            r = bboxes_per_im[:, 2] - e_anchors_cx[candidate_idxs].view(-1, num_gt)
            b = bboxes_per_im[:, 3] - e_anchors_cy[candidate_idxs].view(-1, num_gt)
            is_in_gts = torch.stack([l, t, r, b], dim=1).min(dim=1)[0] > 0.01
            is_pos = is_pos & is_in_gts

            # if an anchor box is assigned to multiple gts, the one with the highest IoU will be selected.
            ious_inf = torch.full_like(ious, -INF).t().contiguous().view(-1)
            index = candidate_idxs.view(-1)[is_pos.view(-1)]
            ious_inf[index] = ious.t().contiguous().view(-1)[index]
            ious_inf = ious_inf.view(num_gt, -1).t()

            anchors_to_gt_values, anchors_to_gt_indexs = ious_inf.max(dim=1)
            cls_labels_per_im = labels_per_im[anchors_to_gt_indexs]
            cls_labels_per_im[anchors_to_gt_values == -INF] = 0
            matched_gts = bboxes_per_im[anchors_to_gt_indexs]

            matched_gts = matched_gts.to(self.device)
            anchors_per_im = anchors_per_im.to(self.device)

            reg_targets_per_im = self.box_coder.encode(matched_gts, anchors_per_im.bbox)
            cls_labels.append(cls_labels_per_im.to(self.device))
            reg_targets.append(reg_targets_per_im)

        return cls_labels, reg_targets

    def compute_centerness_targets(self, regr_targets, anchors):
        gts = self.box_coder.decode(regr_targets, anchors)
        anchors_cx = (anchors[:, 2] + anchors[:, 0]) / 2
        anchors_cy = (anchors[:, 3] + anchors[:, 1]) / 2
        l = anchors_cx - gts[:, 0]
        t = anchors_cy - gts[:, 1]
        r = gts[:, 2] - anchors_cx
        b = gts[:, 3] - anchors_cy
        left_right = torch.stack([l, r], dim=1)
        top_bottom = torch.stack([t, b], dim=1)
        centerness = torch.sqrt((left_right.min(dim=-1)[0] / left_right.max(dim=-1)[0]) * (top_bottom.min(dim=-1)[0] / top_bottom.max(dim=-1)[0]))
        assert not torch.isnan(centerness).any()
        return centerness

    def get_loss_dyn_image_size(self, box_cls, box_regression, centerness, targets, anchors):
        labels, reg_targets = self.prepare_targets(targets, anchors)
        N = len(labels)
        box_cls_flatten, box_regression_flatten = concat_box_prediction_layers(box_cls, box_regression)
        centerness_flatten = [ct.permute(0, 2, 3, 1).reshape(N, -1, 1) for ct in centerness]
        centerness_flatten = torch.cat(centerness_flatten, dim=1).reshape(-1)

        labels_flatten = torch.cat(labels, dim=0)
        reg_targets_flatten = torch.cat(reg_targets, dim=0)
        anchors_flatten = torch.cat([cat_boxlist(anchors_per_image).bbox for anchors_per_image in anchors], dim=0)

        pos_inds = torch.nonzero(labels_flatten > 0).squeeze(1)

        num_gpus = get_num_gpus()
        total_num_pos = reduce_sum(pos_inds.new_tensor([pos_inds.numel()])).item()
        num_pos_avg_per_gpu = max(total_num_pos / float(num_gpus), 1.0)

        cls_loss = torch.div(self.cls_loss(box_cls_flatten, labels_flatten.int()), num_pos_avg_per_gpu)
        # cls_loss = self.cls_loss(box_cls_flatten, labels_flatten.int())
        if pos_inds.numel() > 0:
            box_regression_flatten = box_regression_flatten[pos_inds]
            reg_targets_flatten = reg_targets_flatten[pos_inds]
            anchors_flatten = anchors_flatten[pos_inds]
            centerness_flatten = centerness_flatten[pos_inds]
            centerness_targets = self.compute_centerness_targets(reg_targets_flatten,
                                                                 anchors_flatten).type(centerness_flatten.dtype)
            sum_centerness_targets_avg_per_gpu = reduce_sum(centerness_targets.sum()).item() / float(num_gpus)
            reg_loss = self.regr_loss(box_regression_flatten, reg_targets_flatten, anchors_flatten,
                                      weight=centerness_targets) / sum_centerness_targets_avg_per_gpu
            centerness_loss = torch.div(self.centerness_loss(centerness_flatten, centerness_targets),
                                        num_pos_avg_per_gpu)
        else:
            reg_loss = box_regression_flatten.sum()
            centerness_loss = reg_loss * 0

        return cls_loss, reg_loss, centerness_loss

    def get_loss_static_image_size(self, box_cls, box_regression, centerness, anchors, labels, reg_targets,
                                   centerness_target):
        batch_size = centerness[0].size(0)

        anchors = [anchors for _ in range(batch_size)]

        box_cls_flatten, box_regression_flatten = concat_box_prediction_layers(box_cls, box_regression)
        centerness_flatten = [ct.permute(0, 2, 3, 1).reshape(batch_size, -1, 1) for ct in centerness]
        centerness_flatten = torch.cat(centerness_flatten, dim=1).reshape(-1)

        if isinstance(labels, list): labels_flatten = torch.cat(labels, dim=0).to(self.device)
        else: labels_flatten = labels
        if isinstance(reg_targets, list): reg_targets_flatten = torch.cat(reg_targets, dim=0)
        else: reg_targets_flatten = reg_targets
        anchors_flatten = torch.cat([cat_boxlist(anchors_per_image).bbox for anchors_per_image in anchors], dim=0)
        # anchors_flatten = torch.cat([box for box in cat_boxlist(anchors).bbox], dim=0)
        pos_inds = torch.nonzero(labels_flatten > 0)[:, 0]  # .squeeze(1)
        num_gpus = get_num_gpus()
        total_num_pos = reduce_sum(pos_inds.new_tensor([pos_inds.numel()])).item()
        num_pos_avg_per_gpu = max(total_num_pos / float(num_gpus), 1.0)

        cls_loss = torch.div(self.cls_loss(box_cls_flatten, labels_flatten.int()), num_pos_avg_per_gpu)
        # cls_loss = self.cls_loss(box_cls_flatten, labels_flatten.int())
        if pos_inds.numel() > 0:
            box_regression_flatten = box_regression_flatten[pos_inds]
            anchors_flatten = anchors_flatten[pos_inds]
            centerness_flatten = centerness_flatten[pos_inds]
            reg_targets_flatten = reg_targets_flatten[pos_inds]

            # Convert centerness targets to tensor
            centerness_target_tensor = []
            for t in centerness_target: centerness_target_tensor.extend(t)
            centerness_target = torch.stack(centerness_target_tensor).to(self.device)
            sum_centerness_targets_avg_per_gpu = reduce_sum(centerness_target.sum()).item() / float(num_gpus)
            reg_loss = self.regr_loss(box_regression_flatten, reg_targets_flatten.to(self.device), anchors_flatten,
                                      weight=centerness_target.to(self.device)) / sum_centerness_targets_avg_per_gpu
            centerness_loss = torch.div(self.centerness_loss(centerness_flatten, centerness_target),
                                        num_pos_avg_per_gpu)
        else:
            reg_loss = box_regression_flatten.sum()
            centerness_loss = reg_loss * 0
        return cls_loss, reg_loss, centerness_loss

    def __call__(self, box_cls, box_regression, centerness, targets=None, anchors=None, labels=None, reg_targets=None,
                 centerness_target=None):
        if labels is None or reg_targets is None or centerness_target is None:
            cls_loss, reg_loss, centerness_loss = self.get_loss_dyn_image_size(box_cls, box_regression, centerness,
                                                                               targets, anchors)
        else:
            cls_loss, reg_loss, centerness_loss = self.get_loss_static_image_size(box_cls, box_regression, centerness,
                                                                                  anchors, labels, reg_targets,
                                                                                  centerness_target)

        losses = {
            "loss_cls": cls_loss,
            "loss_reg": reg_loss * self.cfg.MODEL.ATSS.REG_LOSS_WEIGHT,
            "loss_centerness": centerness_loss
        }
        return losses


def get_atss_loss_evaluator(cfg, box_coder, device):
    return ATSSLossComputation(cfg, box_coder, device)


# HELPER FUNCTIONS #####################################################################################################
def get_num_gpus():
    return 1


def reduce_sum(tensor):
    if get_num_gpus() <= 1:
        return tensor
    import torch.distributed as dist
    tensor = tensor.clone()
    dist.all_reduce(tensor, op=dist.reduce_op.SUM)
    return tensor


def cat(tensors, dim=0):
    """
    Efficient version of torch.cat that avoids a copy if there is only a single element in a list
    """
    assert isinstance(tensors, (list, tuple))
    if len(tensors) == 1:
        return tensors[0]
    return torch.cat(tensors, dim)


def permute_and_flatten(layer, N, A, C, H, W):
    layer = layer.view(N, -1, C, H, W)
    layer = layer.permute(0, 3, 4, 1, 2)
    layer = layer.reshape(N, -1, C)
    return layer


def concat_box_prediction_layers(box_cls, box_regression):
    box_cls_flattened = []
    box_regression_flattened = []
    # for each feature level, permute the outputs to make them be in the
    # same format as the labels. Note that the labels are computed for
    # all feature levels concatenated, so we keep the same representation
    # for the objectness and the box_regression
    for box_cls_per_level, box_regression_per_level in zip(
        box_cls, box_regression
    ):
        N, AxC, H, W = box_cls_per_level.shape
        Ax4 = box_regression_per_level.shape[1]
        A = Ax4 // 4
        C = AxC // A
        box_cls_per_level = permute_and_flatten(
            box_cls_per_level, N, A, C, H, W
        )
        box_cls_flattened.append(box_cls_per_level)

        box_regression_per_level = permute_and_flatten(
            box_regression_per_level, N, A, 4, H, W
        )
        box_regression_flattened.append(box_regression_per_level)
    # concatenate on the first dimension (representing the feature levels), to
    # take into account the way the labels were generated (with all feature maps
    # being concatenated as well)
    box_cls = cat(box_cls_flattened, dim=1).reshape(-1, C)
    box_regression = cat(box_regression_flattened, dim=1).reshape(-1, 4)
    return box_cls, box_regression