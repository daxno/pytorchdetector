import torch.nn.functional as F
from torch import nn
import torch
import math

from layers import Scale, get_anchor_generator_atss

from .loss_evaluator import get_atss_loss_evaluator
from .postprocessor import get_atss_postprocessor
from .box_coder import ATSSBoxCoder


class ATSSHead(nn.Module):
    def __init__(self, cfg, in_channels: list):
        super(ATSSHead, self).__init__()
        self.cfg = cfg
        num_classes = cfg.MODEL.ATSS.NUM_CLASSES - 1
        num_anchors = len(cfg.MODEL.ATSS.ASPECT_RATIOS) * cfg.MODEL.ATSS.SCALES_PER_OCTAVE

        cls_tower = []
        bbox_tower = []
        if sum(in_channels) != len(in_channels)*in_channels[0]:
            # TODO попробоавть сделать разные головы на разные feature map'ы
            raise NotImplementedError('All input channels have to be equal')
        in_channels = in_channels[0]

        for i in range(cfg.MODEL.ATSS.NUM_CONVS):
            if self.cfg.MODEL.ATSS.USE_DCN_IN_TOWER and i == cfg.MODEL.ATSS.NUM_CONVS - 1:
                # conv_func = DFConv2d
                raise NotImplementedError('Deformable convolution is not implemented yet.')
            else:
                conv_func = nn.Conv2d

            cls_tower.append(
                conv_func(
                    in_channels,
                    in_channels,
                    kernel_size=3,
                    stride=1,
                    padding=1,
                    bias=True
                )
            )
            cls_tower.append(nn.GroupNorm(32, in_channels))
            cls_tower.append(nn.ReLU())
            bbox_tower.append(
                conv_func(
                    in_channels,
                    in_channels,
                    kernel_size=3,
                    stride=1,
                    padding=1,
                    bias=True
                )
            )
            bbox_tower.append(nn.GroupNorm(32, in_channels))
            bbox_tower.append(nn.ReLU())

        self.add_module('cls_tower', nn.Sequential(*cls_tower))
        self.add_module('bbox_tower', nn.Sequential(*bbox_tower))
        self.cls_logits = nn.Conv2d(
            in_channels, num_anchors * num_classes, kernel_size=3, stride=1, padding=1
        )
        self.bbox_pred = nn.Conv2d(
            in_channels, num_anchors * 4, kernel_size=3, stride=1, padding=1
        )
        self.centerness = nn.Conv2d(
            in_channels, num_anchors * 1, kernel_size=3, stride=1, padding=1
        )

        # initialization
        for modules in [self.cls_tower, self.bbox_tower,
                        self.cls_logits, self.bbox_pred,
                        self.centerness]:
            for l in modules.modules():
                if isinstance(l, nn.Conv2d):
                    nn.init.normal_(l.weight, std=0.01)
                    nn.init.constant_(l.bias, 0)

        # initialize the bias for focal loss
        prior_prob = cfg.MODEL.ATSS.PRIOR_PROB
        bias_value = -math.log((1 - prior_prob) / prior_prob)
        nn.init.constant_(self.cls_logits.bias, bias_value)

        self.scales = nn.ModuleList([Scale(init_value=1.0) for _ in range(5)])

    def forward(self, x):
        logits = []
        bbox_reg = []
        centerness = []
        for l, feature in enumerate(x):
            cls_tower = self.cls_tower(feature)
            box_tower = self.bbox_tower(feature)

            logits.append(self.cls_logits(cls_tower))

            bbox_pred = self.scales[l](self.bbox_pred(box_tower))
            bbox_reg.append(bbox_pred)

            centerness.append(self.centerness(box_tower))
        return logits, bbox_reg, centerness


class ATSS(nn.Module):

    def __init__(self, cfg, backbone):
        super(ATSS, self).__init__()
        self.cfg = cfg

        in_channels = cfg.MODEL.ATSS.IN_CHANNELS
        box_coder = ATSSBoxCoder(cfg)

        self.backbone = backbone
        self.head = ATSSHead(cfg, in_channels)
        self.loss_evaluator = get_atss_loss_evaluator(cfg, box_coder)
        self.box_selector_test = get_atss_postprocessor(cfg, box_coder)
        self.anchor_generator = get_anchor_generator_atss(cfg)

    def forward(self, images, targets=None):
        features = self.backbone(images.tensors)
        box_cls, box_regression, centerness = self.head(features)
        anchors = self.anchor_generator(images, features)

        if self.training:
            return self._forward_train(box_cls, box_regression, centerness, targets, anchors)
        else:
            return self._forward_test(box_cls, box_regression, centerness, anchors)

    def _forward_train(self, box_cls, box_regression, centerness, targets, anchors):
        loss_box_cls, loss_box_reg, loss_centerness = self.loss_evaluator(
            box_cls, box_regression, centerness, targets, anchors
        )
        losses = {
            "loss_cls": loss_box_cls,
            "loss_reg": loss_box_reg,
            "loss_centerness": loss_centerness
        }
        return None, losses

    def _forward_test(self, box_cls, box_regression, centerness, anchors):
        boxes = self.box_selector_test(box_cls, box_regression, centerness, anchors)
        return boxes, {}

    def save(self, state, path):
        assert 'iteration' in state.keys() and 'optimizer' in state.keys(), f'State keys: {state.keys()}'
        state.update({
            'state_dict': self.state_dict()
        })
        torch.save(state, path)
