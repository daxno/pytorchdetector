import torch.nn.functional as F
from torch import nn
import torch
import math

from structures import to_image_list
from .box_coder import get_atss_box_coder
from .postprocessor import get_atss_postprocessor
from layers import Scale, get_anchor_generator_atss, weight_init


class h_sigmoid(nn.Module):
    def __init__(self, inplace=True):
        super(h_sigmoid, self).__init__()
        self.relu = nn.ReLU6(inplace=inplace)

    def forward(self, x):
        return self.relu(x + 3) / 6


class h_swish(nn.Module):
    def __init__(self, inplace=True):
        super(h_swish, self).__init__()
        self.sigmoid = h_sigmoid(inplace=inplace)

    def forward(self, x):
        return x * self.sigmoid(x)


class SELayer(nn.Module):
    def __init__(self, channel, reduction=4):
        super(SELayer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Sequential(
                nn.Linear(channel, channel // reduction),
                nn.ReLU(inplace=True),
                nn.Linear(channel // reduction, channel),
                h_sigmoid()
        )

    def forward(self, x):
        b, c, _, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1, 1)
        return x * y


class MBConvBlock(nn.Module):
    def __init__(self, input_channels, hidden_channels):
        super(MBConvBlock, self).__init__()

        self.seq = nn.Sequential(
            # pw
            nn.Conv2d(input_channels, hidden_channels, 1, 1, 0, bias=True),
            nn.BatchNorm2d(hidden_channels),
            nn.ReLU(inplace=True),  # h_swish(),  # if use_hs else nn.ReLU(inplace=True),
            # dw
            nn.Conv2d(hidden_channels, hidden_channels, kernel_size=3, stride=1, padding=1, groups=hidden_channels,
                      bias=False),
            nn.BatchNorm2d(hidden_channels),
            # Squeeze-and-Excite
            # SELayer(hidden_channels),  # if use_se else nn.Sequential(),
            nn.ReLU(inplace=True),  # h_swish(),  # if use_hs else nn.ReLU(inplace=True),
            # pw-linear
            nn.Conv2d(hidden_channels, input_channels, 1, 1, 0, bias=True),
            nn.BatchNorm2d(input_channels),
        )

    def forward(self, x):
        return self.seq(x)


class ATSSHead(nn.Module):
    def __init__(self, cfg, in_channels: list):
        super(ATSSHead, self).__init__()
        self.cfg = cfg
        num_classes = cfg.MODEL.ATSS.NUM_CLASSES - 1
        num_anchors = len(cfg.MODEL.ATSS.ASPECT_RATIOS) * cfg.MODEL.ATSS.SCALES_PER_OCTAVE

        cls_tower = []
        bbox_tower = []
        ct_tower = []
        if sum(in_channels) != len(in_channels)*in_channels[0]:
            # TODO попробоавть сделать разные головы на разные feature map'ы
            raise NotImplementedError('All input channels have to be equal')
        in_channels = in_channels[0]

        for i in range(cfg.MODEL.ATSS.NUM_CONVS):
            if self.cfg.MODEL.ATSS.USE_DCN_IN_TOWER and i == cfg.MODEL.ATSS.NUM_CONVS - 1:
                # conv_func = DFConv2d
                raise NotImplementedError('Deformable convolution is not implemented yet.')
            else:
                conv_func = nn.Conv2d
                # conv_func = MBConvBlock

            cls_tower.append(
                conv_func(
                    in_channels,
                    in_channels,
                    kernel_size=3,
                    stride=1,
                    padding=1,
                    bias=True,
                    groups=in_channels
                )
                # conv_func(in_channels, in_channels)
            )

            cls_tower.append(nn.BatchNorm2d(in_channels))
            # cls_tower.append(nn.GroupNorm(32, in_channels))
            cls_tower.append(nn.ReLU(inplace=True))
            bbox_tower.append(
                conv_func(
                    in_channels,
                    in_channels,
                    kernel_size=3,
                    stride=1,
                    padding=1,
                    bias=True,
                    groups=in_channels
                )
                # conv_func(in_channels, in_channels)
            )
            bbox_tower.append(nn.BatchNorm2d(in_channels))
            # bbox_tower.append(nn.GroupNorm(32, in_channels))
            bbox_tower.append(nn.ReLU(inplace=True))

            # ct_tower.extend([
            #     conv_func(
            #         in_channels,
            #         in_channels,
            #         kernel_size=3,
            #         stride=1,
            #         padding=1,
            #         bias=True,
            #         groups=in_channels
            #     ),
            #     nn.BatchNorm2d(in_channels),
            #     nn.ReLU(inplace=True)
            # ])

        self.add_module('cls_tower', nn.Sequential(*cls_tower))
        self.add_module('bbox_tower', nn.Sequential(*bbox_tower))
        # self.add_module('ct_tower', nn.Sequential(*ct_tower))
        self.cls_logits = nn.Conv2d(
            in_channels, num_anchors * num_classes, kernel_size=3, stride=1, padding=1
        )
        self.bbox_pred = nn.Conv2d(
            in_channels, num_anchors * 4, kernel_size=3, stride=1, padding=1
        )
        self.centerness = nn.Conv2d(
            in_channels, num_anchors * 1, kernel_size=3, stride=1, padding=1
        )

        # initialization
        for module in [self.cls_tower, self.bbox_tower, self.cls_logits, self.bbox_pred, self.centerness]:
            module.apply(weight_init)
        print('ATSS model initialized')

        # initialize the bias for focal loss
        prior_prob = cfg.MODEL.ATSS.PRIOR_PROB
        bias_value = -math.log((1 - prior_prob) / prior_prob)
        nn.init.constant_(self.cls_logits.bias, bias_value)

        self.scales = nn.ModuleList([Scale(init_value=1.0) for _ in range(5)])

    def forward(self, x):
        logits = []
        bbox_reg = []
        centerness = []
        for l, feature in enumerate(x):
            cls_tower = self.cls_tower(feature)
            box_tower = self.bbox_tower(feature)
            # ct_tower = self.ct_tower(feature)

            logits.append(self.cls_logits(cls_tower))

            bbox_pred = self.scales[l](self.bbox_pred(box_tower))
            bbox_reg.append(bbox_pred)

            centerness.append(self.centerness(box_tower))
            # centerness.append(self.centerness(ct_tower))
        return logits, bbox_reg, centerness

import tensorrt as trt
from torch2trt import tensorrt_converter
# @tensorrt_converter('torch.cat')
# def convert_cat(ctx):
#     tensors = ctx.method_args[0]
#     if 'dim' in ctx.method_kwargs: dim = ctx.method_kwargs['dim']
#     elif len(ctx.method_args) == 2: dim = ctx.method_args[1]
#     else: dim = 0
#
#     if tensors[0].ndim < 4: dim = dim - 1
#     output = ctx.method_return
#     layer = ctx.network.add_concatenation([t._trt for t in tensors])
#     layer.axis = dim
#     output._trt = layer.get_output(0)


class ATSS(nn.Module):

    def __init__(self, cfg, backbone, trt_mode=False):
        super(ATSS, self).__init__()
        self.cfg = cfg

        in_channels = cfg.MODEL.ATSS.IN_CHANNELS

        self.backbone = backbone(cfg)
        self.head = ATSSHead(cfg, in_channels)
        self.trt_mode = trt_mode
        self.num_fpn_layers = cfg.MODEL.BACKBONE.FPN.NUM_LAYERS

    def forward(self, images: torch.Tensor):
        features = self.backbone(images)
        # print([f.size() for f in features])
        features = [f.squeeze(1) if f.ndim == 5 else f for f in features]
        box_cls, box_regression, centerness = self.head(features)
        # print([b.size() for b in box_cls])
        # print([b.size() for b in box_regression])
        # print([b.size() for b in centerness])
        # print([b.size() for b in features])
        if self.trt_mode:
            # For TRT conversion
            # per_layer_outputs = [torch.cat([cls, ct, regr], 1) for cls, regr, ct in
            #                      zip(box_cls, box_regression, centerness)]
            per_layer_outputs = []
            for idx, (cls, regr, ct) in enumerate(zip(box_cls, box_regression, centerness)):
                # print(f'layer {idx}: cls size: {cls.size()} regr size: {regr.size()} ct size: {ct.size()}')
                per_layer_outputs.append(torch.cat([cls, ct, regr], dim=1))

            return per_layer_outputs
        else:
            return box_cls, box_regression, centerness, features


# if __name__ == '__main__':
#     from config import cfg
#     from modeling import get_mobilenetv3_small_fpn, get_atss_model
#
#     model = get_atss_model(cfg, get_mobilenetv3_small_fpn, trt_mode=True).eval().cuda()
#     model(torch.zeros((1, 3, 768, 1024)).cuda())
