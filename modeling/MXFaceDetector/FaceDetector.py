from mxnet import gluon
import mxnet as mx
import numpy as np
import cv2

from .Postprocessor import SSDResultProcessorPerLayerThresholds
from .utils import correct_shape_to_factor, resize_image_keep_aspect, pad_along_axis, inv_rel_boxes_resize


class FaceDetector:
    def __init__(self, cfg):
        self.batch_size = cfg.SSD.BATCH_SIZE
        self.ctx = mx.gpu(cfg.SSD.GPU)

        self.mean_rgb = np.array([123.68, 116.28, 103.53]).astype(float)
        self.std_rgb = np.array([1.0, 1.0, 1.0]).astype(float)
        self.min_size, self.max_size = 256, 2048

        self.target_shape = (512, 768)
        self.current_data_shape = self.target_shape
        self.shape_factor = 256
        self.avoid_resizing = True

        nms_ths = [[0.25, 0.3, 0.3, 0.4], [0.5, 0.5, 0.55], [0.7, 0.7, 0.7], [0.8, 0.8, 0.8, 0.8]]
        self.res_processor = SSDResultProcessorPerLayerThresholds(
            num_classes=1, nms_topk=100, post_nms=48, nms_threshold=0.1, valid_thresholds=nms_ths,
            num_layers=4
        )
        self.res_processor.initialize(ctx=self.ctx)

        self.model = gluon.nn.SymbolBlock.imports(
            cfg.SSD.SYMBOL_PATH, ['data'], cfg.SSD.PARAMS_PATH, ctx=self.ctx
        )

        self.im_paths = []
        self.images = []
        self.old_shapes = []

        # zero forward
        self.store_data(image=np.zeros(tuple(self.target_shape) + (3,), dtype=np.uint8))
        self.process_data()
        self.reset()

    def store_data(self, image_path=None, image=None):
        assert image_path is not None or image is not None

        if image_path is None:
            self.im_paths.append('None_%d' % len(self.im_paths))
        else:
            image = cv2.imread(image_path)
            self.im_paths.append(image_path)

        self.images.append(image)
        self.old_shapes.append(image.shape[:2])

    def compute_shape(self):
        if self.avoid_resizing:
            return correct_shape_to_factor(self.target_shape, self.shape_factor)
        else:
            shapes = np.array([im.shape[:2] for im in self.images])
            print(shapes.shape)
            return correct_shape_to_factor(
                tuple(np.clip(np.mean(shapes, axis=0), self.min_size, self.max_size).astype(int)), self.shape_factor
            )

    def preprocess_images(self):
        num_ims = len(self.images)
        num_batches = num_ims // self.batch_size
        residual = num_ims % self.batch_size

        images = np.array([resize_image_keep_aspect(image, self.current_data_shape) for image in self.images])
        images = (images - self.mean_rgb) / self.std_rgb
        if residual != 0:
            t_length = (num_batches+1)*self.batch_size
            images = pad_along_axis(images, axis=0, target_length=t_length)
            residual = t_length - num_ims
            num_batches += 1

        images = images.transpose(0, 3, 1, 2)
        return images, num_batches, residual

    def process_data(self):
        self.current_data_shape = self.compute_shape()
        images, num_batches, residual = self.preprocess_images()

        intermediate = []

        for start in range(num_batches):
            start_idx = start * self.batch_size
            im_list = images[start_idx:start_idx+self.batch_size]

            image_batch = mx.nd.array(im_list)
            cls_preds, box_regs, anchors, layer_mask = self.model.forward(image_batch.as_in_context(self.ctx))

            inter_bundle = [i.asnumpy() for i in self.res_processor.forward(
                box_regs, anchors, cls_preds, layer_mask)]
            intermediate.append(inter_bundle)  # ids_result, scores_result, boxes_result

        predictions = []
        for num, (ids_result, scores_result, boxes_result) in enumerate(intermediate):
            start_idx = num * self.batch_size
            for ids_im, boxes_im, scores_im, gen_shape in zip(
                    ids_result, boxes_result, scores_result, self.old_shapes[start_idx: start_idx + self.batch_size]):
                scores_im = scores_im[ids_im[:, 0] != -1]
                boxes_im = boxes_im[ids_im[:, 0] != -1]
                ids_im = ids_im[ids_im[:, 0] != -1]

                predictions_per_im = []

                boxes_rsz = inv_rel_boxes_resize(
                    boxes_im, target_shape=self.current_data_shape, genuine_shape=gen_shape
                )
                for idx, box, score in zip(ids_im, boxes_rsz, scores_im):
                    if float(score[0]) < 0.4:
                        continue

                    predictions_per_im.append({
                        'class': 'face',
                        'score': score[0],
                        'x': box[0], 'y': box[1], 'w': box[2] - box[0], 'h': box[3] - box[1]
                    })

                predictions.append(predictions_per_im)
        return self.im_paths, predictions

    def reset(self):
        self.im_paths = []
        self.images = []
        self.old_shapes = []
        self.current_data_shape = self.target_shape
