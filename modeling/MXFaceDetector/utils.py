import cv2
import numpy as np


def correct_shape_to_factor(shape, shape_factor):
    to_corr = [i % shape_factor for i in shape]
    corr_shape = []
    for val, to_c in zip(shape, to_corr):
        if to_c:
            tmp = shape_factor * int(float(val) / shape_factor)
        else:
            tmp = val
        corr_shape.append(tmp)

    return tuple(corr_shape)


def inv_rel_boxes_resize(boxes, target_shape, genuine_shape):
    """
    Resize boxes in cases when image will be resized with preserving aspect ratio,
    :param boxes: N,4 shape
    :param target_shape: 2-tuple
    :param genuine_shape 2-tuple
    :return: relative scale boxes for new image shape
    """
    h_t, w_t = target_shape
    boxes_px_t = boxes * np.tile(target_shape[::-1], 2)

    t_ar, g_ar = [float(shp[0]) / float(shp[1]) for shp in [target_shape, genuine_shape]]
    ar_rel = t_ar / g_ar

    h_g1, w_g1 = (int(h_t / ar_rel), w_t) if t_ar > g_ar else (h_t, int(w_t * ar_rel))

    dh, dw = h_t - h_g1, w_t - w_g1
    dwh = np.tile((dw, dh), 2)
    boxes_rel_g = np.clip((boxes_px_t - dwh / 2) / np.tile((w_g1, h_g1), 2), 0, 1)
    boxes_px_g = boxes_rel_g * np.tile(genuine_shape[::-1], 2)

    return boxes_px_g.astype(int)


def resize_image_keep_aspect(img, target_size=(500, 500)):
    """
    :param img:
    :param target_size: in h,w format
    """
    old_size = img.shape[:2]  # old_size is in (height, width) format

    ratios = [float(i) / float(j) for i, j in zip(target_size, old_size)]

    min_ratio_index = 0 if ratios[0] < ratios[1] else 1
    min_ratio = ratios[min_ratio_index]

    new_size = tuple([int(x * min_ratio) for x in old_size])  # new_size should be in (width, height) format
    img = cv2.resize(img, (new_size[1], new_size[0]))

    delta_w = target_size[1] - new_size[1]
    delta_h = target_size[0] - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    new_im = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=(0, 0, 0))
    return new_im


def pad_along_axis(array, target_length, axis=0):

    pad_size = target_length - array.shape[axis]
    axis_nb = len(array.shape)

    if pad_size < 0:
        return array

    npad = [(0, 0) for x in range(axis_nb)]
    npad[axis] = (0, pad_size)

    b = np.pad(array, pad_width=npad, mode='constant', constant_values=0)

    return b

