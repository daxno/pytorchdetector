import mxnet.gluon as gluon
import numpy as np
import itertools
from gluoncv.nn.coder import NormalizedBoxCenterDecoder


class MultiPerClassDecoderMultiThreshold(gluon.HybridBlock):
    """copies gcv implementation with one slight difference"""

    def __init__(self, num_class, axis=-1):
        super(MultiPerClassDecoderMultiThreshold, self).__init__()
        self._fg_class = num_class - 1
        self._axis = axis

    def hybrid_forward(self, F, x, thresholds):
        """
        :param F:
        :param x:
        :param thresholds: (N,)
        :return:
        """

        scores = x.slice_axis(axis=self._axis, begin=1, end=None)  # b x N x fg_class
        template = F.zeros_like(x.slice_axis(axis=-1, begin=0, end=1))
        cls_ids = []
        for i in range(self._fg_class):
            cls_ids.append(template + i)  # b x N x 1
        cls_id = F.concat(*cls_ids, dim=-1)  # b x N x fg_class

        thresholds = F.broadcast_like(F.reshape(thresholds, (1, -1, 1)), scores)
        mask = scores > thresholds
        cls_id = F.where(mask, cls_id, F.ones_like(cls_id) * -1)
        scores = F.where(mask, scores, F.zeros_like(scores))
        return cls_id, scores


class SSDResultProcessorPerLayerThresholds(gluon.HybridBlock):
    def __init__(self, valid_thresholds, num_layers, num_classes, nms_threshold=0.45,
                 post_nms=100, nms_topk=400, stds=(0.1, 0.1, 0.2, 0.2),):
        super(SSDResultProcessorPerLayerThresholds, self).__init__()
        self._num_classes = num_classes
        self._nms_thresh = nms_threshold
        self._post_nms = post_nms
        self._nms_topk = nms_topk

        self._mx_valid_threshold = min(itertools.chain(*valid_thresholds))

        valid_thresholds_maxlen = max(map(len, valid_thresholds))
        for th in valid_thresholds:
            th.extend([-1.] * (valid_thresholds_maxlen - len(th)))
        self._valid_thresholds = valid_thresholds

        self._num_layers = num_layers

        with self.name_scope():
            self.bbox_decoder = NormalizedBoxCenterDecoder(stds, convert_anchor=True)
            self.cls_decoder = MultiPerClassDecoderMultiThreshold(num_class=self._num_classes + 1)

            self.thresholds = self.params.get_constant('thresholds_const', np.array(self._valid_thresholds))

    def hybrid_forward(self, F, box_preds, anchors, cls_preds, layer_mask, *args, **kwargs):
        """

        :param F:
        :param box_preds:
        :param anchors:
        :param cls_preds:
        :param layer_mask: 2, n_anc, 1
        :param args:
        :param kwargs:
        :return:
        """

        thresholds = kwargs.get('thresholds')  # (n_layers, max_n_sizes)

        _layer_mask = F.reshape(layer_mask[0], (-1,))
        _size_mask = F.reshape(layer_mask[1], (-1,))

        per_layer_thresholds = F.take(thresholds, _layer_mask, axis=0)
        per_layer_thresholds = F.pick(per_layer_thresholds, _size_mask)

        boxes = self.bbox_decoder(box_preds, anchors)
        cls_ids, scores = self.cls_decoder(F.softmax(cls_preds, axis=-1), per_layer_thresholds)
        cls_ids += 1.  # compatibility with gluoncv
        results = []
        for i in range(self._num_classes):
            cls_id = cls_ids.slice_axis(axis=-1, begin=i, end=i+1)
            score = scores.slice_axis(axis=-1, begin=i, end=i+1)
            # per class results
            per_result = F.concat(*[cls_id, score, boxes], dim=-1)
            results.append(per_result)
        result = F.concat(*results, dim=1)
        if 0 < self._nms_thresh < 1:
            result = F.contrib.box_nms(
                result, overlap_thresh=self._nms_thresh, topk=self._nms_topk, valid_thresh=self._mx_valid_threshold,
                id_index=0, score_index=1, coord_start=2, force_suppress=False)
            if self._post_nms > 0:
                result = result.slice_axis(axis=1, begin=0, end=self._post_nms)
        ids = F.slice_axis(result, axis=2, begin=0, end=1)
        scores = F.slice_axis(result, axis=2, begin=1, end=2)
        boxes = F.slice_axis(result, axis=2, begin=2, end=6)
        return ids, scores, boxes
