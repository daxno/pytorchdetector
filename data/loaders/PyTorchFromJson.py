from torch.utils.data import Dataset, DataLoader
from data.transforms import build_transforms
from structures import BoxList, to_image_list
import torch
import json
import cv2


class Detection(Dataset):
    def __init__(self, annotations_path, transforms, classes):
        annot_path = annotations_path
        self.annotations = json.load(open(annot_path))
        print('Number of examples:', len(self.annotations.keys()))
        self._transform = transforms
        self.label2idx = {label: idx + 1 for idx, label in enumerate(classes)}
        self.paths = list(self.annotations.keys())
        self.values = [self.annotations[path] for path in self.paths]

    def __len__(self):
        return len(self.annotations.keys())

    def __getitem__(self, idx):
        image_path, labels = list(self.annotations.items())[idx]
        image = cv2.imread(image_path)

        boxes = [[float(box['x']), float(box['y']), float(box['w']), float(box['h'])] for box in labels]
        classes = [self.label2idx[box['class']] for box in labels]
        if not len(boxes):
            boxes = [[0., 0., 0., 0.]]
            classes = [1]

        out = self._transform(image=image.copy(), bboxes=boxes, category_ids=classes)
        image, boxes, classes = out['image'], out['bboxes'], out['category_ids']
        if 'ratio' in out.keys() and 'pad' in out.keys(): ratio, pad = out['ratio'], out['pad']
        else: ratio, pad = 1, (0, 0)

        targets = BoxList(torch.as_tensor(boxes).reshape(-1, 4), image.shape[1:][::-1], mode='xywh').convert('xyxy')
        targets.add_field('labels', torch.as_tensor(classes))
        targets.add_field('ratio', ratio)
        targets.add_field('pad', torch.as_tensor(pad))

        return image, targets

    def collate_fn(self, batch):
        transposed_batch = list(zip(*batch))
        images = to_image_list(transposed_batch[0])
        targets = transposed_batch[1]
        return images, targets


if __name__ == '__main__':
    from config import cfg
    from tqdm import tqdm
    import time
    annot_path = '/home/nlab/Experiments/Detector/resources/coco_FAKE_DETS.json'
    transform = build_transforms(cfg, False)
    dataset = Detection(annotations_path=annot_path, transforms=transform, classes=('face', ))

    batch_size = 32
    loader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=4, collate_fn=dataset.collate_fn)

    start_time = time.time()
    for data in tqdm(loader):
        images = data[0].to(torch.device('cuda:0'))
    duration = time.time() - start_time

    print(f'Total time: {duration:.4f}; '
          f'Mean time per batch: {duration/len(loader):.4f}; '
          f'Mean BPS: {len(loader)/duration:.4f}; '
          f'Mean FPS: {len(loader)*batch_size/duration:.4f}')


