import cv2
import json
import numpy as np
from tqdm import tqdm

import nvidia.dali.ops as ops
import nvidia.dali.types as types
import nvidia.dali.tfrecord as tfrec
from nvidia.dali.pipeline import Pipeline
from nvidia.dali.plugin.pytorch import DALIGenericIterator, TorchPythonFunction


class TransformMatrixGetter:
    def __init__(self, batch_size, ):
        self.batch_size = batch_size

    @staticmethod
    def single_transform_fn():
        dst_cx, dst_cy = (200, 200)
        src_cx, src_cy = (200, 200)

        # This function uses homogeneous coordinates - hence, 3x3 matrix

        # translate output coordinates to center defined by (dst_cx, dst_cy)
        t1 = np.array([[1, 0, -dst_cx],
                       [0, 1, -dst_cy],
                       [0, 0, 1]])

        def u():
            return np.random.uniform(-0.5, 0.5)

        # apply a randomized affine transform - uniform scaling + some random distortion
        m = np.array([
            [1 + u(), u(), 0],
            [u(), 1 + u(), 0],
            [0, 0, 1]])

        # translate input coordinates to center (src_cx, src_cy)
        t2 = np.array([[1, 0, src_cx],
                       [0, 1, src_cy],
                       [0, 0, 1]])

        # combine the transforms
        m = (np.matmul(t2, np.matmul(m, t1)))

        # remove the last row; it's not used by affine transform
        return m[0:2, 0:3]

    def get(self):
        out = np.zeros([self.batch_size, 2, 3])
        for i in range(self.batch_size):
            out[i, :, :] = self.single_transform_fn()
        return out.astype(np.float32)


class DataIterator:
    def __init__(self, annotations, batch_size, classes):
        """
        :param annotations: dict in format: {image_path: [{'x': x, 'y': y, 'w': w, 'h': h}]}
                            all coordinates have to be in range (0, 1)
        :param batch_size:
        :param classes: list of classes
        """
        self.annotations = annotations
        self.batch_size = batch_size
        self.paths = list(self.annotations.keys())
        self.i = 0
        self.n = len(self.annotations)
        # self.next = self.__next__
        self.class_mapping = {cls: idx for idx, cls in enumerate(classes)}

    def __iter__(self):
        self.i = 0
        self.n = len(self.annotations)
        return self

    @property
    def size(self, ):
        return len(self.annotations)

    def create_detections_list(self, batch_detections):
        max_det_num = max([len(dets) for dets in batch_detections])
        bboxes = np.zeros((self.batch_size, max_det_num, 4), dtype=np.float32)
        labels = np.zeros((self.batch_size, max_det_num, 1), dtype=np.int32) - 1
        for idx, dets in enumerate(batch_detections):
            bboxes[idx, :len(dets)] = np.array([[det['x'], det['y'],
                                                 (det['x'] + det['w']), (det['y'] + det['h'])]
                                                for det in dets])
            labels[idx, :len(dets)] = np.array([[self.class_mapping[det['class']]] for det in dets])
        bboxes = np.clip(bboxes, a_min=0, a_max=1)
        return bboxes, labels

    def __next__(self):
        images = []
        labels = []
        for _ in range(self.batch_size):
            image_path = self.paths[self.i]
            annot = self.annotations[image_path]

            # Reading image buffer and saving to images list
            fp = open(image_path, 'rb')
            images.append(np.frombuffer(fp.read(), dtype=np.uint8))
            # Saving detections dict to list
            labels.append(annot)

            self.i = (self.i + 1) % self.n

        bboxes, labels = self.create_detections_list(labels)
        return images, bboxes, labels

    next = __next__


class DetectionPipeline(Pipeline):
    def __init__(self, tfrecord_path, batch_size, num_threads, device_id, target_shape=(768, 1024), exec_async=True,
                 exec_pipelined=True):
        super(DetectionPipeline, self).__init__(batch_size, num_threads, device_id, seed=125, exec_async=exec_async,
                                                exec_pipelined=exec_pipelined)

        self.target_shape = target_shape
        self.target_x = ops.Uniform(range=(self.target_shape[1], self.target_shape[1]))
        self.target_y = ops.Uniform(range=(self.target_shape[0], self.target_shape[0]))

        self.input = ops.TFRecordReader(path=f'{tfrecord_path}/images.tfrec',
                                        index_path=f'{tfrecord_path}/images.idx',
                                        features={
                                            'raw_image': tfrec.FixedLenFeature([], tfrec.string, ''),
                                            'image_height': tfrec.FixedLenFeature([], tfrec.int64, 0),
                                            'image_width': tfrec.FixedLenFeature([], tfrec.int64, 0),
                                            'boxes': tfrec.VarLenFeature(tfrec.float32, 0.0),
                                            'num_detections': tfrec.FixedLenFeature([], tfrec.int64, 0)
                                        })

        self.decode = ops.ImageDecoder(device="mixed", output_type=types.RGB)

        # Transforms
        self.flip = ops.Flip(device='gpu')
        self.apply_flip = ops.CoinFlip(probability=0.5)
        self.uniform = ops.Uniform(range=(0, 1))
        self.bbox_flip = ops.BbFlip(device='cpu', ltrb=False)
        self.normalize = ops.CropMirrorNormalize(device='gpu', mean=[0., 0., 0.], std=[1., 1., 1.],
                                                 output_dtype=types.FLOAT, output_layout='CHW')

        # Color augmentations params
        # self.apply_br_contr = ops.CoinFlip(probability=0.5)
        # self.br_mult = ops.Uniform(range=(0.8, 1))
        # self.br_shift = ops.Uniform(range=(0, 0.2))
        # self.contr_mult = ops.Uniform(range=(0.8, 1))
        # self.br_contr = ops.BrightnessContrast(device='gpu')
        #
        # self.apply_hsv = ops.CoinFlip(probability=0.5)
        # self.hue = ops.Uniform(range=(0.7, 1))
        # self.saturation = ops.Uniform(range=(0.7, 1))
        # self.value = ops.Uniform(range=(0.7, 1))
        # self.hsv = ops.Hsv(device='gpu')
        # Affine transform
        # self.transform_matrix_getter = TransformMatrixGetter(batch_size)
        # self.affine_transorm = ops.WarpAffine(device='gpu')
        # self.affine = ops.Uniform(range=(-1, 1))
        # TODO add reading from .rec file (MXNetReader)

        self.prospective_crop = ops.RandomBBoxCrop(
            device="cpu",
            aspect_ratio=[0.25, 2.0],
            thresholds=[0.1, 0.3, 0.5],
            scaling=[0.4, 1.0],
            ltrb=True,
            num_attempts=1
        )
        self.resize = ops.Resize(device='gpu')
        self.slice = ops.Slice(device='gpu')

        self.cast = ops.Cast(device='gpu', dtype=types.FLOAT)
        self.cast_bbox = ops.Cast(device='cpu', dtype=types.FLOAT)
        self.reshape_bbox = TorchPythonFunction(self.box_reshape_fn, num_outputs=1)

        self.jpegs = None
        self.bboxes = None
        self.labels = None
        self.transfrom_matrix = None

    @staticmethod
    def box_reshape_fn(boxes, num_outputs):
        return boxes.view((num_outputs[0], 4))

    def define_graph(self):
        inputs = self.input(name='Reader')
        images = inputs['raw_image']
        bboxes = inputs['boxes']
        num_detections = inputs['num_detections']
        # bboxes = self.reshape_bbox(bboxes, (num_detections, 4))
        # paths = inputs['image_path']
        # print(paths)
        # self.bboxes = self.input_bbox()
        # self.labels = self.input_label()
        # self.transfrom_matrix = self.input_matrix()

        # Decode from buffer and applying transformations
        images = self.decode(images)
        # Applying color image augmentations
        # images = self.br_contr(images, brightness=self.br_mult(), brightness_shift=self.br_shift(),
        #                        contrast=self.contr_mult())
        #
        # images = self.hsv(images, hue=self.hue(), saturation=self.saturation(), value=self.value())

        # Applying spatial image, bboxes augmentation
        # is_flip = self.apply_flip()
        # images = self.flip(images, horizontal=is_flip)
        # bboxes = self.cast_bbox(bboxes)
        # if is_flip:
        #     bboxes = self.bbox_flip(bboxes, horizontal=is_flip)

        # TODO почему-то выскакривает SizeError
        # crop_begin, crop_size, bboxes, labels = self.prospective_crop(bboxes, self.labels)
        # images = self.slice(images, crop_begin, crop_size)
        # images = self.resize(images, resize_x=self.target_x(), resize_y=self.target_y())

        # Applying affine transform to image (dont know how to do in to bbox)
        # images = self.affine_transorm(images, matrix=self.transfrom_matrix)

        bboxes = self.reshape_bbox(bboxes, num_detections)
        images = self.resize(images, resize_x=self.target_x(), resize_y=self.target_y())
        images = self.normalize(images)
        output = self.cast(images)
        return output  # , bboxes


if __name__ == '__main__':
    import time
    # annotations = json.load(open('/home/nlab/Experiments/Detector/resources/face_annotations_v3.json'))
    tfrec_path = '/home/nlab/Experiments/Detector/resources'

    batch_size = 8
    num_threads = 2
    num_iterations = 500
    pipeline_0 = DetectionPipeline(tfrec_path, 64, num_threads=num_threads, device_id=0,
                                   exec_async=False, exec_pipelined=False)
    pipeline_1 = DetectionPipeline(tfrec_path, 64, num_threads=num_threads, device_id=1,
                                   exec_async=False, exec_pipelined=False)
    pipeline_0.build()
    pipeline_1.build()

    dali_iterator = DALIGenericIterator(pipelines=[pipeline_0, pipeline_1], output_map=['data'],
                                        size=pipeline_0.epoch_size("Reader"), dynamic_shape=False)

    start_time = time.time()
    for idx, data in tqdm(enumerate(dali_iterator)):
        images = [d['data'] for d in data]
        # bboxes = [d['boxes'] for d in data]
        # labels = [d['labels'] for d in data]
        if idx == num_iterations:
            break
    dali_iterator.reset()

    duration = time.time() - start_time
    num_batches = min(5000, num_iterations * batch_size) / batch_size

    print(f'Total time: {duration:.4f}; '
          f'Mean time per batch: {duration / num_batches:.4f}; '
          f'Mean BPS: {num_batches / duration:.4f}; '
          f'Mean FPS: {num_batches * batch_size / duration:.4f}')
