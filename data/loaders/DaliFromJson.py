import cv2
import json
import time
import numpy as np
from tqdm import tqdm

import torch
import nvidia.dali.ops as ops
import torch.nn.functional as F
import nvidia.dali.types as types
import nvidia.dali.tfrecord as tfrec
from data.transforms import DALIRescale
from nvidia.dali.pipeline import Pipeline
from nvidia.dali.plugin.pytorch import DALIGenericIterator, TorchPythonFunction


class DataIterator:
    def __init__(self, annotations, batch_size, classes):
        """
        :param annotations: dict in format: {image_path: [{'x': x, 'y': y, 'w': w, 'h': h}]}
                            all coordinates have to be in range (0, 1)
        :param batch_size:
        :param classes: list of classes
        """
        self.batch_size = batch_size
        if isinstance(annotations, str): annotations = json.load(open(annotations))
        self.annotations = annotations
        self.paths = list(self.annotations.keys())
        self.i = 0
        self.n = len(self.annotations)
        # self.next = self.__next__
        self.class_mapping = {cls: idx+1 for idx, cls in enumerate(classes)}

    def __iter__(self):
        self.i = 0
        self.n = len(self.annotations)
        return self

    @property
    def size(self, ):
        return len(self.annotations)

    def create_detections_list(self, batch_detections):
        max_det_num = max([len(dets) for dets in batch_detections])
        bboxes = np.zeros((self.batch_size, max_det_num, 4), dtype=np.float32)
        labels = np.zeros((self.batch_size, max_det_num, 1), dtype=np.int32)
        num_dets_per_image = np.zeros((self.batch_size, 1), dtype=np.int32)
        for idx, dets in enumerate(batch_detections):
            assert len(dets), f'{dets}, {len(dets)}'
            bboxes[idx, :len(dets)] = np.array([[det['x'], det['y'],
                                                 (det['x'] + det['w']), (det['y'] + det['h'])]
                                                for det in dets])
            labels[idx, :len(dets)] = np.array([[self.class_mapping[det['class']]] for det in dets])
            num_dets_per_image[idx] = len(dets)
        bboxes = np.clip(bboxes, a_min=0, a_max=1)
        return bboxes, labels, num_dets_per_image

    def __next__(self):
        images = []
        labels = []
        # images_original = []
        for _ in range(self.batch_size):
            image_path = self.paths[self.i]
            annot = self.annotations[image_path]

            # Reading image buffer and saving to images list
            fp = open(image_path, 'rb')
            images.append(np.frombuffer(fp.read(), dtype=np.uint8))
            # fp = open(image_path, 'rb')
            # images_original.append(np.frombuffer(fp.read(), dtype=np.uint8))
            # Saving detections dict to list
            labels.append(annot)

            self.i = (self.i + 1) % self.n

        bboxes, labels, num_dets_per_image = self.create_detections_list(labels)
        return images, bboxes, labels, num_dets_per_image

    next = __next__


class DetectionPipeline(Pipeline):
    def __init__(self, iterator, batch_size, num_threads, device_id, target_size=(768, 1024), color_mean=(0., 0., 0.),
                 color_std=(1., 1., 1.)):
        # TODO придумать как ему подавать transform отдельным объектом
        exec_async = exec_pipelined = True
        if target_size is not None:
            exec_async = exec_pipelined = False

        super(DetectionPipeline, self).__init__(batch_size, num_threads, device_id, seed=125, exec_async=exec_async,
                                                exec_pipelined=exec_pipelined)

        self.target_size = target_size
        self.device = torch.device(f'cuda:{device_id}')

        self.iterator = iter(iterator)
        self.input_image = ops.ExternalSource()
        self.input_bbox = ops.ExternalSource()
        self.input_label = ops.ExternalSource()
        self.input_num_dets = ops.ExternalSource()

        # self.decode = ops.ImageDecoder(device='mixed', output_type=types.RGB)
        self.decode = ops.ImageDecoder(device='mixed', output_type=types.RGB)

        # Transforms
        self.flip = ops.Flip(device='gpu')
        self.apply_flip = ops.CoinFlip(probability=0.5)
        self.uniform = ops.Uniform(range=(0, 1))
        self.bbox_flip = ops.BbFlip(device='cpu', ltrb=True)
        self.normalize = ops.CropMirrorNormalize(device='gpu', mean=color_mean, std=color_std,
                                                 output_dtype=types.FLOAT, output_layout='CHW')
        # self.normalize = ops.CropMirrorNormalize(device='gpu', mean=[0., 0., 0.], std=[255., 255., 255.],
        #                                          output_dtype=types.FLOAT, output_layout='CHW')

        # Color augmentations params
        self.br_mult = ops.Uniform(range=(0.9, 1))
        self.br_shift = ops.Uniform(range=(0, 0.15))
        self.contr_mult = ops.Uniform(range=(0.9, 1))
        self.br_contr = ops.BrightnessContrast(device='gpu')

        self.hue = ops.Uniform(range=(0.8, 1))
        self.saturation = ops.Uniform(range=(0.8, 1))
        self.value = ops.Uniform(range=(0.8, 1))
        self.hsv = ops.Hsv(device='gpu')

        self.shape = ops.Shapes(device='gpu')
        if self.target_size is not None:
            self.resize_im_fn = TorchPythonFunction(self.resize_image_keep_aspect_ratios, num_outputs=1, device='gpu')

        self.cast = ops.Cast(device='gpu', dtype=types.FLOAT)
        self.cast_bbox = ops.Cast(device='cpu', dtype=types.FLOAT)

        self.images = None
        self.bboxes = None
        self.labels = None
        self.num_dets_per_image = None

    def resize_image_keep_aspect_ratios(self, image: torch.Tensor):
        target_size = self.target_size
        if not isinstance(target_size, list):
            target_size = list(target_size)

        old_size = image.size()[1:]  # old_size is in (height, width) format
        ratios = [float(i) / float(j) for i, j in zip(target_size, old_size)]
        min_ratio_index = 0 if ratios[0] < ratios[1] else 1
        min_ratio = ratios[min_ratio_index]

        img = F.interpolate(image.unsqueeze(0), scale_factor=min_ratio, mode='bilinear')[0]
        new_size = img.size()[1:]
        delta_w = target_size[1] - new_size[1]
        delta_h = target_size[0] - new_size[0]
        top, bottom = delta_h // 2, delta_h - (delta_h // 2)
        left, right = delta_w // 2, delta_w - (delta_w // 2)
        if sum([left, right, top, bottom]):  img = F.pad(img, [left, right, top, bottom], mode='constant', value=0)
        assert img.sum(), f'Pads: {left, right, top, bottom}; Ratio: {min_ratio}'
        return img

    def define_graph(self):
        self.images = self.input_image()
        self.bboxes = self.input_bbox()
        self.labels = self.input_label()
        self.num_dets_per_image = self.input_num_dets()

        # Decode from buffer and applying transformations
        images = self.decode(self.images)
        bboxes = self.cast_bbox(self.bboxes)
        # Applying color image augmentations
        # images = self.br_contr(images, brightness=self.br_mult(), brightness_shift=self.br_shift(),
        #                        contrast=self.contr_mult())
        #
        # images = self.hsv(images, hue=self.hue(), saturation=self.saturation(), value=self.value())

        # Applying spatial image, bboxes augmentation
        # is_flip = self.apply_flip()
        # images = self.flip(images, horizontal=is_flip)
        # if is_flip:
        #     bboxes = self.bbox_flip(bboxes, horizontal=is_flip)

        images = self.normalize(images)
        images = self.cast(images)

        orig_shapes = self.shape(images)
        if self.target_size is not None:
            images = self.resize_im_fn(images)
        return images, bboxes, self.labels, self.num_dets_per_image, orig_shapes

    def resize_bboxes(self, bboxes, orig_shapes):
        orig_shapes = [shape.data.cpu().numpy()[1:] for shape in orig_shapes]

        for image_idx in list(range(len(bboxes))):
            shape = orig_shapes[image_idx]
            ratios = [float(i) / float(j) for i, j in zip(self.target_size, shape)]
            min_ratio_index = 0 if ratios[0] < ratios[1] else 1
            ratio = ratios[min_ratio_index]
            new_size = [s*ratio for s in shape]
            bboxes[image_idx] *= torch.tensor([*new_size[::-1], *new_size[::-1]])

            delta_w = self.target_size[1] - new_size[1]
            delta_h = self.target_size[0] - new_size[0]
            top = (delta_h // 2)  # / self.target_size[0]
            left = (delta_w // 2)  # / self.target_size[1]
            bboxes[image_idx, :, 0] += left
            bboxes[image_idx, :, 1] += top
            bboxes[image_idx, :, 2] += left
            bboxes[image_idx, :, 3] += top

        return bboxes

    def preprocess_boxes(self, images, bboxes, orig_shapes=None):
        if self.target_size is None:
            # Просто делаем unnormalize боксов
            image_size = images.size()[-2:]
            bboxes *= torch.tensor([*image_size[::-1], *image_size[::-1]])
            return bboxes
        else:
            assert orig_shapes is not None, f'Pass original image shapes for bboxes preprocessing, passed: {orig_shapes}'
            return self.resize_bboxes(bboxes, orig_shapes)

    def iter_setup(self):
        try:
            images, bboxes, labels, num_dets_per_image = next(self.iterator)
            self.feed_input(self.images, images, "HWC")
            self.feed_input(self.bboxes, bboxes)
            self.feed_input(self.labels, labels)
            self.feed_input(self.num_dets_per_image, num_dets_per_image)
        except StopIteration:
            self.iterator = iter(self.iterator)


if __name__ == '__main__':
    import torch
    # annotations = json.load(open('/home/nlab/Experiments/Detector/resources/obj_365_train_person_v2.json'))
    annotations = json.load(open('/home/nlab/Experiments/Detector/resources/face_annotations_v3.json'))
    # annotations = json.load(open('/home/nlab/Experiments/Detector/resources/coco_FAKE_DETS.json'))

    # Работает ~ 69 FPS (0.014 per batch)
    batch_size = 1
    num_threads = 4
    num_iterations = 10000

    iterator = DataIterator(annotations, batch_size, ('face',))
    pipeline_0 = DetectionPipeline(iterator, batch_size, target_size=(768, 1024), num_threads=num_threads, device_id=0)
    # pipeline_1 = DetectionPipeline(iterator, batch_size, num_threads=num_threads, device_id=1)
    pipeline_0.build()
    # pipeline_1.build()

    dali_iterator = DALIGenericIterator(pipelines=[pipeline_0], output_map=['images', 'boxes', 'labels', 'num_dets',
                                                                            'orig_shapes'],
                                        size=iterator.size, dynamic_shape=True)
    print('Iterator created')
    start_time = time.time()
    for idx, data in tqdm(enumerate(dali_iterator)):
        images = torch.cat([d['images'] for d in data])
        boxes_orig = torch.cat([d['boxes'] for d in data])
        orig_shapes = torch.cat([d['orig_shapes'] for d in data])
        print('Original images shapes:', orig_shapes)
        bboxes = dali_iterator._pipes[0].preprocess_boxes(images=images, bboxes=boxes_orig, orig_shapes=orig_shapes)

        # bboxes = torch.cat([d['boxes'] for d in data])
        # ratios = torch.cat([d['ratios'] for d in data])
        # pads = torch.cat([d['pads'] for d in data])
        # print(ratios)
        # print(pads)
        # resized_shapes = torch.cat([d['resized_shapes'] for d in data])

        print(images.shape)
        if idx == num_iterations:
            break

        for image, boxes in zip(images, bboxes):
            image = image.cpu().numpy().transpose((1, 2, 0)).astype(np.uint8).copy()
            boxes = boxes.numpy()

            for box in boxes:
                x1, y1, x2, y2 = list(map(int, [box[0],  # *resized_shape[1],
                                                box[1],  # *resized_shape[0],
                                                box[2],  # *resized_shape[1],
                                                box[3]]))  # *resized_shape[0]]))
                print(x1, y1, x2, y2)
                image = cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)

            cv2.imshow('image_1', image[..., ::-1])

            if cv2.waitKey() == 27:
                break
        print('='*80)
    duration = time.time() - start_time
    num_batches = min(len(annotations), num_iterations*batch_size) / batch_size

    print(f'Total time: {duration:.4f}; '
          f'Mean time per batch: {duration/num_batches:.4f}; '
          f'Mean BPS: {num_batches/duration:.4f}; '
          f'Mean FPS: {num_batches*batch_size/duration:.4f}')

