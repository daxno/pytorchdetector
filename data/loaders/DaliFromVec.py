import cv2
import json
import time
import numpy as np
from tqdm import tqdm

import nvidia.dali.ops as ops
import nvidia.dali.types as types
import nvidia.dali.tfrecord as tfrec
from nvidia.dali.pipeline import Pipeline
from nvidia.dali.plugin.pytorch import DALIGenericIterator


class DataIterator:
    def __init__(self, annotations, batch_size, classes):
        """
        :param annotations: dict in format: {image_path: [{'x': x, 'y': y, 'w': w, 'h': h}]}
                            all coordinates have to be in range (0, 1)
        :param batch_size:
        :param classes: list of classes
        """
        self.annotations = annotations
        self.batch_size = batch_size
        self.paths = list(self.annotations.keys())
        self.i = 0
        self.n = len(self.annotations)
        # self.next = self.__next__
        self.class_mapping = {cls: idx for idx, cls in enumerate(classes)}

    def __iter__(self):
        self.i = 0
        self.n = len(self.annotations)
        return self

    @property
    def size(self, ):
        return len(self.annotations)

    def create_detections_list(self, batch_detections):
        max_det_num = max([len(dets) for dets in batch_detections])
        bboxes = np.zeros((self.batch_size, max_det_num, 4), dtype=np.float32)
        labels = np.zeros((self.batch_size, max_det_num, 1), dtype=np.int32) - 1
        for idx, dets in enumerate(batch_detections):
            bboxes[idx, :len(dets)] = np.array([[det['x'], det['y'],
                                                 (det['x'] + det['w']), (det['y'] + det['h'])]
                                                for det in dets])
            labels[idx, :len(dets)] = np.array([[self.class_mapping[det['class']]] for det in dets])
        bboxes = np.clip(bboxes, a_min=0, a_max=1)
        return bboxes, labels

    def __next__(self):
        images = []
        labels = []
        for _ in range(self.batch_size):
            image_path = self.paths[self.i]
            annot = self.annotations[image_path]

            # Reading image buffer and saving to images list
            fp = open(image_path, 'rb')
            images.append(np.frombuffer(fp.read(), dtype=np.uint8))
            # Saving detections dict to list
            labels.append(annot)

            self.i = (self.i + 1) % self.n

        bboxes, labels = self.create_detections_list(labels)
        return images, bboxes, labels

    next = __next__


class DetectionPipeline(Pipeline):
    def __init__(self, rec_path, idx_path, batch_size, num_threads, device_id, target_shape=(768, 1024), num_gpus=1):
        # TODO придумать как ему подавать transform отдельным объектом
        super(DetectionPipeline, self).__init__(batch_size, num_threads, device_id, seed=125)

        self.target_shape = target_shape
        self.target_x = ops.Uniform(range=(self.target_shape[1], self.target_shape[1]))
        self.target_y = ops.Uniform(range=(self.target_shape[0], self.target_shape[0]))

        self.input = ops.MXNetReader(path=[rec_path], index_path=[idx_path],
                                     random_shuffle=True, shard_id=device_id, num_shards=num_gpus)

        self.decode = ops.ImageDecoder(device="mixed", output_type=types.RGB)

        # Transforms
        self.flip = ops.Flip(device='gpu')
        self.apply_flip = ops.CoinFlip(probability=0.5)
        self.uniform = ops.Uniform(range=(0, 1))
        self.bbox_flip = ops.BbFlip(device='cpu', ltrb=True)
        self.normalize = ops.CropMirrorNormalize(device='gpu', mean=[0., 0., 0.], std=[1., 1., 1.],
                                                 output_dtype=types.FLOAT, output_layout='CHW')

        # Color augmentations params
        self.apply_br_contr = ops.CoinFlip(probability=0.5)
        self.br_mult = ops.Uniform(range=(0.8, 1))
        self.br_shift = ops.Uniform(range=(0, 0.2))
        self.contr_mult = ops.Uniform(range=(0.8, 1))
        self.br_contr = ops.BrightnessContrast(device='gpu')

        self.apply_hsv = ops.CoinFlip(probability=0.5)
        self.hue = ops.Uniform(range=(0.7, 1))
        self.saturation = ops.Uniform(range=(0.7, 1))
        self.value = ops.Uniform(range=(0.7, 1))
        self.hsv = ops.Hsv(device='gpu')
        # Affine transform
        # self.transform_matrix_getter = TransformMatrixGetter(batch_size)
        # self.affine_transorm = ops.WarpAffine(device='gpu')
        # self.affine = ops.Uniform(range=(-1, 1))
        # TODO add reading from .rec file (MXNetReader)

        self.prospective_crop = ops.RandomBBoxCrop(
            device="cpu",
            aspect_ratio=[0.25, 2.0],
            thresholds=[0.1, 0.3, 0.5],
            scaling=[0.4, 1.0],
            ltrb=True,
            num_attempts=1
        )
        self.resize = ops.Resize(device='gpu')
        self.slice = ops.Slice(device='gpu')

        self.cast = ops.Cast(device='gpu', dtype=types.FLOAT)
        self.cast_bbox = ops.Cast(device='cpu', dtype=types.FLOAT)

    def define_graph(self):
        images, labels = self.input()

        # Decode from buffer and applying transformations
        images = self.decode(images)
        # Applying color image augmentations
        # images = self.br_contr(images, brightness=self.br_mult(), brightness_shift=self.br_shift(),
        #                        contrast=self.contr_mult())
        #
        # images = self.hsv(images, hue=self.hue(), saturation=self.saturation(), value=self.value())

        # Applying spatial image, bboxes augmentation
        # is_flip = self.apply_flip()
        # images = self.flip(images, horizontal=is_flip)
        # bboxes = self.cast_bbox(self.bboxes)
        # if is_flip:
        #     bboxes = self.bbox_flip(bboxes, horizontal=is_flip)

        # TODO почему-то выскакривает SizeError
        # crop_begin, crop_size, bboxes, labels = self.prospective_crop(bboxes, self.labels)
        # images = self.slice(images, crop_begin, crop_size)
        images = self.resize(images, resize_x=self.target_x(), resize_y=self.target_y())

        images = self.normalize(images)
        output = self.cast(images)
        return output, labels  # bboxes, self.labels


if __name__ == '__main__':
    batch_size = 16
    num_threads = 8
    num_iterations = 500
    rec_path = '/home/nlab/Experiments/Detector/resources/test.rec'
    idx_path = '/home/nlab/Experiments/Detector/resources/test.idx'
    pipeline_0 = DetectionPipeline(rec_path, idx_path, batch_size, num_threads=num_threads, device_id=0, num_gpus=2)
    pipeline_1 = DetectionPipeline(rec_path, idx_path, batch_size, num_threads=num_threads, device_id=1, num_gpus=2)
    pipeline_0.build()
    pipeline_1.build()

    pipelines = [pipeline_0, pipeline_1]
    dali_iterator = DALIGenericIterator(pipelines=pipelines, output_map=['data', 'labels'],
                                        size=5000, dynamic_shape=False)
    print('Iterator created')
    start_time = time.time()
    for idx, data in tqdm(enumerate(dali_iterator)):
        images = [d['data'] for d in data]
        labels = [d['labels'] for d in data]
        if idx == num_iterations:
            break
        image = images[0][2].cpu().numpy().transpose((1, 2, 0)).astype(np.uint8)
        # boxes = bboxes[0][2].numpy()
        # for box in boxes:
        #     x1, y1, x2, y2 = list(map(int, [box[0]*1024, box[1]*768, box[2]*1024, box[3]*768]))
        #     image = cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)

        cv2.imshow('image_1', image)
        image = images[1][2].cpu().numpy().transpose((1, 2, 0)).astype(np.uint8)
        # boxes = bboxes[1][2].numpy()
        # for box in boxes:
        #     x1, y1, x2, y2 = list(map(int, [box[0] * 1024, box[1] * 768, box[2] * 1024, box[3] * 768]))
        #     image = cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
        cv2.imshow('image_2', image)

        if cv2.waitKey() == 27:
            break
    duration = time.time() - start_time
    num_batches = 5000 / (batch_size*len(pipelines))

    print(f'Total time: {duration:.4f}; '
          f'Mean time per batch: {duration/num_batches:.4f}; '
          f'Mean BPS: {num_batches/duration:.4f}; '
          f'Mean FPS: {num_batches*batch_size*len(pipelines)/duration:.4f}')

