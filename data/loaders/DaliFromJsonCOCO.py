import json
import torch
import ctypes
from math import ceil
from pycocotools.coco import COCO
from contextlib import redirect_stdout
from nvidia.dali import pipeline, ops, types
from nvidia.dali.plugin.pytorch import DALIGenericIterator


class DetectionPipeline(pipeline.Pipeline):
    def __init__(self, annotations_path, data_path, batch_size, num_threads, device_id, is_train, stride=128,
                 target_size=(768, 1024), color_mean=(0., 0., 0.), color_std=(1., 1., 1.)):
        super(DetectionPipeline, self).__init__(batch_size, num_threads, device_id, seed=125)
        self.training = is_train
        self.stride = stride  # Хз чо это, взял из примера пайплайна
        self.iter = 0

        self.output_names = ['original_shapes', 'ids']  # 'bboxes', 'labels', 'original_shapes']
        self.reader = ops.COCOReader(annotations_file=annotations_path,
                                     file_root=data_path,
                                     ltrb=True,  # is bboxes in xyxy format
                                     ratio=True,  # bboxes returned values as expressed as ratio
                                     # w.r.t. to the image width and height.
                                     shuffle_after_epoch=True,
                                     save_img_ids=True)

        self.decode_train = ops.ImageDecoder(device='mixed', output_type=types.RGB)
        self.bbox_flip = ops.BbFlip(device='cpu', ltrb=True)
        self.image_flip = ops.Flip(device='gpu')
        self.coin_flip = ops.CoinFlip(probability=0.5)

        self.resize = ops.Resize(device='gpu', resize_longer=max(target_size), save_attrs=True)
        self.paste = ops.Paste(device='gpu', ratio=1., paste_x=0, paste_y=0, fill_value=0)
        self.pad = ops.Pad(device='gpu', axes=[])
        self.normalize = ops.CropMirrorNormalize(device='gpu', mean=color_mean, std=color_std,
                                                 output_dtype=types.FLOAT, output_layout='CHW')
        self.shape = ops.Shapes(device='gpu')

    def define_graph(self):
        images, bboxes, labels, img_ids = self.reader()
        images = self.decode_train(images)
        original_shapes = self.shape(images)
        # images_resized, original_shapes = self.resize(images)
        # images_normalized = self.normalize(self.paste(self.pad(images)))

        return original_shapes, img_ids  # images_normalized, bboxes, labels, original_shapes


class DaliDataIterator:
    """Data loader for data parallel using Dali"""

    def __init__(self, annotations_path, data_path, target_size, max_size, batch_size, stride=128,
                 training=True, preprocessing_device_id=0, num_threads=4, color_mean=(0., 0., 0.), color_std=(1., 1., 1.)):
        self.training = training
        self.resize = target_size
        self.max_size = max_size
        self.stride = stride
        self.batch_size = batch_size
        self.mean = [255. * x for x in [0.485, 0.456, 0.406]]
        self.std = [255. * x for x in [0.229, 0.224, 0.225]]
        self.path = data_path

        # Setup COCO
        with redirect_stdout(None):
            self.coco = COCO(annotations_path)
        self.ids = list(self.coco.imgs.keys())
        if 'categories' in self.coco.dataset:
            self.categories_inv = {k: i for i, k in enumerate(self.coco.getCatIds())}
        self.categories_inv = {k: i for i, k in enumerate(self.coco.getCatIds())}

        self.pipe = DetectionPipeline(annotations_path, data_path, batch_size, num_threads, preprocessing_device_id,
                                      training, target_size=target_size, max_size=max_size, color_mean=color_mean,
                                      color_std=color_std)

        self.pipe.build()

    def __repr__(self):
        return '\n'.join([
            '    loader: dali',
            '    resize: {}, max: {}'.format(self.resize, self.max_size),
        ])

    def __len__(self):
        return ceil(len(self.ids) / self.batch_size)

    def __iter__(self):
        for _ in range(self.__len__()):

            data, ratios, ids, num_detections = [], [], [], []
            dali_data, dali_boxes, dali_labels, dali_ids, dali_attrs, dali_resized_shape = self.pipe.run()

            for l in range(len(dali_boxes)):
                num_detections.append(dali_boxes.at(l).shape[0])

            pyt_targets = -1 * torch.ones([len(dali_boxes), max(max(num_detections), 1), 5])

            for batch in range(self.batch_size):
                id = int(dali_ids.at(batch)[0])

                # Convert dali tensor to pytorch
                dali_tensor = dali_data.at(batch)
                tensor_shape = dali_tensor.shape()

                datum = torch.zeros(dali_tensor.shape(), dtype=torch.float, device=torch.device('cuda'))
                c_type_pointer = ctypes.c_void_p(datum.data_ptr())
                dali_tensor.copy_to_external(c_type_pointer)

                # Calculate image resize ratio to rescale boxes
                prior_size = dali_attrs.as_cpu().at(batch)
                resized_size = dali_resized_shape.as_cpu().at(batch)
                # print(resized_size)
                # resized_size = dali_resize_img.at(batch).shape()
                ratio = max(resized_size) / max(prior_size)

                if self.training:
                    # Rescale boxes
                    b_arr = dali_boxes.at(batch)
                    num_dets = b_arr.shape[0]
                    if num_dets is not 0:
                        pyt_bbox = torch.from_numpy(b_arr).float()

                        pyt_bbox[:, 0] *= float(prior_size[1])
                        pyt_bbox[:, 1] *= float(prior_size[0])
                        pyt_bbox[:, 2] *= float(prior_size[1])
                        pyt_bbox[:, 3] *= float(prior_size[0])
                        # (l,t,r,b) ->  (x,y,w,h) == (l,r, r-l, b-t)
                        pyt_bbox[:, 2] -= pyt_bbox[:, 0]
                        pyt_bbox[:, 3] -= pyt_bbox[:, 1]
                        pyt_targets[batch, :num_dets, :4] = pyt_bbox * ratio

                    # Arrange labels in target tensor
                    l_arr = dali_labels.at(batch)
                    if num_dets is not 0:
                        pyt_label = torch.from_numpy(l_arr).float()
                        pyt_label -= 1  # Rescale labels to [0,79] instead of [1,80]
                        pyt_targets[batch, :num_dets, 4] = pyt_label.squeeze()

                ids.append(id)
                data.append(datum.unsqueeze(0))
                ratios.append(ratio)

            data = torch.cat(data, dim=0)

            if self.training:
                pyt_targets = pyt_targets.cuda(non_blocking=True)

                yield data, pyt_targets

            else:
                ids = torch.Tensor(ids).int().cuda(non_blocking=True)
                ratios = torch.Tensor(ratios).cuda(non_blocking=True)

                yield data, ids, ratios


if __name__ == '__main__':
    from tqdm import tqdm
    import numpy as np
    import time
    import cv2

    annot_path = '/home/nlab/Experiments/Detector/resources/obj_365_train_coco_format.json'
    # num_images = len(json.load(open(annot_path)).keys())
    images_path = '/home/nlab/Datasets/Objects365/train_person'
    batch_size = 16
    num_threads = 4
    device_id = 1
    detection_pipeline = DetectionPipeline(annot_path, images_path, batch_size, num_threads, device_id, is_train=True,
                                           target_size=(768, 1024))
    detection_pipeline.build()
    # iterator = DaliDataIterator(annot_path, images_path, target_size=800, max_size=1333, batch_size=batch_size)
    num_images = 2000
    iterator = DALIGenericIterator([detection_pipeline], output_map=detection_pipeline.output_names, size=len(json.load(open(annot_path))['images']),
                                   auto_reset=False, dynamic_shape=False)

    id2shape_mapping = {}

    start_time = time.time()
    for idx, data in tqdm(enumerate(iterator), total=iterator._size//batch_size):
        # images = [d['images'].data.cpu().numpy().transpose((1, 2, 0)) for d in data]
        # bboxes = [d['bboxes'].numpy() for d in data]
        original_shapes = data[0]['original_shapes'].cpu().numpy()
        ids = data[0]['ids'].cpu().numpy().tolist()

        id2shape_mapping.update({i[0]: shape.tolist() for i, shape in zip(ids, original_shapes)})


        # for image, image_boxes, orig_shape in zip(images, bboxes, original_shapes):
        #     ratio = 1024/max(orig_shape)
        #     for b in image_boxes:
        #         x1, y1, x2, y2 = list(map(int, [b[0] * ratio * orig_shape[1],
        #                                         b[1] * ratio * orig_shape[0],
        #                                         b[2] * ratio * orig_shape[1],
        #                                         b[3] * ratio * orig_shape[0]]))
        #         image = cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
        #     cv2.imshow('image', image.copy()[..., ::-1])
        #     cv2.waitKey()
        # print(images.shape)
        # print(targets.shape)
        # for image in images:
        #     image = image.transpose((1, 2, 0)).astype(np.uint8)
        #     cv2.imshow('image', image[..., ::-1])
        #     if cv2.waitKey() == 27:
        #         break
    json.dump(id2shape_mapping, open('/home/nlab/Datasets/Objects365/notebooks/id2shape_mapping.json', 'w'))

