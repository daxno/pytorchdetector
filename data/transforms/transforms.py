import cv2
import math
import random
import numpy as np


class RandomErasing(object):
    """ Randomly selects a rectangle region in an image and erases its pixels.
        'Random Erasing Data Augmentation' by Zhong et al.
        See https://arxiv.org/pdf/1708.04896.pdf
    Args:
         probability: The probability that the Random Erasing operation will be performed.
         sl: Minimum proportion of erased area against input image.
         sh: Maximum proportion of erased area against input image.
         r1: Minimum aspect ratio of erased area.
         mean: Erasing value.
    """

    def __init__(self, probability=0.5, sl=0.02, sh=0.4, r1=0.3, mean=(0.4914, 0.4822, 0.4465)):
        self.probability = probability
        self.mean = mean
        self.sl = sl
        self.sh = sh
        self.r1 = r1

    def __call__(self, img):

        if random.uniform(0, 1) > self.probability:
            return img

        for attempt in range(100):
            area = img.size()[1] * img.size()[2]

            target_area = random.uniform(self.sl, self.sh) * area
            aspect_ratio = random.uniform(self.r1, 1 / self.r1)

            h = int(round(math.sqrt(target_area * aspect_ratio)))
            w = int(round(math.sqrt(target_area / aspect_ratio)))

            if w < img.size()[2] and h < img.size()[1]:
                x1 = random.randint(0, img.size()[1] - h)
                y1 = random.randint(0, img.size()[2] - w)
                if img.size()[0] == 3:
                    img[0, x1:x1 + h, y1:y1 + w] = self.mean[0]
                    img[1, x1:x1 + h, y1:y1 + w] = self.mean[1]
                    img[2, x1:x1 + h, y1:y1 + w] = self.mean[2]
                else:
                    img[0, x1:x1 + h, y1:y1 + w] = self.mean[0]
                return img

        return img


class ResizeKeepAspectRatio:
    def __init__(self, target_size, size_jitter=None):
        assert target_size[0] <= target_size[1]
        self.target_size = target_size
        self.size_jitter = size_jitter

    def __call__(self, force_apply, image, bboxes=None, category_ids=None):
        """
        :param image:
        :param bboxes: in xywh size
        """
        target_size = self.target_size
        if not isinstance(target_size, list):
            target_size = list(target_size)
        if self.size_jitter is not None:
            target_size[0] = np.random.randint(self.size_jitter[0], self.size_jitter[-1])
            jitter_ratio = target_size[0] / self.target_size[0]
            target_size[1] = int(target_size[1] * jitter_ratio)
            # print(target_size)
        old_size = image.shape[:2]  # old_size is in (height, width) format
        ratios = [float(i) / float(j) for i, j in zip(target_size, old_size)]
        min_ratio_index = 0 if ratios[0] < ratios[1] else 1
        min_ratio = ratios[min_ratio_index]

        new_size = tuple([int(x * min_ratio) for x in old_size])

        # new_size should be in (width, height) format
        img = cv2.resize(image, (new_size[1], new_size[0]))
        # img = cv2.resize(image, None, fx=min_ratio, fy=min_ratio)
        delta_w = target_size[1] - new_size[1]
        delta_h = target_size[0] - new_size[0]
        top, bottom = delta_h // 2, delta_h - (delta_h // 2)
        left, right = delta_w // 2, delta_w - (delta_w // 2)
        new_im = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=(0, 0, 0))

        if bboxes is not None:
            if not isinstance(bboxes, np.ndarray):
                bboxes = np.array(bboxes)
            bboxes *= min_ratio
            bboxes[:, 0] += left
            bboxes[:, 1] += top

        return {
            'image': new_im,
            'bboxes': bboxes,
            'category_ids': category_ids,
            'ratio': min_ratio,
            'pad': [left, top]
        }


class Normalize:
    def __init__(self, mean, std, max_val=1.):
        self.mean = np.array(mean, dtype=np.float32)
        self.std = np.array(std, dtype=np.float32)
        self.max_val = max_val

    def __call__(self, force_apply, image, bboxes, **params):
        denominator = np.reciprocal(self.std, dtype=np.float32)

        image = (image / self.max_val).astype(np.float32)
        image -= self.mean
        image *= denominator

        return {
            'image': image,
            'bboxes': bboxes,
            **params
        }


class DALIRescale:
    def __init__(self, target_size):
        self.target_size = target_size

    def __call__(self, image, bboxes):
        print(type(image), type(bboxes))
        target_size = self.target_size
        if not isinstance(target_size, list):
            target_size = list(target_size)

        old_size = image.shape[:2]  # old_size is in (height, width) format
        ratios = [float(i) / float(j) for i, j in zip(target_size, old_size)]
        min_ratio_index = 0 if ratios[0] < ratios[1] else 1
        min_ratio = ratios[min_ratio_index]

        new_size = tuple([int(x * min_ratio) for x in old_size])

        # new_size should be in (width, height) format
        img = cv2.resize(image, (new_size[1], new_size[0]))
        # img = cv2.resize(image, None, fx=min_ratio, fy=min_ratio)
        delta_w = target_size[1] - new_size[1]
        delta_h = target_size[0] - new_size[0]
        top, bottom = delta_h // 2, delta_h - (delta_h // 2)
        left, right = delta_w // 2, delta_w - (delta_w // 2)
        new_im = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=(0, 0, 0))

        if bboxes is not None:
            if not isinstance(bboxes, np.ndarray):
                bboxes = np.array(bboxes)
            bboxes *= min_ratio
            bboxes[:, 0] += left
            bboxes[:, 1] += top

        return new_im, bboxes
