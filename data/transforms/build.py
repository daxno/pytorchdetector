from .transforms import ResizeKeepAspectRatio, Normalize
from albumentations.pytorch import ToTensor
import albumentations as A


def build_transforms(cfg, is_train=True):
    normalize_transform = Normalize(mean=cfg.DATA.MEAN, std=cfg.DATA.STD, max_val=255.)
    if is_train:
        transform = A.Compose([
            # A.HorizontalFlip(),
            ResizeKeepAspectRatio(cfg.DATA.TRAIN_SIZE),
            A.OneOf([
                A.Blur(),
                A.RandomBrightnessContrast(),
                A.RandomGamma()
            ]),
            normalize_transform,
            ToTensor()
        ], bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))
    else:
        transform = A.Compose([
            # A.Resize(*cfg.DATA.SIZE_TEST),
            ResizeKeepAspectRatio(cfg.DATA.SIZE_TEST),
            normalize_transform,
            ToTensor(),
        ], bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))

    return transform
