import torch
import numpy as np
from modeling.ATSS import get_atss_box_coder
from structures import BoxList, cat_boxlist, boxlist_iou

INF = 1e9


class TargetGenerator:
    def __init__(self, cfg, box_coder, anchors=None, target_image_size=None, target_device='cuda:1'):
        self.target_device = torch.device(target_device)
        self.cfg = cfg
        self.box_coder = box_coder
        self.anchors = anchors
        self.anchors = [anch.to(self.target_device) for anch in anchors]

        self.image_size = target_image_size

    def __call__(self, boxes_batch: torch.Tensor, labels_batch: torch.Tensor, num_dets_per_image: torch.Tensor,
                 image_size=None):
        """
        :param boxes: in xyxy format
        :param labels: int
        :return:
        """
        anchors = [self.anchors for _ in range(1)]
        batch_cls_targets, batch_regr_targets, batch_ct_targets = [], [], []
        # boxes_batch = (boxes_batch * torch.tensor([*(self.image_size[::-1]), *(self.image_size[::-1])])).type(torch.int32)  # Unnormalize
        assert image_size is not None or self.image_size is not None
        if self.image_size is None: self.image_size = image_size
        for boxes, labels, num_dets in zip(boxes_batch, labels_batch, num_dets_per_image):
            boxes = boxes[:num_dets]
            # print(bool((boxes[:, 2] - boxes[:, 0]).sum().item()) and bool((boxes[:, 3] - boxes[:, 1]).sum().item()))
            assert bool((boxes[:, 2] - boxes[:, 0]).sum().item()) and bool((boxes[:, 3] - boxes[:, 1]).sum().item()), boxes # Checking is box in xyxy format
            labels = labels[:num_dets]

            if boxes.device != self.target_device: boxes = boxes.to(self.target_device)
            if labels.device != self.target_device: labels = labels.to(self.target_device)

            # targets = BoxList(boxes.reshape(-1, 4), self.image_size[::-1], mode='xyxy')
            targets = BoxList(boxes.reshape(-1, 4), self.image_size[::-1], mode='xyxy')
            targets.add_field('labels', labels)

            cls_targets, regr_targets = self._prepare_cls_regr_targets(targets, self.anchors)
            labels_flatten = torch.cat(cls_targets, dim=0)
            reg_targets_flatten = torch.cat(regr_targets, dim=0)
            # anchors_flatten = torch.cat([anch.bbox for anch in self.anchors], dim=0)
            # anchors_flatten = cat_boxlist(self.anchors).bbox
            anchors_flatten = torch.cat([cat_boxlist(anchors_per_image).bbox for anchors_per_image in anchors], dim=0)

            pos_inds = torch.nonzero(labels_flatten > 0)[:, 0]  # .squeeze(1)
            if pos_inds.numel() > 0:
                reg_targets_flatten = reg_targets_flatten[pos_inds]
                anchors_flatten = anchors_flatten[pos_inds]
                centerness_targets = self._prepare_centerness_targets(reg_targets_flatten,
                                                                      anchors_flatten).type(torch.float32)
            else:
                centerness_targets = []
            batch_cls_targets.append(torch.cat(cls_targets))
            batch_regr_targets.append(torch.cat(regr_targets))
            batch_ct_targets.append(centerness_targets)
        targets = {
            'cls_target': batch_cls_targets,
            'reg_target': batch_regr_targets,
            'centerness_target': batch_ct_targets
        }
        return targets

    def _prepare_cls_regr_targets(self, targets, anchors):
        cls_labels = []
        reg_targets = []

        targets_per_im = targets
        assert targets_per_im.mode == "xyxy"
        bboxes_per_im = targets_per_im.bbox
        labels_per_im = targets_per_im.get_field("labels")
        anchors_per_im = cat_boxlist(anchors)
        num_gt = bboxes_per_im.shape[0]

        num_anchors_per_level = [len(anchors_per_level.bbox) for anchors_per_level in anchors]
        ious = boxlist_iou(anchors_per_im, targets_per_im)

        gt_cx = (bboxes_per_im[:, 2] + bboxes_per_im[:, 0]) / 2.0
        gt_cy = (bboxes_per_im[:, 3] + bboxes_per_im[:, 1]) / 2.0
        gt_points = torch.stack((gt_cx, gt_cy), dim=1)

        anchors_cx_per_im = (anchors_per_im.bbox[:, 2] + anchors_per_im.bbox[:, 0]) / 2.0
        anchors_cy_per_im = (anchors_per_im.bbox[:, 3] + anchors_per_im.bbox[:, 1]) / 2.0
        anchor_points = torch.stack((anchors_cx_per_im, anchors_cy_per_im), dim=1)

        distances = (anchor_points[:, None, :] - gt_points[None, :, :]).pow(2).sum(-1).sqrt()

        # Selecting candidates based on the center distance between anchor box and object
        candidate_idxs = []
        star_idx = 0
        for level, anchors_per_level in enumerate(anchors):
            end_idx = star_idx + num_anchors_per_level[level]
            distances_per_level = distances[star_idx:end_idx, :]
            _, topk_idxs_per_level = distances_per_level.topk(self.cfg.MODEL.ATSS.TOPK, dim=0, largest=False)
            candidate_idxs.append(topk_idxs_per_level + star_idx)
            star_idx = end_idx
        candidate_idxs = torch.cat(candidate_idxs, dim=0)
        # Using the sum of mean and standard deviation as the IoU threshold to select final positive samples
        candidate_ious = ious[candidate_idxs, torch.arange(num_gt)]
        iou_mean_per_gt = candidate_ious.mean(0)
        iou_std_per_gt = candidate_ious.std(0)
        iou_thresh_per_gt = iou_mean_per_gt + iou_std_per_gt
        is_pos = candidate_ious >= iou_thresh_per_gt[None, :]

        # Limiting the final positive samples’ center to object
        anchor_num = anchors_cx_per_im.shape[0]
        for ng in range(num_gt):
            candidate_idxs[:, ng] += ng * anchor_num
        e_anchors_cx = anchors_cx_per_im.view(1, -1).expand(num_gt, anchor_num).contiguous().view(-1)
        e_anchors_cy = anchors_cy_per_im.view(1, -1).expand(num_gt, anchor_num).contiguous().view(-1)
        candidate_idxs = candidate_idxs.view(-1)
        l = e_anchors_cx[candidate_idxs].view(-1, num_gt) - bboxes_per_im[:, 0]
        t = e_anchors_cy[candidate_idxs].view(-1, num_gt) - bboxes_per_im[:, 1]
        r = bboxes_per_im[:, 2] - e_anchors_cx[candidate_idxs].view(-1, num_gt)
        b = bboxes_per_im[:, 3] - e_anchors_cy[candidate_idxs].view(-1, num_gt)
        is_in_gts = torch.stack([l, t, r, b], dim=1).min(dim=1)[0] > 0.01
        is_pos = is_pos & is_in_gts

        # if an anchor box is assigned to multiple gts, the one with the highest IoU will be selected.
        ious_inf = torch.full_like(ious, -INF).t().contiguous().view(-1)
        index = candidate_idxs.view(-1)[is_pos.view(-1)]
        ious_inf[index] = ious.t().contiguous().view(-1)[index]
        ious_inf = ious_inf.view(num_gt, -1).t()

        anchors_to_gt_values, anchors_to_gt_indexs = ious_inf.max(dim=1)
        cls_labels_per_im = labels_per_im[anchors_to_gt_indexs]
        cls_labels_per_im[anchors_to_gt_values == -INF] = 0
        matched_gts = bboxes_per_im[anchors_to_gt_indexs]

        # matched_gts = matched_gts.to(self.device)
        # anchors_per_im = anchors_per_im.to(self.device)

        reg_targets_per_im = self.box_coder.encode(matched_gts, anchors_per_im.bbox)
        # cls_labels.append(cls_labels_per_im.to(self.device))
        cls_labels.append(cls_labels_per_im)
        reg_targets.append(reg_targets_per_im)

        return cls_labels, reg_targets

    def _prepare_centerness_targets(self, regr_targets, anchors):
        gts = self.box_coder.decode(regr_targets, anchors)
        anchors_cx = (anchors[:, 2] + anchors[:, 0]) / 2
        anchors_cy = (anchors[:, 3] + anchors[:, 1]) / 2
        l = anchors_cx - gts[:, 0]
        t = anchors_cy - gts[:, 1]
        r = gts[:, 2] - anchors_cx
        b = gts[:, 3] - anchors_cy
        left_right = torch.stack([l, r], dim=1)
        top_bottom = torch.stack([t, b], dim=1)
        centerness = torch.sqrt((left_right.min(dim=-1)[0] / left_right.max(dim=-1)[0]) * (
                    top_bottom.min(dim=-1)[0] / top_bottom.max(dim=-1)[0]))
        assert not torch.isnan(centerness).any()
        return centerness


def get_atss_target_generator(cfg, anchors, target_size, target_generator_device):
    box_coder = get_atss_box_coder(cfg)
    return TargetGenerator(cfg, box_coder, anchors, target_size, target_generator_device)


if __name__ == '__main__':
    from tqdm import tqdm
    from modeling.ATSS import get_atss_box_coder
    from layers import get_anchor_generator_atss
    from config import cfg
    from data.loaders.DaliFromJson import DataIterator, DetectionPipeline
    from nvidia.dali.plugin.pytorch import DALIGenericIterator
    from structures import to_image_list
    from modeling import get_hbonet_fpn
    from modeling import get_atss_model

    annotations_path = cfg.PATHS.TRAIN_ANNOTATION
    batch_size = cfg.DATALOADER.TRAIN_BS
    classes = cfg.DATA.CLASSES
    iterator = DataIterator(annotations_path, 16, classes)
    color_mean, color_std = cfg.DATA.MEAN, cfg.DATA.STD

    # Generate anchors
    anchor_generator = get_anchor_generator_atss(cfg)
    image_list = to_image_list(torch.zeros((1, 3, 768, 1024)))
    backbone = get_hbonet_fpn(cfg)
    feat_maps = backbone(torch.zeros((1, 3, 768, 1024)))
    anchors = anchor_generator.forward(image_list, feat_maps)[0]
    print('Anchors generated')

    # TODO WARNING В DETECTIONPIPELINE НЕТ РЕСАЙЗА ИЗОБРАЖЕНИЙ
    dali_loader = DetectionPipeline(iterator, 16, num_threads=4, device_id=1, target_shape=(768, 1024),
                                    color_mean=color_mean, color_std=color_std)
    dali_loader.build()
    iterator = DALIGenericIterator([dali_loader], ['images', 'boxes', 'labels', 'num_dets'], size=10000, auto_reset=True,
                                   dynamic_shape=True)

    target_generator = TargetGenerator(cfg, get_atss_box_coder(cfg), anchors, (768, 1024))

    for data in tqdm(iterator):
        images = data[0]['images']
        boxes = data[0]['boxes']
        labels = data[0]['labels']
        num_dets = data[0]['num_dets']
        targets = target_generator(boxes, labels, num_dets)

