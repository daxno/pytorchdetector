from samplers import IterationBasedBatchSampler, DistributedSampler
from .loaders.DaliFromJson import DetectionPipeline, DataIterator
from nvidia.dali.plugin.pytorch import DALIGenericIterator
from .datasets import DetectionDataset
from .transforms import build_transforms
from torch.utils.data import DataLoader
from structures import to_image_list
import torch


class BatchCollator(object):
    """
    From a list of samples from the dataset,
    returns the batched images and targets.
    This should be passed to the DataLoader
    """

    def __init__(self, size_divisible=0):
        self.size_divisible = size_divisible

    def __call__(self, batch):
        transposed_batch = list(zip(*batch))
        images = to_image_list(transposed_batch[0], self.size_divisible)
        # images = transposed_batch[0]
        targets = transposed_batch[1]
        print(type(targets))
        img_ids = transposed_batch[2]
        return images, targets, img_ids


def make_data_sampler(dataset, shuffle, distributed=False):
    if distributed:
        return DistributedSampler(dataset, shuffle=shuffle)
    if shuffle:
        sampler = torch.utils.data.sampler.RandomSampler(dataset)
    else:
        sampler = torch.utils.data.sampler.SequentialSampler(dataset)
    return sampler


def make_batch_data_sampler(sampler, aspect_grouping, images_per_batch, num_iters=None, start_iter=0 ):
    if aspect_grouping:
        raise NotImplementedError('')
        # if not isinstance(aspect_grouping, (list, tuple)):
        #     aspect_grouping = [aspect_grouping]
        # aspect_ratios = _compute_aspect_ratios(dataset)
        # group_ids = _quantize(aspect_ratios, aspect_grouping)
        # batch_sampler = samplers.GroupedBatchSampler(
        #     sampler, group_ids, images_per_batch, drop_uneven=False
        # )
    else:
        batch_sampler = torch.utils.data.sampler.BatchSampler(
            sampler, images_per_batch, drop_last=False
        )
    if num_iters is not None:
        batch_sampler = IterationBasedBatchSampler(batch_sampler, num_iters, start_iter)
    return batch_sampler


def get_dataloader(cfg, is_train, start_iter=0, anchors=None):
    transform = build_transforms(cfg, is_train)
    dataset = DetectionDataset(cfg, is_train, transform, anchors=anchors)
    sampler = make_data_sampler(dataset, shuffle=is_train, distributed=cfg.DATALOADER.DISTR)
    batch_sampler = make_batch_data_sampler(sampler, aspect_grouping=False,
                                            images_per_batch=cfg.DATALOADER.TRAIN_BS
                                            if is_train else cfg.DATALOADER.VAL_BS, num_iters=cfg.DATALOADER.ITER_PER_EPOCH,
                                            start_iter=start_iter)
    loader = DataLoader(dataset, num_workers=cfg.DATALOADER.NUM_WORKERS, batch_sampler=batch_sampler, pin_memory=False,
                        collate_fn=dataset.collate_fn_dyn_image_size
                        if len(cfg.DATA.JITTER_SIZE) else dataset.collate_fn_static_image_size)
    return loader


def get_validation_loder(cfg):
    transform = build_transforms(cfg, is_train=False)
    dataset = DetectionDataset(cfg, is_train=False, transform=transform)
    loader = DataLoader(dataset, num_workers=1, collate_fn=dataset.collate_fn_static_image_size, shuffle=False,
                        batch_size=1, pin_memory=True)
    return loader


def get_dali_dataloader(cfg, dev_id=1, is_train=True):
    annotations_path = cfg.PATHS.TRAIN_ANNOTATION if is_train else cfg.PATHS.TEST_ANNOTATION
    batch_size = cfg.DATALOADER.TRAIN_BS if is_train else cfg.DATALOADER.VAL_BS
    classes = cfg.DATA.CLASSES
    iterator = DataIterator(annotations_path, batch_size, classes)
    color_mean, color_std = cfg.DATA.MEAN, cfg.DATA.STD
    # TODO WARNING В DETECTIONPIPELINE НЕТ РЕСАЙЗА ИЗОБРАЖЕНИЙ
    target_size = cfg.DATA.TRAIN_SIZE if is_train else cfg.DATA.TEST_SIZE
    target_size = target_size if len(target_size) else None
    dali_loader = DetectionPipeline(iterator, batch_size, num_threads=4, device_id=dev_id,
                                    target_size=target_size, color_mean=color_mean, color_std=color_std)
    dali_loader.build()
    return dali_loader

