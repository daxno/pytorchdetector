from structures import BoxList, to_image_list, cat_boxlist, boxlist_iou
from modeling.ATSS import get_atss_box_coder
from torch.utils.data import Dataset
import torch.nn.functional as F
import torch
import json
import cv2
import os

INF = 1e9


class DetectionsFromJson(Dataset):
    def __init__(self, cfg, is_train, transform=None, anchors=None):
        annot_path = cfg.PATHS.TRAIN_ANNOTATION if is_train else cfg.PATHS.TEST_ANNOTATION
        self.annotations = json.load(open(annot_path))
        print('Number of examples:', len(self.annotations.keys()))
        self._transform = transform
        self.is_train = is_train
        print('Loader train mode:', self.is_train)
        self.label2idx = {label: idx+1 for idx, label in enumerate(cfg.DATA.CLASSES)}
        self.cfg = cfg

        self.anchors = anchors
        self.is_fixed_size = self.anchors is not None
        self.box_coder = get_atss_box_coder(cfg)

    def __len__(self):
        return len(self.annotations.keys())

    def __getitem__(self, idx):
        image_path, labels = list(self.annotations.items())[idx]
        image_orig = cv2.imread(image_path)

        boxes = [[float(box['x']), float(box['y']), float(box['w']), float(box['h'])] for box in labels]
        classes = [self.label2idx[box['class']] for box in labels]
        if not len(boxes):
            boxes = [[0., 0., 0., 0.]]
            classes = [1]

        if self._transform is not None:
            out = self._transform(image=image_orig.copy(), bboxes=boxes, category_ids=classes)
            image, boxes, classes = out['image'], out['bboxes'], out['category_ids']

        targets = BoxList(torch.as_tensor(boxes).reshape(-1, 4), image.shape[1:][::-1], mode='xywh').convert('xyxy')
        targets.add_field('labels', torch.as_tensor(classes))

        if self.anchors is not None:
            targets = self._generate_targets(targets)
            targets = [t for t in targets.values()]

        if self.is_train:
            return image, targets
        else:
            return image, image_orig, targets, image_path

    def _generate_targets(self, targets):
        # Таргеты генерятся на CPU
        cls_targets, regr_targets = self._prepare_cls_regr_targets(targets, self.anchors)
        # reg_targets_flatten = torch.cat(regr_targets, dim=0)
        # anchors_flatten = cat_boxlist(self.anchors).bbox
        labels_flatten = torch.cat(cls_targets, dim=0)
        reg_targets_flatten = torch.cat(regr_targets, dim=0)
        anchors_flatten = cat_boxlist(self.anchors).bbox

        pos_inds = torch.nonzero(labels_flatten > 0).squeeze(1)
        if pos_inds.numel() > 0:
            reg_targets_flatten = reg_targets_flatten[pos_inds]
            anchors_flatten = anchors_flatten[pos_inds]
            centerness_targets = self._prepare_centerness_targets(reg_targets_flatten,
                                                                  anchors_flatten).type(torch.float32)
        else:
            centerness_targets = []
        targets = {
            'cls_target': cls_targets[0],
            'reg_target': regr_targets[0],
            'centerness_target': centerness_targets
        }
        return targets

    def _prepare_cls_regr_targets(self, targets, anchors):
        cls_labels = []
        reg_targets = []

        targets_per_im = targets
        assert targets_per_im.mode == "xyxy"
        bboxes_per_im = targets_per_im.bbox
        labels_per_im = targets_per_im.get_field("labels")
        anchors_per_im = cat_boxlist(anchors)
        num_gt = bboxes_per_im.shape[0]

        num_anchors_per_level = [len(anchors_per_level.bbox) for anchors_per_level in anchors]
        ious = boxlist_iou(anchors_per_im, targets_per_im)

        gt_cx = (bboxes_per_im[:, 2] + bboxes_per_im[:, 0]) / 2.0
        gt_cy = (bboxes_per_im[:, 3] + bboxes_per_im[:, 1]) / 2.0
        gt_points = torch.stack((gt_cx, gt_cy), dim=1)

        anchors_cx_per_im = (anchors_per_im.bbox[:, 2] + anchors_per_im.bbox[:, 0]) / 2.0
        anchors_cy_per_im = (anchors_per_im.bbox[:, 3] + anchors_per_im.bbox[:, 1]) / 2.0
        anchor_points = torch.stack((anchors_cx_per_im, anchors_cy_per_im), dim=1)

        distances = (anchor_points[:, None, :] - gt_points[None, :, :]).pow(2).sum(-1).sqrt()

        # Selecting candidates based on the center distance between anchor box and object
        candidate_idxs = []
        star_idx = 0
        for level, anchors_per_level in enumerate(anchors):
            end_idx = star_idx + num_anchors_per_level[level]
            distances_per_level = distances[star_idx:end_idx, :]
            _, topk_idxs_per_level = distances_per_level.topk(self.cfg.MODEL.ATSS.TOPK, dim=0, largest=False)
            candidate_idxs.append(topk_idxs_per_level + star_idx)
            star_idx = end_idx
        candidate_idxs = torch.cat(candidate_idxs, dim=0)

        # Using the sum of mean and standard deviation as the IoU threshold to select final positive samples
        candidate_ious = ious[candidate_idxs, torch.arange(num_gt)]
        iou_mean_per_gt = candidate_ious.mean(0)
        iou_std_per_gt = candidate_ious.std(0)
        iou_thresh_per_gt = iou_mean_per_gt + iou_std_per_gt
        is_pos = candidate_ious >= iou_thresh_per_gt[None, :]

        # Limiting the final positive samples’ center to object
        anchor_num = anchors_cx_per_im.shape[0]
        for ng in range(num_gt):
            candidate_idxs[:, ng] += ng * anchor_num
        e_anchors_cx = anchors_cx_per_im.view(1, -1).expand(num_gt, anchor_num).contiguous().view(-1)
        e_anchors_cy = anchors_cy_per_im.view(1, -1).expand(num_gt, anchor_num).contiguous().view(-1)
        candidate_idxs = candidate_idxs.view(-1)
        l = e_anchors_cx[candidate_idxs].view(-1, num_gt) - bboxes_per_im[:, 0]
        t = e_anchors_cy[candidate_idxs].view(-1, num_gt) - bboxes_per_im[:, 1]
        r = bboxes_per_im[:, 2] - e_anchors_cx[candidate_idxs].view(-1, num_gt)
        b = bboxes_per_im[:, 3] - e_anchors_cy[candidate_idxs].view(-1, num_gt)
        is_in_gts = torch.stack([l, t, r, b], dim=1).min(dim=1)[0] > 0.01
        is_pos = is_pos & is_in_gts

        # if an anchor box is assigned to multiple gts, the one with the highest IoU will be selected.
        ious_inf = torch.full_like(ious, -INF).t().contiguous().view(-1)
        index = candidate_idxs.view(-1)[is_pos.view(-1)]
        ious_inf[index] = ious.t().contiguous().view(-1)[index]
        ious_inf = ious_inf.view(num_gt, -1).t()

        anchors_to_gt_values, anchors_to_gt_indexs = ious_inf.max(dim=1)
        cls_labels_per_im = labels_per_im[anchors_to_gt_indexs]
        cls_labels_per_im[anchors_to_gt_values == -INF] = 0
        matched_gts = bboxes_per_im[anchors_to_gt_indexs]

        # matched_gts = matched_gts.to(self.device)
        # anchors_per_im = anchors_per_im.to(self.device)

        reg_targets_per_im = self.box_coder.encode(matched_gts, anchors_per_im.bbox)
        # cls_labels.append(cls_labels_per_im.to(self.device))
        cls_labels.append(cls_labels_per_im)
        reg_targets.append(reg_targets_per_im)

        return cls_labels, reg_targets

    def _prepare_centerness_targets(self, regr_targets, anchors):
        gts = self.box_coder.decode(regr_targets, anchors)
        anchors_cx = (anchors[:, 2] + anchors[:, 0]) / 2
        anchors_cy = (anchors[:, 3] + anchors[:, 1]) / 2
        l = anchors_cx - gts[:, 0]
        t = anchors_cy - gts[:, 1]
        r = gts[:, 2] - anchors_cx
        b = gts[:, 3] - anchors_cy
        left_right = torch.stack([l, r], dim=1)
        top_bottom = torch.stack([t, b], dim=1)
        centerness = torch.sqrt((left_right.min(dim=-1)[0] / left_right.max(dim=-1)[0]) * (top_bottom.min(dim=-1)[0] / top_bottom.max(dim=-1)[0]))
        assert not torch.isnan(centerness).any()
        return centerness

    @staticmethod
    def get_num_gpus():
        # DataParallel позаботится обо всем
        return 1

    @staticmethod
    def collate_fn_dyn_image_size(batch):
        transposed_batch = list(zip(*batch))
        if len(transposed_batch) == 2:
            # Train loader (ImageList, Targets)
            images = to_image_list(transposed_batch[0])
            targets = transposed_batch[1]
            return images, targets
        elif len(transposed_batch) == 4:
            # Test loader (ImageList, images_original, Targets (maybe empty), images path)
            images = to_image_list(transposed_batch[0])
            images_orig = transposed_batch[1]
            targets = transposed_batch[2]
            paths = transposed_batch[3]
            return images, images_orig, targets, paths
        else:
            raise RuntimeError(f'Unexpected number of dataset outputs: {len(transposed_batch)}')

    @staticmethod
    def collate_fn_static_image_size(batch):
        transposed_batch = list(zip(*batch))
        if len(transposed_batch) == 2:
            # Train loader (ImageList, Targets)
            images = to_image_list(transposed_batch[0])
            targets = transposed_batch[1]
            cls_targets, reg_targets, centerness_targets = [], [], []
            for idx in range(len(batch)):
                cls_targets.append(targets[idx][0])
                reg_targets.append(targets[idx][1])
                centerness_targets.append(targets[idx][2])

            targets = [cls_targets, reg_targets, centerness_targets]
            return images, targets
        elif len(transposed_batch) == 4:
            # Test loader (ImageList, images_original, Targets (maybe empty), images path)
            images = to_image_list(transposed_batch[0])
            images_orig = transposed_batch[1]
            targets = transposed_batch[2]
            paths = transposed_batch[3]
            return images, images_orig, targets, paths
        else:
            raise RuntimeError(f'Unexpected number of dataset outputs: {len(transposed_batch)}')