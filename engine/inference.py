from layers import get_anchor_generator_atss
from modeling.ATSS import get_atss_postprocessor, get_atss_box_coder
from modeling import get_mobilenetv1_fpn, get_atss_model, get_mobilenetv3_large_fpn
from data.transforms.transforms import ResizeKeepAspectRatio
from data import get_validation_loder
from structures import to_image_list
from config import cfg

import matplotlib.pyplot as plt
from tqdm import tqdm
import numpy as np
import torch
import json
import cv2


class Postprocessor:
    def __init__(self, cfg):
        # self.device = torch.device('cpu')
        self.anchor_generator = get_anchor_generator_atss(cfg)
        self.postprocessor = get_atss_postprocessor(cfg, get_atss_box_coder(cfg))

    def __call__(self, model_outputs, images=None, anchors=None):
        if anchors is None: assert images is not None
        box_cls, box_regression, centerness, bb_features = model_outputs
        # box_cls = [feature.to(self.device) for feature in box_cls]
        # box_regression = [feature.to(self.device) for feature in box_regression]
        # centerness = [feature.to(self.device) for feature in centerness]
        # bb_features = [feature.to(self.device) for feature in bb_features]
        if anchors is None:
            anchors = self.anchor_generator(images, bb_features)
        return self.postprocessor(box_cls, box_regression, centerness, anchors)


def get_anchors(model, target_size, anchor_generator, device='cuda:0'):
    input_tensor = torch.zeros((1, 3, *target_size))
    input_images = to_image_list(input_tensor)
    model_out = model(input_tensor.to(device))
    bb_features = model_out[-1]
    anchors = anchor_generator(input_images, bb_features)
    anchors = [[anch.to(device) for anch in anchor] for anchor in anchors]
    return anchors


def save_results(out_path, image_path, detections):
    image_name = image_path.split('/')[-1].split('.')[0]
    with open(f'{out_path}/{image_name}.txt', 'w') as fp:
        for det in detections:
            fp.write(f'{det[0]} {det[1]} {det[2]} {det[3]} {det[4]} {det[5]}\n')


if __name__ == '__main__':
    cfg.freeze()
    device = torch.device('cuda:0')
    model = get_atss_model(cfg, get_mobilenetv3_large_fpn)
    state_dict = torch.load(cfg.INFERENCE.CKPT_PATH)['state_dict']
    state_dict = {k.replace('module.', ''): v for k, v in state_dict.items()}
    model.load_state_dict(state_dict)
    model.to(device).eval()

    postprocessor = Postprocessor(cfg)
    anchors = get_anchors(model, (768, 1024), get_anchor_generator_atss(cfg))
    # resize_fn = ResizeKeepAspectRatio((768, 1024))

    image_counter = 0
    if cfg.INFERENCE.PROCESS == 'json':
        loader = get_validation_loder(cfg)
        iterator = tqdm(enumerate(loader), total=len(loader))
        for idx, (images, images_orig, targets, paths) in iterator:
            model_output = model(images.tensors.to(device))
            pred_boxes = postprocessor(model_output, anchors=anchors)
            # pred_boxes = [postprocessor([box_cls.unsqueeze(0), box_regression.unsqueeze(0), centerness.unsqueeze(0), bb_features.unsqueeze(0)], anchors=anchors)
            #               for box_cls, box_regression, centerness, bb_features in zip(*model_output)]
            pred_boxes = [pred.to(torch.device('cpu')) for pred in pred_boxes]
            for image, image_boxes, image_targets, path in zip(images_orig, pred_boxes, targets, paths):

                image_detections = []
                # image = resize_fn(True, image, None, None)['image']
                # ratio = image_targets.get_field('ratio')
                # pad = image_targets.get_field('pad')
                # print('Predicted boxes:')
                # print(image_boxes.bbox)
                # print('Scores')
                # print(image_boxes.get_field('scores'))
                print(image_boxes.get_field('scores') )
                for box, score in zip(image_boxes.bbox, image_boxes.get_field('scores')):
                    if score < cfg.INFERENCE.SCORE_TH: continue
                    # box -= torch.as_tensor([pad[0], pad[1], pad[0], pad[1]])
                    # box /= ratio
                    if cfg.INFERENCE.VISUALIZE:
                        x1, y1, x2, y2 = list(map(int, box))
                        image = cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
                        # image_detections.append(['face', float(score.data.cpu().numpy()), x1, y1, x2, y2])
                if cfg.INFERENCE.VISUALIZE:
                    cv2.imshow('image', image[..., ::-1])
                    cv2.waitKey()

        #         image_counter += 1
        #         save_results('/home/nlab/Experiments/ObjectDetectionMeter/wider-val-atss-dets', path, image_detections)
        # print('Print processed images:', image_counter)

    elif cfg.INFERENCE.PROCESS == 'images':
        pass
    elif cfg.INFERENCE.PROCESS == 'video':
        raise NotImplementedError('')
