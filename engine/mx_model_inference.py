import cv2
import json
import time
# import torch
# import mxnet as mx
# import numpy as np
from tqdm import tqdm
from config import cfg
from modeling import get_ssd_face_detector_mxnet

import os
os.environ['MXNET_CUDNN_AUTOTUNE_DEFAULT'] = '1'

if __name__ == '__main__':
    wider_val = json.load(open('/home/nlab/Experiments/Detector/resources/face_annotations_v3_test_small.json'))
    ssd_model = get_ssd_face_detector_mxnet(cfg)
    gt_path = '/home/nlab/Experiments/ObjectDetectionMeter/NLabFaces/val-gt'
    dets_path = '/home/nlab/Experiments/ObjectDetectionMeter/NLabFaces/val-ssd-dets'

    # Forward example
    # in_image = np.zeros((512, 768, 3))
    # model.store_data(image=in_image)
    # predictions = model.process_data()
    # print(predictions)

    # bs = 4
    paths, predictions = [], []
    start_time = time.time()
    for idx, image_path in tqdm(enumerate(wider_val.keys()), total=len(wider_val)):
        ssd_model.store_data(image_path=image_path)
        # print('Start prediction')
        # start_time = time.time()
        if idx and not idx % cfg.SSD.BATCH_SIZE:
            out = ssd_model.process_data()
            # duration = time.time() - start_time
            # print(f'Images processed, mean FPS: {bs/duration:.4f}')
            paths.extend(out[0])
            predictions.extend(out[1])
            ssd_model.reset()
    duration = time.time() - start_time
    print(f'Total time: {duration:.4f} s; FPS: {len(wider_val)/duration:.4f}')

    # Visualization
    for image_path, preds in zip(paths, predictions):
        image = cv2.imread(image_path)
        for det in preds:
            x, y, w, h = list(map(int, [det['x'], det['y'], det['w'], det['h']]))
            image = cv2.rectangle(image, (x, y), (x+w, y+h), (0, 0, 255), 2)
        cv2.imshow('image', image)
        if cv2.waitKey() == 27: break

    # for image_path, preds in zip(paths, predictions):
    #     image_name = image_path.split('/')[-1].split('.')[0]
    #     gt_boxes = wider_val[image_path]
    #
    #     image = cv2.imread(image_path)
    #     if cfg.INFERENCE.VISUALIZE:
    #         for d in gt_boxes:
    #             x, y, w, h = list(map(int, [d['x']*image.shape[1], d['y']*image.shape[0],
    #                                         d['w']*image.shape[1], d['h']*image.shape[0]]))
    #             image = cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)
    #     else:
    #         with open(f'{gt_path}/{image_name}.txt', 'w') as fp:
    #             for d in gt_boxes:
    #                 x, y, w, h = list(map(int, [d['x']*image.shape[1], d['y']*image.shape[0],
    #                                             d['w']*image.shape[1], d['h']*image.shape[0]]))
    #                 fp.write(f'face {x} {y} {x+w} {y+h}\n')
    #
    #     if cfg.INFERENCE.VISUALIZE:
    #         for d in preds:
    #             score = d['score']
    #             x, y, w, h = list(map(int, [d['x'], d['y'], d['w'], d['h']]))
    #             image = cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
    #         cv2.imshow('image', image)
    #         cv2.waitKey()
    #     else:
    #         with open(f'{dets_path}/{image_name}.txt', 'w') as fp:
    #             for d in preds:
    #                 score = d['score']
    #                 x, y, w, h = list(map(int, [d['x'], d['y'], d['w'], d['h']]))
    #                 fp.write(f'face {score} {x} {y} {x + w} {y + h}\n')
    # print('Predictions successfully saved')

