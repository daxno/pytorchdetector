from modeling.ATSS import get_atss_postprocessor, get_atss_box_coder
from modeling import get_atss_model, get_mobilenetv3_small_fpn
from nvidia.dali.plugin.pytorch import DALIGenericIterator
from layers import get_anchor_generator_atss
from data import get_dali_dataloader
from structures import to_image_list
from torch.backends import cudnn
from torch2trt import TRTModule
from config import cfg
from tqdm import tqdm
import numpy as np
import torch
import time
import json
import cv2


class Postprocessor:
    def __init__(self, cfg):
        self.device = torch.device('cpu')
        self.anchor_generator = get_anchor_generator_atss(cfg)
        self.postprocessor = get_atss_postprocessor(cfg, get_atss_box_coder(cfg))
        self.postprocessor = self.postprocessor.to(self.device)

    def __call__(self, model_outputs, anchors):
        if anchors is None: assert images is not None
        box_cls, box_regression, centerness = model_outputs
        # box_cls = [feature.to(self.device) for feature in box_cls]
        # box_regression = [feature.to(self.device) for feature in box_regression]
        # centerness = [feature.to(self.device) for feature in centerness]
        # bb_features = [feature.to(self.device) for feature in bb_features]

        return self.postprocessor(box_cls, box_regression, centerness, anchors)


def get_anchors(model, batch_size, target_size, anchor_generator, device='cuda:0'):
    input_tensor = torch.zeros((batch_size, 3, *target_size))
    input_images = to_image_list(input_tensor)
    model_out = model(input_tensor.to(device))
    bb_features = model_out[-1]
    anchors = anchor_generator(input_images, bb_features)
    anchors = [[anch.to(device) for anch in anchor] for anchor in anchors]
    return anchors


def postprocess_outputs(outputs: list):
    # print([out.size() for out in outputs])
    # trt_output = [out.squeeze(1) if out.ndim == 5 else out for out in outputs]
    box_cls = [out[:, :1, ...] for out in outputs]
    centerness = [out[:, 1:2, ...] for out in outputs]
    box_regression = [out[:, 2:, ...] for out in outputs]
    return box_cls, box_regression, centerness


if __name__ == '__main__':
    cudnn.benchmark = True
    device = torch.device('cuda:0')

    inputs = ['images']
    outputs = [f'feature_map_{idx}' for idx in range(5)]
    model_trt = TRTModule(input_names=inputs, output_names=outputs)
    model_trt.load_state_dict(torch.load(cfg.PATHS.TRT_ENGINE_PATH))

    gt_dets_path = '/home/nlab/Experiments/ObjectDetectionMeter/NLabFaces/val-resized-gt'
    dets_path = '/home/nlab/Experiments/ObjectDetectionMeter/NLabFaces/val-atss-trt-dets'

    # Generate anchors
    model = get_atss_model(cfg, get_mobilenetv3_small_fpn).to(device)
    # model.backbone.backbone = backbone_trt
    anchors = get_anchors(model, cfg.DATALOADER.VAL_BS, (768, 1024), get_anchor_generator_atss(cfg), device)
    postprocessor = Postprocessor(cfg)
    mean_t, std_t = torch.tensor(cfg.DATA.MEAN), torch.tensor(cfg.DATA.STD)
    mean, std = cfg.DATA.MEAN, cfg.DATA.STD

    pipeline = get_dali_dataloader(cfg, dev_id=0, is_train=False)
    num_examples = len(json.load(open(cfg.PATHS.TEST_ANNOTATION)))
    torch_data_iterator = DALIGenericIterator([pipeline], output_map=['images', 'boxes', 'labels', 'num_dets', ''],
                                              size=num_examples,
                                              auto_reset=False, dynamic_shape=True)

    images_counter = 0
    start_time = time.time()
    for idx, data in tqdm(enumerate(torch_data_iterator), total=num_examples / cfg.DATALOADER.VAL_BS):

        images = torch.cat([d['images'] for d in data])
        gt_boxes = torch.cat([d['boxes'] for d in data])
        # gt_labels = torch.cat([d['labels'] for d in data])
        # num_dets_per_image = torch.cat([d['num_dets'].squeeze(-1) for d in data])
        with torch.no_grad():
            trt_out = model_trt(images)
        model_output = postprocess_outputs(trt_out)
        # for out in model_output:
        #     print([o.size() for o in out])
        # for out in model_output:
        #     print([o.size() for o in out])
        pred_boxes = postprocessor(model_output, anchors=anchors)
        pred_boxes = [pred.to(torch.device('cpu')) for pred in pred_boxes]

        for image, image_boxes, gt_b in zip(images, pred_boxes, gt_boxes):
            image = image.data.cpu().numpy().transpose((1, 2, 0))
            image = ((image * np.array(std) / 255. + np.array(mean) / 255.) * 255.).astype(np.uint8).copy()
            image_h, image_w = image.shape[:2]
            image_detections = []

            with open(f'{gt_dets_path}/image_{images_counter:08d}.txt', 'w') as fp:
                for box in gt_b:
                    x1, y1, x2, y2 = list(map(int, [box[0] * image_w, box[1] * image_h, box[2] * image_w, box[3] * image_h]))
                    if x1 == x2 or y1 == y2 or not sum([x1, y1, x2, y2]): continue
                    print('GT box:', [x1, y1, x2, y2])
                    if cfg.INFERENCE.VISUALIZE:
                        image = cv2.rectangle(image, (x1, y1), (x2, y2), (0, 0, 255), 2)
                    else:
                        fp.write(f'face {x1} {y1} {x2} {y2}\n')

            # print(f'Scores:', image_boxes.get_field('scores'))
            with open(f'{dets_path}/image_{images_counter:08d}.txt', 'w') as fp:
                for box, score in zip(image_boxes.bbox, image_boxes.get_field('scores')):
                    if score < cfg.INFERENCE.SCORE_TH: continue
                    x1, y1, x2, y2 = list(map(int, box))
                    # print(x1, y1, x2, y2)
                    if cfg.INFERENCE.VISUALIZE:
                        image = cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
                    else:
                        fp.write(f'face {score} {x1} {y1} {x2} {y2}\n')
            images_counter += 1

            if cfg.INFERENCE.VISUALIZE:
                cv2.imshow('image', image[..., ::-1])
                cv2.waitKey()

    # duration = time.time() - start_time
    # print(f'Total time: {duration:.4f} s; FPS: {num_examples / duration:.4f}')
